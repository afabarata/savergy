package pt.ipleiria.estg.dei.savergy.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.ArrayAdapter;

import pt.ipleiria.estg.dei.savergy.EnergyProfileActivity;
import pt.ipleiria.estg.dei.savergy.LoginActivity;
import pt.ipleiria.estg.dei.savergy.RegisterChoiceActivity;
import pt.ipleiria.estg.dei.savergy.helpers.NetworkUtility;
import pt.ipleiria.estg.dei.savergy.model.Equipment;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.Iterator;

import pt.ipleiria.estg.dei.savergy.R;
import pt.ipleiria.estg.dei.savergy.model.Savergy;

/**
 * Created by Bruno on 2017-04-25.
 */

public class EquipmentGridAdapter extends ArrayAdapter<Equipment> {

    private static ProgressDialog progress;
    private static int numObjects;
    private static int numAux;

    private int maxHeight = 0;

    private static final class ViewHolder {
        public View view;

        public TextView textViewEquipmentName;
        public ImageView imageViewEquipamentIcon;
        public TextView textViewEquipmentTime;

        public ViewHolder(View view) {
            this.view = view;

            textViewEquipmentName = (TextView) view.findViewById(R.id.textViewEquipmentName);
            imageViewEquipamentIcon = (ImageView) view.findViewById(R.id.imageViewEquipmentIcon);
            textViewEquipmentTime = (TextView) view.findViewById(R.id.textViewEquipmentTime);
        }

        public void update(final Equipment equipment) {

            textViewEquipmentName.setText(equipment.getName());

            if(view.getContext().getClass().getSimpleName().equals("EnergyProfileActivity")) {
                textViewEquipmentTime.setVisibility(View.GONE);
            } else {
                if(equipment.getConfiguredUseTime() == 0) {
                    textViewEquipmentTime.setText(equipment.getDefaultUseTime()+" min");
                } else {
                    textViewEquipmentTime.setText(equipment.getConfiguredUseTime()+" min");
                }
            }


            if(NetworkUtility.hasConnectivity(view.getContext())) {
                Picasso.with(view.getContext()).load(equipment.getIconURL()).into(imageViewEquipamentIcon, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        /*numAux++;
                        if(numObjects == numAux) {
                            numObjects = 0;
                            numAux = 0;
                            progress.dismiss();
                        }*/

                        textViewEquipmentName.setHeight(120);
                    }

                    @Override
                    public void onError() {

                    }
                });
            } else {
                Drawable d = view.getResources().getDrawable(R.drawable.no_photo);
                imageViewEquipamentIcon.setImageDrawable(d);

                //Log.d("Height: ",textViewEquipmentName.getHeight()+"");
                textViewEquipmentName.setHeight(120);
            }

        }
    }

    public EquipmentGridAdapter(Context context, Iterable<Equipment> objects) {
        this(context, objects.iterator());
    }

    public EquipmentGridAdapter(Context context, Iterator<Equipment> objects) {
        super(context, R.layout.equipment_grid_item, R.id.textViewEquipmentName);
        numObjects = 0;
        numAux = 0;

        while (objects.hasNext()) {
            add(objects.next());
            numObjects++;
        }

        /*if(NetworkUtility.hasConnectivity(getContext()) && numObjects > 0) {
            progress = new ProgressDialog(context,R.style.MyProgressDialogTheme);
            progress.setCancelable(false);
            progress.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            progress.show();
        }*/
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        Equipment equipment = getItem(position);

        ViewHolder holder = (ViewHolder)view.getTag();

        if (holder==null) {
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        holder.update(equipment);

        return view;
    }


    @Override
    public long getItemId(int position) {
        return getItem(position).getServerid();
    }

}

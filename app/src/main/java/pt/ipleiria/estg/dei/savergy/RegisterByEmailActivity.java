package pt.ipleiria.estg.dei.savergy;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

import pt.ipleiria.estg.dei.savergy.helpers.LoginHelper;
import pt.ipleiria.estg.dei.savergy.helpers.NetworkUtility;
import pt.ipleiria.estg.dei.savergy.model.Savergy;

/**
 * Created by Trabalho on 14/03/2017.
 */

public class RegisterByEmailActivity extends AppCompatActivity {

    private ActionMenuView defaultMenu;
    private ActionMenuView defaultMenuBack;

    private LoginHelper loginHelper= null;
    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );

    EditText name;
    EditText email;
    EditText password;
    EditText re_password;

    TextView name_error;
    TextView email_error;
    TextView password_error;
    TextView re_password_error;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_by_email);

        Toolbar t = (Toolbar) findViewById(R.id.tToolbar);
        defaultMenu = (ActionMenuView) t.findViewById(R.id.defaultMenu);
        defaultMenuBack = (ActionMenuView) t.findViewById(R.id.defaultMenuBack);
        defaultMenu.setOnMenuItemClickListener(new ActionMenuView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                return onOptionsItemSelected(menuItem);
            }
        });
        defaultMenuBack.setOnMenuItemClickListener(new ActionMenuView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                return onOptionsItemSelected(menuItem);
            }
        });
        setSupportActionBar(t);
        getSupportActionBar().setTitle(null);

        name = (EditText) findViewById(R.id.editTextName);
        email = (EditText) findViewById(R.id.editTextEmail);
        password = (EditText) findViewById(R.id.editTextPassword);
        re_password = (EditText) findViewById(R.id.editTextRePassword);

        name_error = (TextView) findViewById(R.id.textViewNameError);
        email_error = (TextView) findViewById(R.id.textViewEmailError);
        password_error = (TextView) findViewById(R.id.textViewPasswordError);
        re_password_error = (TextView) findViewById(R.id.textViewRePasswordError);

        loginHelper = new LoginHelper(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        //inflater.inflate(R.menu.menu_profile, defaultMenu.getMenu());
        inflater.inflate(R.menu.menu_return, defaultMenuBack.getMenu());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        long id=item.getItemId();

        if (id == R.id.menuReturnActivity) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClickRegister(View view) {

        if(name.getText().toString().isEmpty()) {
            name_error.setText(R.string.register_name_required);
        } else {
            name_error.setText("");
        }

        if(email.getText().toString().isEmpty()) {
            email_error.setText(R.string.register_email_required);
        } else {
            email_error.setText("");
        }

        if(!checkEmail(email.getText().toString())) {
            email_error.setText(R.string.register_email_format);
        } else {
            email_error.setText("");
        }

        if(password.getText().toString().length() < 6) {
            password_error.setText(R.string.register_password_format);
        } else {
            password_error.setText("");
        }

        if(!password.getText().toString().equals(re_password.getText().toString())) {
            re_password_error.setText(R.string.register_password_equals);
        } else {
            re_password_error.setText("");
        }

        if(!name.getText().toString().isEmpty() && !email.getText().toString().isEmpty() && checkEmail(email.getText().toString()) && password.getText().toString().length() >= 6 && password.getText().toString().equals(re_password.getText().toString())) {
            if(NetworkUtility.hasConnectivity(this)) {
                try {
                    if(!loginHelper.persistRemoteUserByEmail(Savergy.INSTANCE.getCurrentUser().getDeviceId(), name.getText().toString(), email.getText().toString(), password.getText().toString())){
                        LayoutInflater li = LayoutInflater.from(RegisterByEmailActivity.this);
                        View promptsView = li.inflate(R.layout.dialog_remove_account_confirm, null);

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegisterByEmailActivity.this);

                        alertDialogBuilder.setView(promptsView);

                        final TextView dialogIntro = (TextView) promptsView.findViewById(R.id.textViewEqDialogQuestion);
                        dialogIntro.setText(R.string.register_error_on_register);

                        alertDialogBuilder
                                .setCancelable(false)
                                .setPositiveButton(R.string.dialog_login,
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,int id) {
                                                Intent intent = new Intent(RegisterByEmailActivity.this, LoginActivity.class);
                                                startActivity(intent);
                                            }
                                        })
                                .setNegativeButton(R.string.dialog_close,
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    } else {
                        loginHelper.getRemoteUserByEmail(email.getText().toString(),password.getText().toString());
                        Intent intent = new Intent(RegisterByEmailActivity.this, DashboardActivity.class);
                        startActivity(intent);
                    }
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }

    }

    private boolean checkEmail(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    @Override
    public void onBackPressed() {
        finish();
    }


}

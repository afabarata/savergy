package pt.ipleiria.estg.dei.savergy.model;

/**
 * Created by Trabalho on 30/03/2017.
 */

public class Answer {

    private int id;
    private int serverid;
    private int questionid;
    private String answer;
    private String advise;
    private int points;
    private int selected;

    public Answer(int serverid, int questionid, String answer, String advise, int points) {
        this.serverid = serverid;
        this.questionid = questionid;
        this.answer = answer;
        this.advise = advise;
        this.points = points;
        this.selected = 0;
    }

    public int getServerid() {
        return serverid;
    }

    /*public void setServerid(int serverid) {
        this.serverid = serverid;
    }*/

    public int getQuestionid() {
        return questionid;
    }

    /*public void setQuestionid(int questionid) {
        this.questionid = questionid;
    }*/

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAdvise() {
        return advise;
    }

    public void setAdvise(String advise) {
        this.advise = advise;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getSelected() {
        return selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }
}

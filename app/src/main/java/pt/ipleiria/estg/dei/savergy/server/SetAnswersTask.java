package pt.ipleiria.estg.dei.savergy.server;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;

import pt.ipleiria.estg.dei.savergy.model.Answer;
import pt.ipleiria.estg.dei.savergy.model.Question;
import pt.ipleiria.estg.dei.savergy.model.Savergy;

/**
 * Created by Trabalho on 27/06/2017.
 */

public class SetAnswersTask extends AsyncTask<String, Void, String> {

    private final static String clientID = "f9alvjJieqOj1bkVZR7orxh8I8yJub7MYNdR4KPmVwM1LChL17Q8qlzkvTwn";

    private Exception exception;

    protected void onPreExecute() {

    }

    protected String doInBackground(String... params) {

        StringBuilder sb = new StringBuilder();
        BufferedReader br;

        try {
            URL url = new URL(Savergy.INSTANCE.getAPI_URL() + "/api/answers/setMyAnswers");
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setDoOutput(true);

            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Accept", "application/json");
            httpURLConnection.setRequestProperty("Content-Type", "application/json");
            httpURLConnection.setRequestProperty("Authorization", "Bearer "+params[0]);
            httpURLConnection.connect();

            LinkedList<Question> questions = new LinkedList<>();
            int id = Integer.parseInt(params[1]);

            if(Long.parseLong(params[1]) == 0) {
                questions = Savergy.INSTANCE.getProfileQuestionsQuestions();
                id = 1;
            } else {
                questions = Savergy.INSTANCE.getQuestionsByBatchServerId(Long.parseLong(params[1]));
            }

            JSONObject jsonObject = new JSONObject();
            JSONArray arr = new JSONArray();

            for (Question q: questions) {
                for (Answer a: q.getAnswers()) {
                    if(a.getSelected() == 1) {
                        JSONObject arr_root = new JSONObject();
                        arr_root.put("batch_id",id);
                        arr_root.put("answer_id",a.getServerid());
                        arr_root.put("question_id",q.getServerid());
                        arr.put((Object)arr_root);
                    }
                }
            }
            jsonObject.put("user",Savergy.INSTANCE.getCurrentUser().getDeviceId());
            jsonObject.put("answers",(Object)arr);

            Log.i("jsonObject: ", jsonObject.toString());

            DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
            wr.writeBytes(jsonObject.toString());
            wr.flush();
            wr.close();

            if (200 <= httpURLConnection.getResponseCode() && httpURLConnection.getResponseCode() <= 299) {
                br = new BufferedReader(new InputStreamReader((httpURLConnection.getInputStream())));
            } else {
                br = new BufferedReader(new InputStreamReader((httpURLConnection.getErrorStream())));
            }

            String output;
            while ((output = br.readLine()) != null) {
                sb.append(output);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }

    protected void onPostExecute(String response) {

        if(response == null) {
            response = "THERE WAS AN ERROR";
        } else {
            try {
                JSONObject obj = new JSONObject(response);
                if(obj.getInt("status") == 200) {
                    Log.i("FEZ SET ÀS RESPOSTAS: ", obj.getInt("status")+"");
                } else {
                    Log.i("N FEZ SET ÀS RESPOSTAS ", obj.getInt("status")+"");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Log.i("INFO", response);
    }
}

package pt.ipleiria.estg.dei.savergy.server;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import pt.ipleiria.estg.dei.savergy.model.Savergy;

/**
 * Created by Trabalho on 18/06/2017.
 */

public class LoginWithDeviceIdTask extends AsyncTask<String, Void, String> {

    private final static String clientID = "f9alvjJieqOj1bkVZR7orxh8I8yJub7MYNdR4KPmVwM1LChL17Q8qlzkvTwn";

    private Exception exception;

    protected void onPreExecute() {

    }

    protected String doInBackground(String... params) {

        StringBuilder sb = new StringBuilder();
        BufferedReader br;

        try {
            URL url = new URL(Savergy.INSTANCE.getAPI_URL() + "/api/login");
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setDoOutput(true);

            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Accept", "application/json");
            httpURLConnection.setRequestProperty("Content-Type", "application/json");
            httpURLConnection.connect();

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("client_id", clientID);
            jsonObject.put("device_id", params[0]);

            DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
            wr.writeBytes(jsonObject.toString());
            wr.flush();
            wr.close();

            if (200 <= httpURLConnection.getResponseCode() && httpURLConnection.getResponseCode() <= 299) {
                br = new BufferedReader(new InputStreamReader((httpURLConnection.getInputStream())));
            } else {
                br = new BufferedReader(new InputStreamReader((httpURLConnection.getErrorStream())));
            }

            String output;
            while ((output = br.readLine()) != null) {
                sb.append(output);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }

    protected void onPostExecute(String response) {

        if(response == null) {
            response = "THERE WAS AN ERROR";
        } else {
            try {
                JSONObject obj = new JSONObject(response);
                if(obj.getInt("status") == 200) {

                    JSONObject js_obj = obj.getJSONObject("data");
                    Log.i("API TOKEN: ", js_obj.getString("api_token"));

                    Savergy.INSTANCE.setAPI_TOKEN(js_obj.getString("api_token"));
                    Savergy.INSTANCE.getCurrentUser().setNumPoints(js_obj.getInt("total_points"));

                } else {
                    Log.i("NÃO FEZ LOGIN DEVICE: ", obj.getInt("status")+"");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Log.i("INFO", response);
    }
}

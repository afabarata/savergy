package pt.ipleiria.estg.dei.savergy.helpers;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import pt.ipleiria.estg.dei.savergy.model.Savergy;
import pt.ipleiria.estg.dei.savergy.server.GetAdvicesTask;
import pt.ipleiria.estg.dei.savergy.server.GetBatchesTask;
import pt.ipleiria.estg.dei.savergy.server.GetEquipmentsTask;
import pt.ipleiria.estg.dei.savergy.server.GetRankingCountryTask;
import pt.ipleiria.estg.dei.savergy.server.GetRankingLocalTask;

/**
 * Created by Trabalho on 27/06/2017.
 */

public class ConnectionsHelper {

    private Context context;
    private GetBatchesTask getBatchesTask;
    private GetEquipmentsTask getEquipmentsTask;
    private GetAdvicesTask getAdvicesTask;
    private GetRankingLocalTask getRankingLocalTask;
    private GetRankingCountryTask getRankingCountryTask;

    public ConnectionsHelper(Context context) {
        this.context = context;
        this.getBatchesTask = new GetBatchesTask();
        this.getEquipmentsTask = new GetEquipmentsTask();
        this.getAdvicesTask = new GetAdvicesTask();
        this.getRankingLocalTask = new GetRankingLocalTask();
        this.getRankingCountryTask = new GetRankingCountryTask();

    }

    public void getBatcherFromServer() throws ExecutionException, InterruptedException {
        String status = getBatchesTask.execute(Savergy.INSTANCE.getAPI_TOKEN()).get();
        try {
            JSONObject obj = new JSONObject(status);
            if (obj.getInt("status") == 200) {
                JSONObject js_obj = obj.getJSONObject("data");
                JsonHelper jsonHelper = new JsonHelper();
                jsonHelper.insertBatchesToBD(context, jsonHelper.readBatchesFromServer(js_obj.toString()));

                Savergy.INSTANCE.uploadProfileQuestions(context);
                Savergy.INSTANCE.uploadBatchesList(context);

            } else {
                Toast.makeText(context,"Não foi possivel carregar os Quizzes.",Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getEquipmentsFromServer() throws ExecutionException, InterruptedException {
        String status = getEquipmentsTask.execute(Savergy.INSTANCE.getAPI_TOKEN()).get();
        try {
            JSONObject obj = new JSONObject(status);
            if (obj.getInt("status") == 200) {
                JSONObject js_obj = obj.getJSONObject("data");
                JsonHelper jsonHelper = new JsonHelper();
                jsonHelper.insertEquipmentsToBD(context, jsonHelper.readEquipmentFromServer(js_obj.toString()));

                Savergy.INSTANCE.uploadEquipmentsList(context);
                Savergy.INSTANCE.uploadSelectedEquipmentsList(context);
            } else {
                Toast.makeText(context,"Não foi possivel carregar os Equipmentos.",Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getAdvicesFromServer() throws ExecutionException, InterruptedException {
        String status = getAdvicesTask.execute(Savergy.INSTANCE.getAPI_TOKEN()).get();
        try {
            JSONObject obj = new JSONObject(status);
            if (obj.getInt("status") == 200) {
                JSONObject js_obj = obj.getJSONObject("data");
                JsonHelper jsonHelper = new JsonHelper();
                jsonHelper.insertAdvicesToBD(context, jsonHelper.readAdvicesFromServer(js_obj.toString()));

                Savergy.INSTANCE.uploadAdvicesList(context);
            } else {
                Toast.makeText(context,"Não foi possivel carregar os Conselhos.",Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getRankingLocalFromSerer() throws ExecutionException, InterruptedException {
        String status = getRankingLocalTask.execute(Savergy.INSTANCE.getAPI_TOKEN()).get();
        try {
            JSONObject obj = new JSONObject(status);
            if (obj.getInt("status") == 200) {
                JSONObject js_obj = obj.getJSONObject("data");
                JsonHelper jsonHelper = new JsonHelper();
                Savergy.INSTANCE.setRankingLocalList(jsonHelper.readRankingFromServer(js_obj.toString()));
            } else {
                Toast.makeText(context,"Não foi possivel carregar o Ranking Local.",Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getRankingCountryFromSerer() throws ExecutionException, InterruptedException  {
        String status = getRankingCountryTask.execute(Savergy.INSTANCE.getAPI_TOKEN()).get();
        try {
            JSONObject obj = new JSONObject(status);
            if (obj.getInt("status") == 200) {
                JSONObject js_obj = obj.getJSONObject("data");
                JsonHelper jsonHelper = new JsonHelper();
                Savergy.INSTANCE.setRankingCountryList(jsonHelper.readRankingFromServer(js_obj.toString()));
            } else {
                Toast.makeText(context,"Não foi possivel carregar o Ranking Local.",Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

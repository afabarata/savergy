package pt.ipleiria.estg.dei.savergy.fragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.LinkedList;

import pt.ipleiria.estg.dei.savergy.R;
import pt.ipleiria.estg.dei.savergy.adapters.RankingListAdapter;
import pt.ipleiria.estg.dei.savergy.model.Savergy;
import pt.ipleiria.estg.dei.savergy.model.User;

/**
 * Created by Trabalho on 11/06/2017.
 */

public class RankingTabFragment extends Fragment {

    View rootView;

    TextView TextView1stPlaceName;
    ImageView imageView1stPlace;
    TextView textView1stPlaceLabel;

    TextView TextView2ndPlaceName;
    ImageView imageView2ndPlace;
    TextView textView2ndPlaceLabel;

    TextView TextView3rdPlaceName;
    ImageView imageView3rdPlace;
    TextView textView3rdPlaceLabel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Bundle args = getArguments();
        ListView listViewTop10;
        RankingListAdapter adapter;

        switch (args.getInt("fragment")) {
            case 0:
                rootView = inflater.inflate(R.layout.fragment_ranking_tab, container, false);

                LinkedList<User> local_ranking = Savergy.INSTANCE.getRankingLocalList();

                TextView1stPlaceName = (TextView) rootView.findViewById(R.id.TextView1stPlaceName);
                TextView1stPlaceName.setVisibility(View.INVISIBLE);
                imageView1stPlace = (ImageView) rootView.findViewById(R.id.imageView1stPlace);
                imageView1stPlace.setVisibility(View.INVISIBLE);
                textView1stPlaceLabel = (TextView) rootView.findViewById(R.id.textView1stPlaceLabel);

                TextView2ndPlaceName = (TextView) rootView.findViewById(R.id.TextView2ndPlaceName);
                TextView2ndPlaceName.setVisibility(View.INVISIBLE);
                imageView2ndPlace = (ImageView) rootView.findViewById(R.id.imageView2ndPlace);
                imageView2ndPlace.setVisibility(View.INVISIBLE);
                textView2ndPlaceLabel = (TextView) rootView.findViewById(R.id.textView2ndPlaceLabel);

                TextView3rdPlaceName = (TextView) rootView.findViewById(R.id.TextView3rdPlaceName);
                TextView3rdPlaceName.setVisibility(View.INVISIBLE);
                imageView3rdPlace = (ImageView) rootView.findViewById(R.id.imageView3rdPlace);
                imageView3rdPlace.setVisibility(View.INVISIBLE);
                textView3rdPlaceLabel = (TextView) rootView.findViewById(R.id.textView3rdPlaceLabel);

                if(Savergy.INSTANCE.getRankingLocalList().size() > 0) {
                    TextView1stPlaceName.setVisibility(View.VISIBLE);
                    imageView1stPlace.setVisibility(View.VISIBLE);
                    if(local_ranking.get(0).getName().isEmpty()) {
                        TextView1stPlaceName.setText("Desconhecido");
                    } else {
                        TextView1stPlaceName.setText(local_ranking.get(0).getName());
                    }
                    imageView1stPlace.setImageDrawable(getImageDrawable(local_ranking.get(0).getAvatar()));
                }

                if(Savergy.INSTANCE.getRankingLocalList().size() > 1) {
                    TextView2ndPlaceName.setVisibility(View.VISIBLE);
                    imageView2ndPlace.setVisibility(View.VISIBLE);
                    if(local_ranking.get(1).getName().isEmpty()) {
                        TextView2ndPlaceName.setText("Desconhecido");
                    } else {
                        TextView2ndPlaceName.setText(local_ranking.get(1).getName());
                    }
                    imageView2ndPlace.setImageDrawable(getImageDrawable(local_ranking.get(1).getAvatar()));
                }

                if(Savergy.INSTANCE.getRankingLocalList().size() > 2) {
                    TextView3rdPlaceName.setVisibility(View.VISIBLE);
                    imageView3rdPlace.setVisibility(View.VISIBLE);
                    if(local_ranking.get(2).getName().isEmpty()) {
                        TextView3rdPlaceName.setText("Desconhecido");
                    } else {
                        TextView3rdPlaceName.setText(local_ranking.get(2).getName());
                    }
                    imageView3rdPlace.setImageDrawable(getImageDrawable(local_ranking.get(2).getAvatar()));
                }

                listViewTop10 = (ListView) rootView.findViewById(R.id.listViewTop10);
                listViewTop10.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        //Toast.makeText(AdvicesListActivity.this,id+"",Toast.LENGTH_SHORT).show();
                    }
                });
                adapter = new RankingListAdapter(getContext(), Savergy.INSTANCE.getRankingLocalList());
                listViewTop10.setAdapter(adapter);
                setListViewHeightBasedOnItems(listViewTop10);

                break;
            case 1:
                rootView = inflater.inflate(R.layout.fragment_ranking_tab, container, false);

                LinkedList<User> country_ranking = Savergy.INSTANCE.getRankingCountryList();

                TextView1stPlaceName = (TextView) rootView.findViewById(R.id.TextView1stPlaceName);
                TextView1stPlaceName.setVisibility(View.INVISIBLE);
                imageView1stPlace = (ImageView) rootView.findViewById(R.id.imageView1stPlace);
                imageView1stPlace.setVisibility(View.INVISIBLE);
                textView1stPlaceLabel = (TextView) rootView.findViewById(R.id.textView1stPlaceLabel);

                TextView2ndPlaceName = (TextView) rootView.findViewById(R.id.TextView2ndPlaceName);
                TextView2ndPlaceName.setVisibility(View.INVISIBLE);
                imageView2ndPlace = (ImageView) rootView.findViewById(R.id.imageView2ndPlace);
                imageView2ndPlace.setVisibility(View.INVISIBLE);
                textView2ndPlaceLabel = (TextView) rootView.findViewById(R.id.textView2ndPlaceLabel);

                TextView3rdPlaceName = (TextView) rootView.findViewById(R.id.TextView3rdPlaceName);
                TextView3rdPlaceName.setVisibility(View.INVISIBLE);
                imageView3rdPlace = (ImageView) rootView.findViewById(R.id.imageView3rdPlace);
                imageView3rdPlace.setVisibility(View.INVISIBLE);
                textView3rdPlaceLabel = (TextView) rootView.findViewById(R.id.textView3rdPlaceLabel);

                if(Savergy.INSTANCE.getRankingLocalList().size() > 0) {
                    TextView1stPlaceName.setVisibility(View.VISIBLE);
                    imageView1stPlace.setVisibility(View.VISIBLE);
                    if(country_ranking.get(0).getName().isEmpty()) {
                        TextView1stPlaceName.setText("Desconhecido");
                    } else {
                        TextView1stPlaceName.setText(country_ranking.get(0).getName());
                    }
                    imageView1stPlace.setImageDrawable(getImageDrawable(country_ranking.get(0).getAvatar()));
                }

                if(Savergy.INSTANCE.getRankingLocalList().size() > 1) {
                    TextView2ndPlaceName.setVisibility(View.VISIBLE);
                    imageView2ndPlace.setVisibility(View.VISIBLE);
                    if(country_ranking.get(1).getName().isEmpty()) {
                        TextView2ndPlaceName.setText("Desconhecido");
                    } else {
                        TextView2ndPlaceName.setText(country_ranking.get(1).getName());
                    }
                    imageView2ndPlace.setImageDrawable(getImageDrawable(country_ranking.get(1).getAvatar()));
                }

                if(Savergy.INSTANCE.getRankingLocalList().size() > 2) {
                    TextView3rdPlaceName.setVisibility(View.VISIBLE);
                    imageView3rdPlace.setVisibility(View.VISIBLE);
                    if(country_ranking.get(2).getName().isEmpty()) {
                        TextView3rdPlaceName.setText("Desconhecido");
                    } else {
                        TextView3rdPlaceName.setText(country_ranking.get(2).getName());
                    }
                    imageView3rdPlace.setImageDrawable(getImageDrawable(country_ranking.get(2).getAvatar()));
                }

                listViewTop10 = (ListView) rootView.findViewById(R.id.listViewTop10);
                listViewTop10.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        //Toast.makeText(AdvicesListActivity.this,id+"",Toast.LENGTH_SHORT).show();
                    }
                });
                adapter = new RankingListAdapter(getContext(), Savergy.INSTANCE.getRankingLocalList());
                listViewTop10.setAdapter(adapter);
                setListViewHeightBasedOnItems(listViewTop10);

                break;
            default:
                rootView = inflater.inflate(R.layout.fragment_ranking_tab, container, false);
        }

        return rootView;
    }

    private Drawable getImageDrawable(int avatarNum) {

        int avatar = R.drawable.no_avatar_masculino;

        switch (avatarNum) {
            case 1:
                avatar = R.drawable.avatar_01;
                break;
            case 2:
                avatar = R.drawable.avatar_02;
                break;
            case 3:
                avatar = R.drawable.avatar_03;
                break;
            case 4:
                avatar = R.drawable.avatar_04;
                break;
            case 5:
                avatar = R.drawable.avatar_05;
                break;
            case 6:
                avatar = R.drawable.avatar_06;
                break;
            case 7:
                avatar = R.drawable.avatar_07;
                break;
            case 8:
                avatar = R.drawable.avatar_08;
                break;
            case 9:
                avatar = R.drawable.avatar_09;
                break;
            case 10:
                avatar = R.drawable.avatar_010;
                break;
            case 11:
                avatar = R.drawable.avatar_011;
                break;
            case 12:
                avatar = R.drawable.avatar_012;
                break;
        }

        Drawable d = rootView.getResources().getDrawable(avatar);
        return d;
    }

    public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();

            return true;

        } else {
            return false;
        }

    }

}

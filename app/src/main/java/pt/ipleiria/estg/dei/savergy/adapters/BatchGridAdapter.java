package pt.ipleiria.estg.dei.savergy.adapters;

/**
 * Created by Trabalho on 01/04/2017.
 */

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;

import java.util.Iterator;

import pt.ipleiria.estg.dei.savergy.R;
import pt.ipleiria.estg.dei.savergy.model.Batch;

public class BatchGridAdapter extends ArrayAdapter<Batch> {

    private static final class ViewHolder {
        public TextView textViewBatchScore;
        public TextView textViewBatchName;
        public TextView textViewBatchQuestions;
        public RoundCornerProgressBar progress_questions;

        public ViewHolder(View view) {
            textViewBatchScore = (TextView) view.findViewById(R.id.textViewBatchScore);
            textViewBatchName = (TextView) view.findViewById(R.id.textViewBatchName);
            progress_questions = (RoundCornerProgressBar) view.findViewById(R.id.progress_questions);
        }

        public void update(Batch batch) {
            textViewBatchScore.setText("Pontuação: " + batch.getScore());
            textViewBatchName.setText(batch.getBatch());
            progress_questions.setProgressColor(Color.parseColor("#F7A42B"));
            progress_questions.setProgressBackgroundColor(Color.parseColor("#808080"));
            progress_questions.setMax(batch.getNquestions());
            progress_questions.setProgress(batch.getNanswered());

            textViewBatchName.setHeight(120);
        }
    }

    public BatchGridAdapter(Context context, Iterable<Batch> objects) {
        this(context, objects.iterator());
    }

    public BatchGridAdapter(Context context, Iterator<Batch> objects) {
        super(context, R.layout.batch_grid_item, R.id.textViewBatchName);
        while (objects.hasNext()) {
            add(objects.next());
        }

    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        Batch batch = getItem(position);

        ViewHolder holder = (ViewHolder)view.getTag();

        if (holder==null) {
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        holder.update(batch);

        return view;
    }


    @Override
    public long getItemId(int position) {
        return getItem(position).getServerid();
    }

}

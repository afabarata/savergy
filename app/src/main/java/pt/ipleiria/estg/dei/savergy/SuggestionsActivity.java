package pt.ipleiria.estg.dei.savergy;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

import pt.ipleiria.estg.dei.savergy.helpers.LoginHelper;
import pt.ipleiria.estg.dei.savergy.helpers.NetworkUtility;
import pt.ipleiria.estg.dei.savergy.model.Savergy;
import pt.ipleiria.estg.dei.savergy.server.CreateSuggestionTask;

/**
 * Created by Trabalho on 27/05/2017.
 */

public class SuggestionsActivity extends AppCompatActivity {

    private ActionMenuView defaultMenu;
    private ActionMenuView defaultMenuBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggestions);

        Toolbar t = (Toolbar) findViewById(R.id.tToolbar);
        defaultMenu = (ActionMenuView) t.findViewById(R.id.defaultMenu);
        defaultMenuBack = (ActionMenuView) t.findViewById(R.id.defaultMenuBack);
        defaultMenu.setOnMenuItemClickListener(new ActionMenuView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                return onOptionsItemSelected(menuItem);
            }
        });
        defaultMenuBack.setOnMenuItemClickListener(new ActionMenuView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                return onOptionsItemSelected(menuItem);
            }
        });
        setSupportActionBar(t);
        getSupportActionBar().setTitle(null);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        //inflater.inflate(R.menu.menu_profile, defaultMenu.getMenu());
        inflater.inflate(R.menu.menu_return, defaultMenuBack.getMenu());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        long id=item.getItemId();

        if (id == R.id.menuReturnActivity) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClickSendSuggestion(View view) {

        Spinner suggestionsType = (Spinner) findViewById(R.id.SuggestionsType);
        EditText editTextSuggestion = (EditText) findViewById(R.id.editTextSuggestion);

        if((!suggestionsType.getSelectedItem().toString().equals("") && !suggestionsType.getSelectedItem().toString().isEmpty())
                && !editTextSuggestion.getText().toString().equals("") && !editTextSuggestion.getText().toString().isEmpty()) {

            if(NetworkUtility.hasConnectivity(this)) {
                CreateSuggestionTask createSuggestionTask = new CreateSuggestionTask();
                createSuggestionTask.execute(Savergy.INSTANCE.getAPI_TOKEN(),suggestionsType.getSelectedItem().toString(),editTextSuggestion.getText().toString());
                finish();
            } else {
                Toast.makeText(this,"É necessário acesso à internet para enviar a sugestão!",Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this,"Tem de preencher todos os campos antes de enviar",Toast.LENGTH_SHORT).show();
        }
    }
}

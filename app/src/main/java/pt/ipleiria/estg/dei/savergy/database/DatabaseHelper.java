package pt.ipleiria.estg.dei.savergy.database;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.Locale;

import pt.ipleiria.estg.dei.savergy.model.Advice;
import pt.ipleiria.estg.dei.savergy.model.Answer;
import pt.ipleiria.estg.dei.savergy.model.Batch;
import pt.ipleiria.estg.dei.savergy.model.Equipment;
import pt.ipleiria.estg.dei.savergy.model.EquipmentRecord;
import pt.ipleiria.estg.dei.savergy.model.Question;
import pt.ipleiria.estg.dei.savergy.model.Savergy;
import pt.ipleiria.estg.dei.savergy.model.User;

/**
 * Created by Trabalho on 12/03/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "LocalDB";

    private static final String DICTIONARY_USER_TABLE_NAME = "user";
    private static final String DICTIONARY_USER_TABLE_CREATE = "CREATE TABLE " + DICTIONARY_USER_TABLE_NAME
            + " (" + " id INTEGER PRIMARY KEY AUTOINCREMENT,"
            + "deviceid VARCHAR2(30) UNIQUE, "
            + "socialid VARCHAR2(100), "
            + "email VARCHAR2(30), "
            + "name VARCHAR2(50), "
            + "avatar INTEGER, "
            + "country VARCHAR2(100), "
            + "district VARCHAR2(100), "
            + "county VARCHAR2(100), "
            + "birthdate DATE, "
            + "gender VARCHAR2(30), "
            + "household INTEGER, "
            + "last_sync DATETIME DEFAULT CURRENT_TIMESTAMP "+");";

    /*private static final String DICTIONARY_USER_INFO_TABLE_NAME = "user_info";
    private static final String DICTIONARY_USER_INFO_TABLE_CREATE = "CREATE TABLE " + DICTIONARY_USER_INFO_TABLE_NAME
            + " (" + " id INTEGER PRIMARY KEY AUTOINCREMENT,"
            + "deviceid VARCHAR2(30), "
            + "address VARCHAR2(500), "
            + "country VARCHAR2(100), "
            + "district VARCHAR2(100), "
            + "county VARCHAR2(100), "
            + "zipcode VARCHAR2(100), "
            + "phonenumber INTEGER, "
            + "birthdate DATE, "
            + "gender INTEGER " +");";*/

    /* BATCHES */

    private static final String DICTIONARY_BATCH_TABLE_NAME = "batch";
    private static final String DICTIONARY_BATCH_TABLE_CREATE = "CREATE TABLE " + DICTIONARY_BATCH_TABLE_NAME
            + " (" + " id INTEGER PRIMARY KEY AUTOINCREMENT,"
            + "serverid INTEGER UNIQUE,"
            + "batch TEXT NOT NULL, "
            + "nquestions INTEGER DEFAULT 0, "
            + "maxscore INTEGER DEFAULT 0, "
            + "score INTEGER DEFAULT 0, "
            + "nanswered INTEGER DEFAULT 0" +");";

    private static final String DICTIONARY_QUESTION_TABLE_NAME = "question";
    private static final String DICTIONARY_QUESTION_TABLE_CREATE = "CREATE TABLE " + DICTIONARY_QUESTION_TABLE_NAME
            + " (" + " id INTEGER PRIMARY KEY AUTOINCREMENT,"
            + "serverid INTEGER, "
            + "batchid INTEGER, "
            + "question TEXT NOT NULL, "
            + "answer_type TEXT NOT NULL, "
            + "answered INTEGER DEFAULT 0" +");";

    private static final String DICTIONARY_QUESTION_INDEX = "CREATE UNIQUE INDEX question_index ON "+ DICTIONARY_QUESTION_TABLE_NAME +" (BATCHID, SERVERID);";

    private static final String DICTIONARY_ANSWER_TABLE_NAME = "answer";
    private static final String DICTIONARY_ANSWER_TABLE_CREATE = "CREATE TABLE " + DICTIONARY_ANSWER_TABLE_NAME
            + " (" + " id INTEGER PRIMARY KEY AUTOINCREMENT,"
            + "serverid INTEGER, "
            + "questionid INTEGER, "
            + "answer TEXT NOT NULL, "
            + "advise TEXT, "
            + "points INTEGER, "
            + "selected INTEGER DEFAULT 0" +");";

    private static final String DICTIONARY_ANSWER_INDEX = "CREATE UNIQUE INDEX answer_index ON "+ DICTIONARY_ANSWER_TABLE_NAME +" (QUESTIONID, SERVERID);";

    private static final String DICTIONARY_USER_ANSWER_TABLE_NAME = "user_answer";
    private static final String DICTIONARY_USER_ANSWER_TABLE_CREATE = "CREATE TABLE " + DICTIONARY_USER_ANSWER_TABLE_NAME
            + " (" + " id INTEGER PRIMARY KEY AUTOINCREMENT,"
            + "deviceid VARCHAR2(30), "
            + "questionid INTEGER, "
            + "answerid INTEGER, "
            + "sended INTEGER DEFAULT 0" +");";

    private static final String DICTIONARY_USER_ANSWER_INDEX = "CREATE UNIQUE INDEX user_answer_index ON "+ DICTIONARY_USER_ANSWER_TABLE_NAME +" (QUESTIONID, ANSWERID);";

    /* EQUIPMENTS */

    private static final String DICTIONARY_EQUIPMENT_TABLE_NAME = "equipment";
    private static final String DICTIONARY_EQUIPMENT_TABLE_CREATE = "CREATE TABLE " + DICTIONARY_EQUIPMENT_TABLE_NAME
            + " (" + " id INTEGER PRIMARY KEY AUTOINCREMENT,"
            + "serverid INTEGER UNIQUE, "
            + "name TEXT NOT NULL, "
            + "icon TEXT NOT NULL, "
            + "description TEXT, "
            + "isSelected INTEGER DEFAULT 0, "
            + "defaultUseTime INTEGER, "
            + "defaultPower INTEGER, "
            + "configuredUseTime INTEGER DEFAULT 0, "
            + "configuredUseTimeUnits TEXT,"
            + "configuredPower INTEGER DEFAULT 0 " +");";

    private static final String DICTIONARY_EQUIPMENT_RECORD_TABLE_NAME = "equipment_record";
    private static final String DICTIONARY_EQUIPMENT_RECORD_TABLE_CREATE = "CREATE TABLE " + DICTIONARY_EQUIPMENT_RECORD_TABLE_NAME
            + " (" + " id INTEGER PRIMARY KEY AUTOINCREMENT,"
            + "deviceid VARCHAR2(30), "
            + "equipmentid INTEGER, "
            + "useTime INTEGER, "
            + "power INTEGER, "
            + "created_at DATETIME DEFAULT CURRENT_TIMESTAMP, "
            + "sended INTEGER DEFAULT 0" +");";

    /* ADVICES */

    private static final String DICTIONARY_ADVICE_TABLE_NAME = "advice";
    private static final String DICTIONARY_ADVICE_TABLE_CREATE = "CREATE TABLE " + DICTIONARY_ADVICE_TABLE_NAME
            + " (" + " id INTEGER PRIMARY KEY AUTOINCREMENT,"
            + "serverid INTEGER UNIQUE, "
            + "advice TEXT NOT NULL, "
            + "isOld INTEGER DEFAULT 0" + ");";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DICTIONARY_USER_TABLE_CREATE);
        db.execSQL(DICTIONARY_BATCH_TABLE_CREATE);
        db.execSQL(DICTIONARY_QUESTION_TABLE_CREATE);
        db.execSQL(DICTIONARY_QUESTION_INDEX);
        db.execSQL(DICTIONARY_ANSWER_TABLE_CREATE);
        db.execSQL(DICTIONARY_ANSWER_INDEX);
        db.execSQL(DICTIONARY_USER_ANSWER_TABLE_CREATE);
        db.execSQL(DICTIONARY_USER_ANSWER_INDEX);
        db.execSQL(DICTIONARY_EQUIPMENT_TABLE_CREATE);
        db.execSQL(DICTIONARY_EQUIPMENT_RECORD_TABLE_CREATE);
        db.execSQL(DICTIONARY_ADVICE_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion == oldVersion + 1) {
            //do database related changes.
        }
    }

    public void insertUser(User user){
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteStatement stmt = db.compileStatement("REPLACE INTO " + DICTIONARY_USER_TABLE_NAME + " (deviceid, socialid, email, name, avatar, country, district, county, " +
                "birthdate, gender, household, last_sync) " + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
        stmt.bindString(1, user.getDeviceId());
        stmt.bindString(2, user.getSocialId());
        stmt.bindString(3, user.getEmail());
        stmt.bindString(4, user.getName());
        stmt.bindDouble(5, user.getAvatar());
        stmt.bindString(6, user.getCountry());
        stmt.bindString(7, user.getDistrict());
        stmt.bindString(8, user.getCounty());
        stmt.bindString(9, user.getBirthdate());
        stmt.bindString(10, user.getGender());
        stmt.bindDouble(11, user.getHousehold());
        stmt.bindString(12, user.getLast_sync());
        stmt.execute();
        stmt.close();
        db.close();
    }

    public void insertBatches(LinkedList<Batch> batches) {
        SQLiteDatabase db = this.getWritableDatabase();

        for (Batch c: batches)
        {

            SQLiteStatement stmtb = db.compileStatement("REPLACE INTO " + DICTIONARY_BATCH_TABLE_NAME + " (serverid, batch, nquestions, maxscore, score, nanswered) " + "VALUES (?,?,?,?,?,?)");
            stmtb.bindDouble(1, c.getServerid());
            stmtb.bindString(2, c.getBatch());
            stmtb.bindDouble(3, c.getNquestions());
            stmtb.bindDouble(4, c.getMaxscore());
            stmtb.bindDouble(5, c.getScore());
            stmtb.bindDouble(6, c.getNanswered());
            stmtb.execute();
            stmtb.close();

            for (Question q : c.getQuestions()) {
                SQLiteStatement stmtq = db.compileStatement("REPLACE INTO " + DICTIONARY_QUESTION_TABLE_NAME + " (serverid, batchid, question, answer_type, answered) " + "VALUES (?,?,?,?,?)");
                stmtq.bindDouble(1, q.getServerid());
                stmtq.bindDouble(2, q.getBatchid());
                stmtq.bindString(3, q.getQuestion());
                stmtq.bindString(4, q.getAnswer_type());
                stmtq.bindDouble(5, q.getAnswered());
                stmtq.execute();
                stmtq.close();

                for (Answer a : q.getAnswers()) {
                    SQLiteStatement stmta = db.compileStatement("REPLACE INTO " + DICTIONARY_ANSWER_TABLE_NAME + " (serverid, questionid, answer, advise, points, selected) " + "VALUES (?,?,?,?,?,?)");
                    stmta.bindDouble(1, a.getServerid());
                    stmta.bindDouble(2, a.getQuestionid());
                    stmta.bindString(3, a.getAnswer());
                    stmta.bindString(4, a.getAdvise());
                    stmta.bindDouble(5, a.getPoints());
                    stmta.bindDouble(6, a.getSelected());
                    stmta.execute();
                    stmta.close();

                }
            }
        }

        db.close();
    }

    public void insertEquipmentRecord(String deviceID, int equipmentID, int useTime, int power){
        SQLiteDatabase db = this.getWritableDatabase();

        SQLiteStatement stmt = db.compileStatement("INSERT INTO " + DICTIONARY_EQUIPMENT_RECORD_TABLE_NAME + " (deviceid, equipmentid, useTime, power) " + "VALUES (?,?,?,?)");
        stmt.bindString(1, deviceID);
        stmt.bindDouble(2, equipmentID);
        stmt.bindDouble(3, useTime);
        stmt.bindDouble(4, power);
        stmt.execute();
        stmt.close();

        db.close();
    }

    public void insertEquipments(LinkedList<Equipment> equipments){
        SQLiteDatabase db = this.getWritableDatabase();

        for(Equipment e: equipments) {
            SQLiteStatement stmt = db.compileStatement("REPLACE INTO " + DICTIONARY_EQUIPMENT_TABLE_NAME + " (serverid, name, icon, description, isSelected, defaultUseTime, defaultPower, configuredUseTime, configuredUseTimeUnits, configuredPower) " + "VALUES (?,?,?,?,?,?,?,?,?,?)");
            stmt.bindDouble(1, e.getServerid());
            stmt.bindString(2, e.getName());
            stmt.bindString(3, e.getIcon());
            stmt.bindString(4, e.getDescription());
            stmt.bindDouble(5, e.getIsSelected());
            stmt.bindDouble(6, e.getDefaultUseTime());
            stmt.bindDouble(7, e.getDefaultPower());
            stmt.bindDouble(8, e.getConfiguredUseTime());
            stmt.bindString(9, e.getConfiguredUseTimeUnits());
            stmt.bindDouble(10, e.getConfiguredPower());
            stmt.execute();
            stmt.close();
        }

        db.close();
    }

    public void insertAdvices(LinkedList<Advice> advices){
        SQLiteDatabase db = this.getWritableDatabase();

        for(Advice a: advices) {
            SQLiteStatement stmt = db.compileStatement("REPLACE INTO " + DICTIONARY_ADVICE_TABLE_NAME + " (serverid, advice, isOld) " + "VALUES (?,?,?)");
            stmt.bindDouble(1, a.getServerid());
            stmt.bindString(2, a.getAdvice());
            stmt.bindDouble(3, a.getIsOld());
            stmt.execute();
            stmt.close();
        }

        db.close();
    }

    public void updateUser(User user){
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteStatement stmt = db.compileStatement("UPDATE " + DICTIONARY_USER_TABLE_NAME + " SET socialid = ?, email = ?, name = ?, avatar = ?, country = ?, district = ?, county = ?, " +
                "birthdate = ?, gender = ?, household = ?" + "WHERE deviceid = ?");
        stmt.bindString(1, user.getSocialId());
        stmt.bindString(2, user.getEmail());
        stmt.bindString(3, user.getName());
        stmt.bindDouble(4, user.getAvatar());
        stmt.bindString(5, user.getCountry());
        stmt.bindString(6, user.getDistrict());
        stmt.bindString(7, user.getCounty());
        stmt.bindString(8, user.getBirthdate());
        stmt.bindString(9, user.getGender());
        stmt.bindDouble(10, user.getHousehold());
        stmt.bindString(11, user.getDeviceId());
        stmt.execute();
        stmt.close();
        db.close();
    }

    public void updateQuestionByBatchID(Long userDeviceId, long categoryid, LinkedList<Question> questions) {
        SQLiteDatabase db = this.getWritableDatabase();
        int num_answered = 0;
        int score = 0;

        for (Question q: questions) {
            if(q.getAnswered() == 1) {
                SQLiteStatement stmt = db.compileStatement("UPDATE " + DICTIONARY_QUESTION_TABLE_NAME + " SET answered=? " + "WHERE serverid = ?");
                stmt.bindLong(1, q.getAnswered());
                stmt.bindLong(2, q.getServerid());
                stmt.execute();
                stmt.close();
                num_answered++;
            }
            for (Answer a: q.getAnswers()) {
                if(a.getSelected() == 1) {
                    SQLiteStatement stmt = db.compileStatement("UPDATE " + DICTIONARY_ANSWER_TABLE_NAME + " SET selected=? " + "WHERE serverid = ?");
                    stmt.bindLong(1, a.getSelected());
                    stmt.bindLong(2, a.getServerid());
                    stmt.execute();
                    stmt.close();
                    score += a.getPoints();

                    if(!existsByUserAnswerAnswerID(q.getServerid(),a.getServerid())) {
                        SQLiteStatement stmtau = db.compileStatement("INSERT INTO " + DICTIONARY_USER_ANSWER_TABLE_NAME + " (deviceid, questionid, answerid, sended) " + "VALUES (?,?,?,?)");
                        stmtau.bindLong(1, userDeviceId);
                        stmtau.bindDouble(2, q.getServerid());
                        stmtau.bindDouble(3, a.getServerid());
                        stmtau.bindDouble(4, 0);
                        stmtau.execute();
                        stmtau.close();
                    }
                }
            }
        }

        SQLiteStatement stmt = db.compileStatement("UPDATE " + DICTIONARY_BATCH_TABLE_NAME + " SET score = ?, nanswered = ? " + "WHERE serverid = ?");
        stmt.bindLong(1, score);
        stmt.bindLong(2, num_answered);
        stmt.bindLong(3, categoryid);
        stmt.execute();
        stmt.close();

        db.close();
    }

    public User getUserByDeviceId(String deviceid) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT id, socialid, email, name, avatar, country, district, county, birthdate, gender, household, last_sync FROM "
                + DICTIONARY_USER_TABLE_NAME + " WHERE deviceid = ?";
        Cursor cursor = db.rawQuery(query, new String[] {deviceid});
        cursor.moveToFirst();

        User user = new User(deviceid);
        user.setId(cursor.getInt(0));
        user.setSocialId(cursor.getString(1));
        user.setEmail(cursor.getString(2));
        user.setName(cursor.getString(3));
        user.setAvatar(cursor.getInt(4));
        user.setCountry(cursor.getString(5));
        user.setDistrict(cursor.getString(6));
        user.setCounty(cursor.getString(7));
        user.setBirthdate(cursor.getString(8));
        user.setGender(cursor.getString(9));
        user.setHousehold(cursor.getInt(10));
        user.setLast_sync(cursor.getString(11));

        db.close();

        return user;
    }

    public LinkedList<Batch> getBatchesList() {
        LinkedList<Batch> batches = new LinkedList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        String query_c = "SELECT id, serverid, batch, nquestions, maxscore, score, nanswered from " + DICTIONARY_BATCH_TABLE_NAME + " WHERE serverid != 1 ORDER BY id ASC";
        Cursor cursor_c = db.rawQuery(query_c, null);
        while (cursor_c.moveToNext()) {
            Batch batch = new Batch(cursor_c.getInt(1),cursor_c.getString(2),cursor_c.getInt(3),cursor_c.getInt(4));
            batch.setScore(cursor_c.getInt(5));
            batch.setNanswered(cursor_c.getInt(6));

            LinkedList<Question> questions = new LinkedList<>();
            String query_q = "SELECT id, serverid, batchid, question, answer_type, answered from " + DICTIONARY_QUESTION_TABLE_NAME + " WHERE batchid = ? ORDER BY id ASC";
            Cursor cursor_q = db.rawQuery(query_q, new String[] {batch.getServerid()+""});
            while (cursor_q.moveToNext()) {
                Question question = new Question(cursor_q.getInt(1),cursor_q.getInt(2),cursor_q.getString(3),cursor_q.getString(4));
                question.setAnswered(cursor_q.getInt(5));

                LinkedList<Answer> answers = new LinkedList<>();
                String query_a = "SELECT id, serverid, questionid, answer, advise, points, selected from " + DICTIONARY_ANSWER_TABLE_NAME + " WHERE questionid = ? ORDER BY id ASC";
                Cursor cursor_a = db.rawQuery(query_a, new String[] {question.getServerid()+""});
                while (cursor_a.moveToNext()) {
                    Answer answer = new Answer(cursor_a.getInt(1),cursor_a.getInt(2),cursor_a.getString(3),cursor_a.getString(4),cursor_a.getInt(5));
                    answer.setSelected(cursor_a.getInt(6));
                    answers.add(answer);
                }
                question.setAnswers(answers);
                questions.add(question);
            }
            batch.setQuestions(questions);
            batches.add(batch);
        }
        db.close();

        /*for (Batch c: batches) {
            Log.d("BATCHES NO SINGLETON",c.toString());
        }*/

        return batches;
    }

    public Batch getProfileQuestions() {
        Batch batch;

        SQLiteDatabase db = this.getWritableDatabase();
        String query_c = "SELECT id, serverid, batch, nquestions, maxscore, score, nanswered from " + DICTIONARY_BATCH_TABLE_NAME + " WHERE serverid == 1 ORDER BY id ASC";
        Cursor cursor_c = db.rawQuery(query_c, null);
        cursor_c.moveToFirst();

        batch = new Batch(cursor_c.getInt(1),cursor_c.getString(2),cursor_c.getInt(3),cursor_c.getInt(4));
        batch.setScore(cursor_c.getInt(5));
        batch.setNanswered(cursor_c.getInt(6));

        LinkedList<Question> questions = new LinkedList<>();
        String query_q = "SELECT id, serverid, batchid, question, answer_type, answered from " + DICTIONARY_QUESTION_TABLE_NAME + " WHERE batchid = ? ORDER BY id ASC";
        Cursor cursor_q = db.rawQuery(query_q, new String[] {batch.getServerid()+""});
        while (cursor_q.moveToNext()) {
            Question question = new Question(cursor_q.getInt(1),cursor_q.getInt(2),cursor_q.getString(3),cursor_q.getString(4));
            question.setAnswered(cursor_q.getInt(5));

            LinkedList<Answer> answers = new LinkedList<>();
            String query_a = "SELECT id, serverid, questionid, answer, advise, points, selected from " + DICTIONARY_ANSWER_TABLE_NAME + " WHERE questionid = ? ORDER BY id ASC";
            Cursor cursor_a = db.rawQuery(query_a, new String[] {question.getServerid()+""});
            while (cursor_a.moveToNext()) {
                Answer answer = new Answer(cursor_a.getInt(1),cursor_a.getInt(2),cursor_a.getString(3),cursor_a.getString(4),cursor_a.getInt(5));
                answer.setSelected(cursor_a.getInt(6));
                answers.add(answer);
            }
            question.setAnswers(answers);
            questions.add(question);
        }
        batch.setQuestions(questions);
        db.close();

        Log.d("PROFILE Q. NO SINGLETON",batch.toString());

        return batch;
    }

    public LinkedList<Equipment> getEquipamentsList() {
        LinkedList<Equipment> equipments = new LinkedList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        String query_e = "SELECT id, serverid, name, icon, description, isSelected, defaultUseTime, defaultPower, configuredUseTime, configuredUseTimeUnits, configuredPower from " + DICTIONARY_EQUIPMENT_TABLE_NAME + " ORDER BY id ASC";
        Cursor cursor_e = db.rawQuery(query_e, null);
        while (cursor_e.moveToNext()) {
            Equipment equipment = new Equipment(cursor_e.getInt(1),cursor_e.getString(2),cursor_e.getString(3),cursor_e.getString(4),cursor_e.getInt(5),cursor_e.getInt(6),cursor_e.getInt(7),cursor_e.getInt(8),cursor_e.getInt(10));
            equipment.setConfiguredUseTimeUnits(cursor_e.getString(9));
            equipments.add(equipment);
        }
        db.close();

        for (Equipment e: equipments) {
            Log.d("EQUIPMENTS NO SINGLETON",e.toString());
        }

        return equipments;
    }

    public LinkedList<Equipment> getSelectedEquipamentsList() {
        LinkedList<Equipment> equipments = new LinkedList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        String query_e = "SELECT id, serverid, name, icon, description, isSelected, defaultUseTime, defaultPower, configuredUseTime, configuredUseTimeUnits, configuredPower from " + DICTIONARY_EQUIPMENT_TABLE_NAME + " WHERE isSelected = 1 ORDER BY id ASC";
        Cursor cursor_e = db.rawQuery(query_e, null);
        while (cursor_e.moveToNext()) {
            Equipment equipment = new Equipment(cursor_e.getInt(1),cursor_e.getString(2),cursor_e.getString(3),cursor_e.getString(4),cursor_e.getInt(5),cursor_e.getInt(6),cursor_e.getInt(7),cursor_e.getInt(8),cursor_e.getInt(10));
            equipment.setConfiguredUseTimeUnits(cursor_e.getString(9));
            equipments.add(equipment);
        }
        db.close();

        for (Equipment e: equipments) {
            Log.d("EQUIPS SELECTED NO SING",e.toString());
        }

        return equipments;
    }

    public LinkedList<EquipmentRecord> getEquipmentRecords() {
        LinkedList<EquipmentRecord> equipmentsRecords = new LinkedList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * from " + DICTIONARY_EQUIPMENT_RECORD_TABLE_NAME + " ORDER BY id ASC";
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            EquipmentRecord record = new EquipmentRecord(cursor.getInt(2),cursor.getString(5),cursor.getInt(3),cursor.getInt(4));
            equipmentsRecords.add(record);
        }
        db.close();

        return equipmentsRecords;
    }

    public int getDailyRecordsAmount(String date) {
        int count = 0;

        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT Count(*) FROM " + DICTIONARY_EQUIPMENT_RECORD_TABLE_NAME + " WHERE created_at LIKE '" + date +"%'";
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        count = cursor.getInt(0);

        return count;
    }

    public int getEquipmentRecordAmount() {
        int count = 0;

        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT Count(*) FROM " + DICTIONARY_EQUIPMENT_RECORD_TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        count = cursor.getInt(0);

        return count;
    }

    public LinkedList<Advice> getAdvicesList() {
        LinkedList<Advice> advices = new LinkedList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        String query_a = "SELECT id, serverid, advice, isOld from " + DICTIONARY_ADVICE_TABLE_NAME + " ORDER BY id DESC";
        Cursor cursor_a = db.rawQuery(query_a, null);
        while (cursor_a.moveToNext()) {
            Advice advice = new Advice(cursor_a.getInt(1),cursor_a.getString(2));
            advice.setIsOld(cursor_a.getInt(3));
            advices.add(advice);
        }
        db.close();

        for (Advice a: advices) {
            Log.d("ADVICES NO SINGLETON",a.toString());
        }

        return advices;
    }

    public boolean existsByDeviceId(String deviceid) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + DICTIONARY_USER_TABLE_NAME + " WHERE deviceid = ?";
        Cursor cursor = db.rawQuery(query, new String[] {deviceid});

        return cursor.moveToFirst();
    }

    public boolean existsBatches() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + DICTIONARY_USER_TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);

        return cursor.moveToFirst();
    }

    public boolean existsByUserAnswerAnswerID(int questionid, int answserid) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT id, deviceid, questionid, answerid, sended FROM " + DICTIONARY_USER_ANSWER_TABLE_NAME + " WHERE questionid = ? AND answerid = ?";
        Cursor cursor = db.rawQuery(query, new String[] {String.valueOf(questionid),String.valueOf(answserid)});

        return cursor.moveToFirst();
    }

    public void toStringUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT id, deviceid, socialid, email, name, avatar, country, district, county, birthdate, gender, household, last_sync from " + DICTIONARY_USER_TABLE_NAME + " ORDER BY id ASC";
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            Log.d("START USER: ", "----------------------");
            Log.d("ID: ", cursor.getInt(0)+"");
            Log.d("Device ID: ", cursor.getString(1));
            Log.d("Social ID: ", cursor.getString(2));
            Log.d("Email: ", cursor.getString(3));
            Log.d("Name: ", cursor.getString(4));
            Log.d("Avatar: ", cursor.getInt(5)+"");
            Log.d("Country: ", cursor.getString(6));
            Log.d("District: ", cursor.getString(7));
            Log.d("County: ", cursor.getString(8));
            Log.d("Birthdate: ", cursor.getString(9));
            Log.d("Gender: ", cursor.getString(10));
            Log.d("Household: ", cursor.getInt(11)+"");
            Log.d("Last_sync: ", cursor.getString(12));
            Log.d("END USER: ", "----------------------");
        }
        db.close();
    }

    public void toStringBatches() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * from " + DICTIONARY_BATCH_TABLE_NAME + " ORDER BY id ASC";
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            Log.d("START CATEGORY: ", "----------------------");
            Log.d("ID: ", cursor.getInt(0)+"");
            Log.d("Server ID: ", cursor.getInt(1)+"");
            Log.d("Batch: ", cursor.getString(2));
            Log.d("NºQuestões: ", cursor.getInt(3)+"");
            Log.d("NºMax Score: ", cursor.getInt(3)+"");
            Log.d("Score: ", cursor.getInt(4)+"");
            Log.d("NºRespondidas: ", cursor.getInt(5)+"");
            Log.d("END CATEGORY: ", "----------------------");
        }
        db.close();
    }

    public void toStringQuestions() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * from " + DICTIONARY_QUESTION_TABLE_NAME + " ORDER BY id ASC";
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            Log.d("START QUESTION: ", "----------------------");
            Log.d("ID: ", cursor.getInt(0)+"");
            Log.d("Server ID: ", cursor.getInt(1)+"");
            Log.d("Batch ID: ", cursor.getInt(2)+"");
            Log.d("Questão: ", cursor.getString(3));
            Log.d("Tipo de Questão: ", cursor.getString(4));
            Log.d("Respondida: ", cursor.getInt(5)+"");
            Log.d("END QUESTION: ", "----------------------");
        }
        db.close();
    }

    public void toStringAnswers() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * from " + DICTIONARY_ANSWER_TABLE_NAME + " ORDER BY id ASC";
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            Log.d("START ANSWER: ", "----------------------");
            Log.d("ID: ", cursor.getInt(0)+"");
            Log.d("Server ID: ", cursor.getInt(1)+"");
            Log.d("Question ID: ", cursor.getInt(2)+"");
            Log.d("Resposta: ", cursor.getString(3));
            Log.d("Conselho: ", cursor.getString(4));
            Log.d("Pontos: ", cursor.getInt(5)+"");
            Log.d("Selecionada: ", cursor.getInt(6)+"");
            Log.d("END ANSWER: ", "----------------------");
        }
        db.close();
    }

    public void toStringUserAnswers() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * from " + DICTIONARY_USER_ANSWER_TABLE_NAME + " ORDER BY id ASC";
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            Log.d("START USER ANSWERS: ", "----------------------");
            Log.d("ID: ", cursor.getInt(0)+"");
            Log.d("Device ID: ", cursor.getInt(1)+"");
            Log.d("Question ID: ", cursor.getInt(2)+"");
            Log.d("Answer ID: ", cursor.getInt(3)+"");
            Log.d("Sended: ", cursor.getInt(4)+"");
            Log.d("END USER ANSWERS: ", "----------------------");
        }
        db.close();
    }

    public void toStringEquipments() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * from " + DICTIONARY_EQUIPMENT_TABLE_NAME + " ORDER BY id ASC";
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            Log.d("START EQUIPMENT: ", "----------------------");
            Log.d("ID: ", cursor.getInt(0)+"");
            Log.d("Server ID: ", cursor.getInt(1)+"");
            Log.d("Nome: ", cursor.getString(2));
            Log.d("Icon: ", cursor.getString(3));
            Log.d("Descrição: ", cursor.getString(4));
            Log.d("Selecionado: ", cursor.getInt(5)+"");
            Log.d("defaultUseTime: ", cursor.getInt(6)+"");
            Log.d("defaultUsePower: ", cursor.getInt(7)+"");
            Log.d("configuredUseTime: ", cursor.getInt(8)+"");
            Log.d("configuredUseTimeUnits:", cursor.getString(9));
            Log.d("configuredUsePower: ", cursor.getInt(10)+"");
            Log.d("END EQUIPMENT: ", "----------------------");
        }
        db.close();
    }

    public void toStringEquipmentRecords() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * from " + DICTIONARY_EQUIPMENT_RECORD_TABLE_NAME + " ORDER BY id ASC";
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            Log.d("START EQUIP. RECORDS: ", "------------------");
            Log.d("ID: ", cursor.getInt(0)+"");
            Log.d("Device ID: ", cursor.getInt(1)+"");
            Log.d("Equipment ID: ", cursor.getInt(2)+"");
            Log.d("UseTime ID: ", cursor.getInt(3)+"");
            Log.d("Power: ", cursor.getInt(4)+"");
            Log.d("Created_at: ", cursor.getString(5));
            Log.d("END EQUIP. RECORDS: ", "----------------------");
        }
        db.close();
    }

    public void deleteUsers(){
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteStatement stmt = db.compileStatement("DELETE FROM " + DICTIONARY_USER_TABLE_NAME);
        stmt.execute();
        stmt.close();
        db.close();
    }

    public void deleteBatches() {
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteStatement stmt = db.compileStatement("DELETE FROM " + DICTIONARY_BATCH_TABLE_NAME);
        stmt.execute();
        stmt.close();
        db.close();
    }

    public void deleteEquipments() {
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteStatement stmt = db.compileStatement("DELETE FROM " + DICTIONARY_EQUIPMENT_TABLE_NAME);
        stmt.execute();
        stmt.close();
        db.close();
    }

    public void deleteAdvices() {
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteStatement stmt = db.compileStatement("DELETE FROM " + DICTIONARY_ADVICE_TABLE_NAME);
        stmt.execute();
        stmt.close();
        db.close();
    }

    public void dropUserTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteStatement stmt = db.compileStatement("DROP TABLE " + DICTIONARY_USER_TABLE_NAME);
        stmt.execute();
        stmt.close();
        db.close();
    }


}

package pt.ipleiria.estg.dei.savergy.model;

import java.util.LinkedList;

/**
 * Created by Trabalho on 21/03/2017.
 */

public class Question {

    private int id;
    private int serverid;
    private int batchid;
    private String question;
    private String answer_type;
    private int answered;
    private LinkedList<Answer> answers;

    public Question(int serverid, int batchid, String question, String answer_type) {
        this.serverid = serverid;
        this.batchid = batchid;
        this.question = question;
        this.answer_type = answer_type;
        this.answered = 0;

        this.answers = new LinkedList<>();
    }

    public int getServerid() {
        return serverid;
    }

    public void setServerid(int serverid) {
        this.serverid = serverid;
    }

    public int getBatchid() {
        return batchid;
    }

    public void setBatchid(int batchid) {
        this.batchid = batchid;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer_type() {
        return answer_type;
    }

    public void setAnswer_type(String answer_type) {
        this.answer_type = answer_type;
    }

    public int getAnswered() {
        return answered;
    }

    public void setAnswered(int answered) {
        this.answered = answered;
    }

    public LinkedList<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(LinkedList<Answer> answers) {
        this.answers = answers;
    }

    public void addAnswer(Answer answer) {
        answers.add(answer);
    }

    public void removeAnswer(Answer answer) {
        answers.remove(answer);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("\n-- Questão --\n" + "ID: " + serverid + "\n" + "ID Batch: " + batchid + "\n" + "Questão: " +question + "\n" + "Tipo Resposta: " + answer_type + "\n");

        for (Answer a: answers) {

            sb.append("-- Respostas --\n "
                    + "ID: " + a.getServerid() + "\n"
                    + "ID Questão: " + a.getQuestionid() + "\n"
                    + "Resposta: " + a.getAnswer()  + "\n"
                    + "Conselho: " + a.getAdvise() + "\n"
                    + "Pontos: " + a.getPoints()  + "\n"
                    + "Selecionado: " + a.getSelected()  + "\n");
        }

        return sb.toString();
    }
}

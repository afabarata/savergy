package pt.ipleiria.estg.dei.savergy.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Trabalho on 21/03/2017.
 */

public class User {

    private int id;
    private String deviceId;
    private String socialId;
    private String email;
    private String name;

    private int avatar;
    private String country;
    private String district;
    private String county;
    private String birthdate;
    private String gender;
    private int household;

    private int numPoints;
    private int rank;
    private int ranking_local;
    private int ranking_country;
    private String last_sync;

    public User(String deviceId) {
        this.deviceId = deviceId;
        this.socialId = "";
        this.email = "";
        this.name = "";

        this.avatar = 0;
        this.country = "";
        this.district = "";
        this.county = "";
        this.birthdate = "";;
        this.gender = "";
        this.household = 0;

        this.numPoints = 0;
        this.rank = 0;
        this.ranking_local = 0;
        this.ranking_country = 0;
        this.last_sync = "";

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getSocialId() {
        return socialId;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAvatar() {
        return avatar;
    }

    public void setAvatar(int avatar) {
        this.avatar = avatar;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getHousehold() {
        return household;
    }

    public void setHousehold(int household) {
        this.household = household;
    }

    public int getNumPoints() {
        return numPoints;
    }

    public void setNumPoints(int numPoints) {
        this.numPoints = numPoints;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getRanking_local() {
        return ranking_local;
    }

    public void setRanking_local(int ranking_local) {
        this.ranking_local = ranking_local;
    }

    public int getRanking_country() {
        return ranking_country;
    }

    public void setRanking_country(int ranking_country) {
        this.ranking_country = ranking_country;
    }

    public String getLast_sync() {
        return last_sync;
    }

    public void setLast_sync(String last_sync) {
        this.last_sync = last_sync;
    }

    public Date getSyncDate() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
        Date newDate = null;
        try {
            newDate = format.parse(last_sync);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("\n--------- Categoria --------\n"
                + "Device ID: " + deviceId + "\n"
                + "Social ID: " + socialId + "\n"
                + "Email: " + email + "\n"
                + "Name: " + name + "\n"
                + "Avatar: " + avatar + "\n"
                + "Country: " + country + "\n"
                + "District: " + district + "\n"
                + "County: " + county + "\n"
                + "Birthdate: " + birthdate + "\n"
                + "Gender: " + gender + "\n"
                + "Household: " + household + "\n"
                + "Last_sync: " + last_sync + "\n");

        return sb.toString();
    }
}

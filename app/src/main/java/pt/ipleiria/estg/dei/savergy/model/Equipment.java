package pt.ipleiria.estg.dei.savergy.model;

import android.util.Log;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Bruno on 2017-04-14.
 */

public class Equipment {

    private int id;
    private int serverid;
    private String name;
    private String icon;
    private String description;
    private int isSelected;
    private int defaultUseTime;
    private int defaultPower;
    private int configuredUseTime;
    private String configuredUseTimeUnits;
    private int configuredPower;


    public Equipment(int serverid, String name, String icon, String description, int isSelected, int defaultUseTime, int defaultPower, int configuredUseTime, int configuredPower) {
        this.serverid = serverid;
        this.name = name;
        this.icon = icon;
        this.description = description;
        this.isSelected = isSelected;
        this.defaultUseTime = defaultUseTime;
        this.defaultPower = defaultPower;
        this.configuredUseTime = configuredUseTime;
        this.configuredUseTimeUnits = "Minutos";
        this.configuredPower = configuredPower;
    }

    public int getServerid() {
        return serverid;
    }

    public void setServerid(int serverid) {
        this.serverid = serverid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public String getIconURL() {
        URL url = null;
        try {
            url = new URL(Savergy.INSTANCE.getAPI_URL());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        URL final_url = null;
        try {
            final_url = new URL(url,icon);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return final_url.toString();
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getDefaultUseTime() {
        return defaultUseTime;
    }

    public void setDefaultUseTime(int defaultUseTime) {
        this.defaultUseTime = defaultUseTime;
    }

    public int getDefaultPower() {
        return defaultPower;
    }

    public void setDefaultPower(int defaultPower) {
        this.defaultPower = defaultPower;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(int isSelected) {
        this.isSelected = isSelected;
    }

    public int getConfiguredUseTime() {
        return configuredUseTime;
    }

    public void setConfiguredUseTime(int configuredUseTime) {
        this.configuredUseTime = configuredUseTime;
    }

    public String getConfiguredUseTimeUnits() {
        return configuredUseTimeUnits;
    }

    public void setConfiguredUseTimeUnits(String configuredUseTimeUnits) {
        this.configuredUseTimeUnits = configuredUseTimeUnits;
    }

    public int getConfiguredPower() {
        return configuredPower;
    }

    public void setConfiguredPower(int configuredPower) {
        this.configuredPower = configuredPower;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("\n-- Equipamento --\n"
                + "ID: " + serverid + "\n"
                + "Nome: " +name + "\n"
                + "Icon: " + icon + "\n"
                + "Description: " + description + "\n"
                + "isSelected: " + isSelected + "\n"
                + "defaultUseTime: " + defaultUseTime + "\n"
                + "defaultPower: " + defaultPower + "\n"
                + "configuredUseTime: " + configuredUseTime + "\n"
                + "configuredPower: " + configuredPower + "\n"
                + "\n");

        return sb.toString();
    }
}

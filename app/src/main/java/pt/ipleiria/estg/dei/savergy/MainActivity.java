package pt.ipleiria.estg.dei.savergy;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import java.util.concurrent.ExecutionException;
import pt.ipleiria.estg.dei.savergy.database.DatabaseHelper;
import pt.ipleiria.estg.dei.savergy.helpers.ConnectionsHelper;
import pt.ipleiria.estg.dei.savergy.helpers.LoginHelper;
import pt.ipleiria.estg.dei.savergy.helpers.NetworkUtility;
import pt.ipleiria.estg.dei.savergy.model.Savergy;

public class MainActivity extends AppCompatActivity {

    private String device_id;
    private LoginHelper loginHelper;
    private ConnectionsHelper connectionsHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*String[] perms = {"android.permission.READ_PHONE_STATE"};
        int permsRequestCode = 200;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(perms, permsRequestCode);
        }*/

        if(hasPermission("android.permission.READ_PHONE_STATE")) {
            TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
            device_id = telephonyManager.getDeviceId();
        }
        else {
            LayoutInflater li = LayoutInflater.from(MainActivity.this);
            View promptsView = li.inflate(R.layout.dialog_remove_account_confirm, null);

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);

            alertDialogBuilder.setView(promptsView);

            final TextView dialogIntro = (TextView) promptsView.findViewById(R.id.textViewEqDialogQuestion);
            dialogIntro.setText(R.string.permission_error);

            alertDialogBuilder
                    .setCancelable(false)
                    .setNegativeButton("Compreendi",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    finish();
                                    moveTaskToBack(true);
                                }
                            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

        }

        //device_id = "864587020834561";
        //loginHelper.persistUser(this, telephonyManager.getDeviceId());

        loginHelper = new LoginHelper(this);
        connectionsHelper = new ConnectionsHelper(this);

        if(loginHelper.checkAlreadyRegister(device_id)){
            Log.d("VERIFICAÇÃO USER: ", "A BD ESTAVA PREENCHIDA");
            if(NetworkUtility.hasConnectivity(this)) {
                Log.d("VERIFICAÇÃO USER: ", "HA NET");
                try {
                    if(loginHelper.checkDeviceID(device_id)) {
                        Log.d("VERIFICAÇÃO USER: ", "O DEVICE JÁ ESTAVA REGISTADO");
                        loginHelper.getLocalUser(device_id);
                        loginHelper.getRemoteUserByDeviceID(device_id);
                    } else {
                        Log.d("VERIFICAÇÃO USER: ", "O DEVICE NÃO ESTAVA REGISTADO");
                        loginHelper.getLocalUser(device_id);
                        loginHelper.persistRemoteUserByDeviceId(device_id);
                        loginHelper.getRemoteUserByDeviceID(device_id);
                    }
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                Log.d("VERIFICAÇÃO USER: ", "NÃO HA NET");
                loginHelper.getLocalUser(device_id);
            }
        } else {
            Log.d("VERIFICAÇÃO USER: ", "A BD NÃO ESTAVA PREENCHIDA");
            if(NetworkUtility.hasConnectivity(MainActivity.this)) {
                Log.d("VERIFICAÇÃO USER: ", "HA NET");
                try {
                    if(loginHelper.checkDeviceID(device_id)) {
                        Log.d("VERIFICAÇÃO USER: ", "O DEVICE JÁ ESTAVA REGISTADO");
                        loginHelper.persistLocalUser(device_id);
                        loginHelper.getRemoteUserByDeviceID(device_id);
                    } else {
                        Log.d("VERIFICAÇÃO USER: ", "O DEVICE NÃO ESTAVA REGISTADO");
                        loginHelper.persistLocalAndRemoteUser(device_id);
                        loginHelper.getRemoteUserByDeviceID(device_id);
                    }
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                Log.d("VERIFICAÇÃO USER: ", "NÃO HA NET");
                loginHelper.persistLocalUser(device_id);
            }
        }

        Savergy.INSTANCE.saveCurrentUser(this);
        loginHelper.checkUsers();

    }

    @Override
    public void onBackPressed() {
        LayoutInflater li = LayoutInflater.from(MainActivity.this);
        View promptsView = li.inflate(R.layout.dialog_remove_account_confirm, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);

        alertDialogBuilder.setView(promptsView);

        final TextView dialogIntro = (TextView) promptsView.findViewById(R.id.textViewEqDialogQuestion);
        dialogIntro.setText(R.string.main_exit_app);

        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton(R.string.dialog_yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                finish();
                                moveTaskToBack(true);
                            }
                        })
                .setNegativeButton(R.string.dialog_no,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void onClickStartApp(View view) throws ExecutionException, InterruptedException {

        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        //device id do meu 864587020834561
        //inventado 318988285400557

        if(databaseHelper.existsBatches()) {
            if(NetworkUtility.hasConnectivity(MainActivity.this)) {
                connectionsHelper.getBatcherFromServer();
                connectionsHelper.getEquipmentsFromServer();
                connectionsHelper.getAdvicesFromServer();
            } else {
                Savergy.INSTANCE.uploadProfileQuestions(this);
                Savergy.INSTANCE.uploadBatchesList(this);
                Savergy.INSTANCE.uploadEquipmentsList(this);
                Savergy.INSTANCE.uploadSelectedEquipmentsList(this);
                Savergy.INSTANCE.uploadAdvicesList(this);
            }
        } else {
            if(NetworkUtility.hasConnectivity(MainActivity.this)) {
                connectionsHelper.getBatcherFromServer();
                connectionsHelper.getEquipmentsFromServer();
                connectionsHelper.getAdvicesFromServer();
            } else {
                LayoutInflater li = LayoutInflater.from(MainActivity.this);
                View promptsView = li.inflate(R.layout.dialog_remove_account_confirm, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);

                alertDialogBuilder.setView(promptsView);

                final TextView dialogIntro = (TextView) promptsView.findViewById(R.id.textViewEqDialogQuestion);
                dialogIntro.setText(R.string.main_not_internet);

                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton(R.string.dialog_try_again,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        if(NetworkUtility.hasConnectivity(MainActivity.this)) {
                                            try {
                                                if(loginHelper.checkDeviceID(device_id)) {
                                                    Log.d("VERIFICAÇÃO USER: ", "O DEVICE JÁ ESTAVA REGISTADO");
                                                    loginHelper.persistLocalUser(device_id);
                                                    loginHelper.getRemoteUserByDeviceID(device_id);
                                                } else {
                                                    Log.d("VERIFICAÇÃO USER: ", "O DEVICE NÃO ESTAVA REGISTADO");
                                                    loginHelper.persistLocalAndRemoteUser(device_id);
                                                    loginHelper.getRemoteUserByDeviceID(device_id);
                                                }
                                                connectionsHelper.getBatcherFromServer();
                                                connectionsHelper.getEquipmentsFromServer();
                                                connectionsHelper.getAdvicesFromServer();
                                            } catch (ExecutionException e) {
                                                e.printStackTrace();
                                            } catch (InterruptedException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                })
                        .setNegativeButton(R.string.dialog_close,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        }

        if(!Savergy.INSTANCE.getCurrentUser().getSocialId().isEmpty() || !Savergy.INSTANCE.getCurrentUser().getEmail().isEmpty()){
            Intent intent = new Intent(this, DashboardActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, RegisterChoiceActivity.class);
            startActivity(intent);
        }

    }

    private boolean canMakeSmores(){
        return(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    /*@Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults){
        switch(permsRequestCode){
            case 200:
                boolean phoneStateAccepted = grantResults[0]== PackageManager.PERMISSION_GRANTED;
                break;
        }
    }*/

    private boolean hasPermission(String permission){
        if(canMakeSmores()){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return(checkSelfPermission(permission)==PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }
}

package pt.ipleiria.estg.dei.savergy.helpers;

import java.util.LinkedList;

/**
 * Created by Trabalho on 06/05/2017.
 */

public class AddressHelper {


    public class Distrito {

        private String name;
        private String[] concelhos;

        public Distrito(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String[] getConcelhos() {
            return concelhos;
        }

        public void setConcelhos(String[] concelhos) {
            this.concelhos = concelhos;
        }
    }

    private LinkedList<Distrito> distritos;

    public AddressHelper() {
        distritos = new LinkedList<>();

        String [] empty_c = {};
        Distrito empty = new Distrito("");
        empty.setConcelhos(empty_c);
        distritos.add(empty);

        String[] aveiro_c = {"Águeda","Albergaria-a-Velha","Anadia","Arouca","Aveiro","Castelo de Paiva","Espinho","Estarreja","Ílhavo","Mealhada","Murtosa","Oliveira de Azeméis",
                "Oliveira do Bairro","Ovar","Sta Maria da Feira","S. João da Madeira"};
        Distrito aveiro = new Distrito("Aveiro");
        aveiro.setConcelhos(aveiro_c);
        distritos.add(aveiro);

        String[] beja_c = {"Aljustrel","Almodôvar","Alvito","Barrancos","Beja","Castro Verde","Cuba","Ferreira do Alentejo","Mértola","Moura","Odemira","Ourique","Serpa","Vidigueira"};
        Distrito beja = new Distrito("Beja");
        beja.setConcelhos(beja_c);
        distritos.add(beja);

        String[] braga_c = {"Amares","Barcelos","Braga","Cabeceiras de Basto","Celoirico de Basto","Esposende","Fafe","Guimarães","Póvoa de Lanhoso","Terras de Bouro","Vieira do Minho",
                "Vila Nova de Famalicão","Vila Verde","Vizela"};
        Distrito braga = new Distrito("Braga");
        braga.setConcelhos(braga_c);
        distritos.add(braga);

        String[] braganca_c = {"Alfandega da Fé","Bragança","Carrazes de Ansiães","Freixo de Espada à Cinta","Macedo dos Cavaleiros","Mirando do Douro","Mirandela","Mogadouro",
                "Torre do Moncorvo","Vila Flor","Vimioso","Vinhais"};
        Distrito braganca = new Distrito("Bragança");
        braganca.setConcelhos(braganca_c);
        distritos.add(braganca);

        String[] castelo_branco_c = {"Belmonte","Castelo Branco","Covilhã","Fundão","Idanha-a-Nova","Oleiros","Penamacor","Proença-a-Nova","Sertã","Vila do Rei","Vila Nova de Ródão"};
        Distrito castelo_branco = new Distrito("Castelo Branco");
        castelo_branco.setConcelhos(castelo_branco_c);
        distritos.add(castelo_branco);

        String[] coimbra_c = {"Ârganil","Cantanhede","Coimbra","Condeixa-a-Nova","Figueira da Foz","Góis","Lousã","Mira","Miranda do Corvo","Montemor-o-Velho","Oliveira do Hospital",
                "Pampilhosa da Serra","Penacova","Penela","Soure","Tábua","Vila Nova de Poiares"};
        Distrito coimbra = new Distrito("Coimbra");
        coimbra.setConcelhos(coimbra_c);
        distritos.add(coimbra);

        String[] evora_c = {"Alandroal","Arraiolos","Borba","Estremoz","Évora","Montemor-o-Novo","Mora","Mourão","Olivença","Portel","Redondo","Reguengos de Monsaraz","Vendas Novas",
                "Viana do Alentejo","Vila Viçosa"};
        Distrito evora = new Distrito("Évora");
        evora.setConcelhos(evora_c);
        distritos.add(evora);

        String[] faro_c = {"Albufeira","Alcoutim","Aljezur","Castro Marim","Faro","Lagoa","Lagos","Loulé","Monchique","Olhão","Portimão","São Brás de Alportel","Silves","Tavira",
                "Vila do Bispo","Vila Real de Sto António"};
        Distrito faro = new Distrito("Faro");
        faro.setConcelhos(faro_c);
        distritos.add(faro);

        String[] guarda_c = {"Aguiar da Beira","Almeida","Celorico da Beira","Figueira do Castelo Rodrigo","Fornos de Algores","Gouveia","Guarda","Manteigas","Mêda","Pinhel","Sabugal",
                "Seia","Trancoso","Vila Nova de Foz Côa"};
        Distrito guarda = new Distrito("Guarda");
        guarda.setConcelhos(guarda_c);
        distritos.add(guarda);

        String[] leiria_c = {"Alcobaça","Alvaiázere","Ansião","Batalha","Bombarral","Caldas da Rainha","Castanheira de Pera","Figueiró dos Vinhos","Leiria","Marinha Grande","Nazaré",
                "Óbidos","Pedrógão Grande","Peniche","Pombal","Porto de Mós"};
        Distrito leiria = new Distrito("Leiria");
        leiria.setConcelhos(leiria_c);
        distritos.add(leiria);

        String[] lisboa_c = {"Alenquer","Amadora","Arruda dos Vinhos","Azambuja","Cadaval","Cascais","Lisboa","Loures","Lourinhã","Mafra","Odivelas","Oeiras","Sintra","Sobral de Monte Agraço",
                "Torres Vedras","Vila Franca de Xira"};
        Distrito lisboa = new Distrito("Lisboa");
        lisboa.setConcelhos(lisboa_c);
        distritos.add(lisboa);

        String[] portalegre_c = {"Alter do Chão","Arronches","Avis","Campo Maior","Castelo de Vide","Crato","Elvas","Fronteira","Gavião","Marvão","Monforte","Nisa","Ponte de Sor",
                "Portalegre","Sousel"};
        Distrito portalegre = new Distrito("Portalegre");
        portalegre.setConcelhos(portalegre_c);
        distritos.add(portalegre);

        String[] porto_c = {"Amarante","Baião","Felgueiras","Gondomar","Lousada","Maia","Marco de Canaveses","Matosinhos","Paços de Ferreira","Paredes","Penafiel","Porto","Póvoa de Varzim",
                "Santo Tirso","Trofa","Valongo","Vila do Conde","Vila Nova de Gaia"};
        Distrito porto = new Distrito("Porto");
        porto.setConcelhos(porto_c);
        distritos.add(porto);

        String[] santarem_c = {"Abrantes","Alcanena","Almeirim","Alpiarça","Benavente","Cartaxo","Chamusca","Constância","Coruche","Entrocamento","Ferreira do Zêzere","Golegã","Mação",
                "Ourém","Rio Maior","Salvaterra de Magos","Santarém","Sardoal","Tomar","Torres Novas","Vila Nova da Barquinha"};
        Distrito santarem = new Distrito("Santarém");
        santarem.setConcelhos(santarem_c);
        distritos.add(santarem);

        String[] setubal_c = {"Alcácer do Sal","Alcochete","Almada","Barreiro","Grandôla","Moita","Montijo","Palmela","Santiago do Cacém","Seixal","Sesimbra","Setúbal","Sines"};
        Distrito setubal = new Distrito("Setúbal");
        setubal.setConcelhos(setubal_c);
        distritos.add(setubal);

        String[] viana_do_castelo_c = {"Arcos de Valdevez","Caminha","Melgaço","Monção","Paredes de Coura","Ponte da Barca","Ponte de Lima","Valença","Viana do Castelo","Vila Nova de Cerveira"};
        Distrito viana_do_castelo = new Distrito("Viana do Castelo");
        viana_do_castelo.setConcelhos(viana_do_castelo_c);
        distritos.add(viana_do_castelo);

        String[] vila_real_c = {"Alijó","Boticas","Chaves","Mesão Frio","Mondim de Baixo","Montalegre","Murça","Peso da Régua","Ribeira da Pena","Sabrosa","Sta Marta de Penaguião","Valpaços","Vila Pouca de Aguiar","Vila Real"};
        Distrito vila_real = new Distrito("Vila Real");
        vila_real.setConcelhos(vila_real_c);
        distritos.add(vila_real);

        String[] viseu_c = {"Armamar","Carregal do Sal","Castro Daire","Cinfães","Lamego","Magualde","Moimenta da Beira","Mortágua","Nelas","Oliveira de Frades","Penalva do Castelo",
                "Penedondo","Resende","Sta Comba Dão","S João da Pesqueira","São Pedro do Sul","Sátão","Sernancelhe","Tabuaço","Tarouca","Tondela","Vila Nova de Paiva","Viseu","Vouzela"};
        Distrito viseu = new Distrito("Viseu");
        viseu.setConcelhos(viseu_c);
        distritos.add(viseu);
    }

    public LinkedList<Distrito> getDistritos() {
        return distritos;
    }

    public void setDistritos(LinkedList<Distrito> distritos) {
        this.distritos = distritos;
    }
}

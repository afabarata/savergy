package pt.ipleiria.estg.dei.savergy.helpers;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import pt.ipleiria.estg.dei.savergy.database.DatabaseHelper;
import pt.ipleiria.estg.dei.savergy.model.Savergy;
import pt.ipleiria.estg.dei.savergy.model.User;
import pt.ipleiria.estg.dei.savergy.server.CheckDeviceIdTask;
import pt.ipleiria.estg.dei.savergy.server.LoginWithDeviceIdTask;
import pt.ipleiria.estg.dei.savergy.server.LoginWithEmailTask;
import pt.ipleiria.estg.dei.savergy.server.LoginWithSocialIDTask;
import pt.ipleiria.estg.dei.savergy.server.RegisterByDeviceIdTask;
import pt.ipleiria.estg.dei.savergy.server.RegisterByEmailTask;
import pt.ipleiria.estg.dei.savergy.server.RegisterBySocialIdTask;
import pt.ipleiria.estg.dei.savergy.server.RemoveAccountTask;
import pt.ipleiria.estg.dei.savergy.server.SetProfileTask;

/**
 * Created by Trabalho on 21/03/2017.
 */

public class LoginHelper {

    Context context;

    private DatabaseHelper dbHelper;
    private JsonHelper jsonHelper;
    private CheckDeviceIdTask checkDeviceIdTask;
    private RegisterByDeviceIdTask registerByDeviceIdTask;
    private LoginWithDeviceIdTask loginWithDeviceIdTask;
    private RegisterBySocialIdTask registerBySocialIdTask;
    private LoginWithSocialIDTask loginWithSocialIdTask;
    private RegisterByEmailTask registerByEmailTask;
    private LoginWithEmailTask loginWithEmailTask;
    private SetProfileTask setProfileTask;
    private RemoveAccountTask removeAccountTask;
    private User auxUser;

    public LoginHelper(Context context) {
        this.context = context;

        dbHelper = new DatabaseHelper(context);
        jsonHelper = new JsonHelper();

        checkDeviceIdTask = new CheckDeviceIdTask();
        registerByDeviceIdTask = new RegisterByDeviceIdTask();
        loginWithDeviceIdTask = new LoginWithDeviceIdTask();
        registerBySocialIdTask = new RegisterBySocialIdTask();
        loginWithSocialIdTask = new LoginWithSocialIDTask();
        registerByEmailTask = new RegisterByEmailTask();
        setProfileTask = new SetProfileTask();
        removeAccountTask = new RemoveAccountTask();
    }

    public boolean checkAlreadyRegister(String device) {
        return dbHelper.existsByDeviceId(device);
    }

    public boolean checkDeviceID(String device) throws ExecutionException, InterruptedException {
        String status = checkDeviceIdTask.execute(device).get();
        try {
            JSONObject obj = new JSONObject(status);
            if(obj.getInt("status") == 200) {
                return true;
            } else {
                return false;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void getLocalUser(String device) {
        Savergy.INSTANCE.setCurrentUser(dbHelper.getUserByDeviceId(device));
    }

    public void getRemoteUserByDeviceID(String device) {
        loginWithDeviceIdTask.execute(device);
    }

    public void getRemoteUserBySocialID() {
        loginWithSocialIdTask.execute(Savergy.INSTANCE.getCurrentUser().getSocialId());
    }

    public boolean getRemoteUserByEmail(String email, String password) throws ExecutionException, InterruptedException {
        loginWithEmailTask = new LoginWithEmailTask();
        String status = loginWithEmailTask.execute(email,password).get();
        try {
            JSONObject obj = new JSONObject(status);
            if(obj.getInt("status") == 200) {
                JSONObject js_obj = obj.getJSONObject("data");
                Log.i("API TOKEN: ", js_obj.getString("api_token"));

                Savergy.INSTANCE.setAPI_TOKEN(js_obj.getString("api_token"));
                Savergy.INSTANCE.getCurrentUser().setNumPoints(js_obj.getInt("total_points"));
                if(!js_obj.getString("social_id").equals("null") ) {
                    Savergy.INSTANCE.getCurrentUser().setSocialId(js_obj.getString("social_id"));
                }
                if(!js_obj.getString("email").equals("null") ) {
                    Savergy.INSTANCE.getCurrentUser().setEmail(js_obj.getString("email"));
                }
                if(!js_obj.getString("name").equals("null") ) {
                    Savergy.INSTANCE.getCurrentUser().setName(js_obj.getString("name"));
                }
                Savergy.INSTANCE.getCurrentUser().setAvatar(js_obj.getInt("avatar"));
                if(!js_obj.getString("country").equals("null") ) {
                    Savergy.INSTANCE.getCurrentUser().setCountry(js_obj.getString("country"));
                }
                if(!js_obj.getString("district").equals("null") ) {
                    Savergy.INSTANCE.getCurrentUser().setDistrict(js_obj.getString("district"));
                }
                if(!js_obj.getString("county").equals("null") ) {
                    Savergy.INSTANCE.getCurrentUser().setCounty(js_obj.getString("county"));
                }
                if(!js_obj.getString("birthdate").equals("null") ) {
                    Savergy.INSTANCE.getCurrentUser().setBirthdate(js_obj.getString("birthdate"));
                }
                if(!js_obj.getString("gender").equals("null") ) {
                    Savergy.INSTANCE.getCurrentUser().setGender(js_obj.getString("gender"));
                }
                if(!js_obj.getString("household").equals("null")) {
                    Savergy.INSTANCE.getCurrentUser().setHousehold(js_obj.getInt("household"));
                }

                Savergy.INSTANCE.saveCurrentUser(context);
                return true;
            } else {
                return false;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;

    }

    public void persistLocalAndRemoteUser(String device) {
        Savergy.INSTANCE.setCurrentUser(new User(device));
        registerByDeviceIdTask.execute(device);
        dbHelper.insertUser(Savergy.INSTANCE.getCurrentUser());
    }

    public void persistLocalUser(String device) {
        Savergy.INSTANCE.setCurrentUser(new User(device));
        User user = Savergy.INSTANCE.getCurrentUser();
        dbHelper.insertUser(user);
    }

    public void persistRemoteUserByDeviceId(String device) {
        registerByDeviceIdTask.execute(device);
    }

    public void persistRemoteUserBySocialID() {
        registerBySocialIdTask.execute(Savergy.INSTANCE.getCurrentUser().getDeviceId(), Savergy.INSTANCE.getCurrentUser().getName(), Savergy.INSTANCE.getCurrentUser().getSocialId());
    }

    public boolean persistRemoteUserByEmail(String device, String name, String email, String password) throws ExecutionException, InterruptedException {
        String status = registerByEmailTask.execute(device, name, email, password).get();
        try {
            JSONObject obj = new JSONObject(status);
            if(obj.getInt("status") == 200) {
                Savergy.INSTANCE.getCurrentUser().setName(name);
                Savergy.INSTANCE.getCurrentUser().setEmail(email);
                dbHelper.updateUser(Savergy.INSTANCE.getCurrentUser());
                return true;
            } else {
                return false;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void RegisterUserWithSocialID(String name, String socialId) {
        Savergy.INSTANCE.getCurrentUser().setName(name);
        Savergy.INSTANCE.getCurrentUser().setSocialId(socialId);
        registerBySocialIdTask.execute(Savergy.INSTANCE.getCurrentUser().getDeviceId(), name, socialId);
        loginWithSocialIdTask.execute(Savergy.INSTANCE.getCurrentUser().getSocialId());
    }

    public void setProfileData() throws ExecutionException, InterruptedException {
        Savergy.INSTANCE.saveCurrentUser(context);
        String status = setProfileTask.execute(Savergy.INSTANCE.getAPI_TOKEN(),
                            Savergy.INSTANCE.getCurrentUser().getAvatar()+"",
                            Savergy.INSTANCE.getCurrentUser().getName(),
                            "",
                            Savergy.INSTANCE.getCurrentUser().getCounty(),
                            Savergy.INSTANCE.getCurrentUser().getDistrict(),
                            Savergy.INSTANCE.getCurrentUser().getCountry(),
                            Savergy.INSTANCE.getCurrentUser().getBirthdate(),
                            Savergy.INSTANCE.getCurrentUser().getGender(),
                            Savergy.INSTANCE.getCurrentUser().getHousehold()+"").get();
        try {
            JSONObject obj = new JSONObject(status);
            if(obj.getInt("status") == 200) {
                Toast.makeText(context,"Dados gravados com sucesso!",Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context,"Ocorreu um erro durante a gravação, tente novamente!",Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void removeAccout() {
        dbHelper.deleteUsers();
        dbHelper.deleteBatches();
        dbHelper.deleteEquipments();
        dbHelper.deleteAdvices();
        removeAccountTask.execute(Savergy.INSTANCE.getAPI_TOKEN());
    }

    public void checkUsers() {
        dbHelper.toStringUsers();
    }


}

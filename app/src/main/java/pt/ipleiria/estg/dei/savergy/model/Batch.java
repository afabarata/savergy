package pt.ipleiria.estg.dei.savergy.model;

import java.util.LinkedList;

/**
 * Created by Trabalho on 21/03/2017.
 */

public class Batch {

    private int id;
    private int serverid;
    private String batch;
    private int nquestions;
    private int maxscore;
    private int score;
    private int nanswered;
    private LinkedList<Question> questions;

    public Batch(int serverid, String batch, int nquestions, int maxscore) {
        this.serverid = serverid;
        this.batch = batch;
        this.nquestions = nquestions;
        this.maxscore = maxscore;
        this.score = 0;
        this.nanswered = 0;

        this.questions = new LinkedList<>();
    }

    public int getServerid() {
        return serverid;
    }

    public void setServerid(int serverid) {
        this.serverid = serverid;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public int getNquestions() {
        return nquestions;
    }

    public void setNquestions(int nquestions) {
        this.nquestions = nquestions;
    }

    public int getMaxscore() {
        return maxscore;
    }

    public void setMaxscore(int maxscore) {
        this.maxscore = maxscore;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getNanswered() {
        return nanswered;
    }

    public void setNanswered(int nanswered) {
        this.nanswered = nanswered;
    }

    public LinkedList<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(LinkedList<Question> questions) {
        this.questions = questions;
    }

    public void addQuestion(Question question) {
        questions.add(question);
    }

    public void removeQuestion(Question question) {
        questions.remove(question);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("\n--------- Categoria --------\n"
                            + "ID: " + serverid + "\n"
                            + "Batch: " + batch + "\n"
                            + "Nº Questões: " + nquestions + "\n"
                            + "Nº Max Score: " + maxscore + "\n"
                            + "Nº Score: " + score + "\n"
                            + "Nº Respondidas: " + nanswered + "\n");
        for (Question q: questions)
        {
            sb.append("-- Questões --\n "
                        + "ID: " + q.getServerid() + "\n"
                        + "ID Batch: " + q.getBatchid() + "\n"
                        + "Questão: " + q.getQuestion()  + "\n"
                        + "Tipo Resposta: " + q.getAnswer_type() + "\n"
                        + "Respondida: " + q.getAnswered()  + "\n");

            for (Answer a: q.getAnswers()) {

                sb.append("-- Respostas --\n "
                        + "ID: " + a.getServerid() + "\n"
                        + "ID Questão: " + a.getQuestionid() + "\n"
                        + "Resposta: " + a.getAnswer()  + "\n"
                        + "Conselho: " + a.getAdvise() + "\n"
                        + "Pontos: " + a.getPoints()  + "\n"
                        + "Selecionada: " + a.getSelected()  + "\n");
            }
        }

        return sb.toString();
    }

}

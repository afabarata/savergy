package pt.ipleiria.estg.dei.savergy;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;

import pt.ipleiria.estg.dei.savergy.database.DatabaseHelper;
import pt.ipleiria.estg.dei.savergy.helpers.JsonHelper;
import pt.ipleiria.estg.dei.savergy.helpers.NetworkUtility;
import pt.ipleiria.estg.dei.savergy.model.Answer;
import pt.ipleiria.estg.dei.savergy.model.Question;
import pt.ipleiria.estg.dei.savergy.model.Savergy;
import pt.ipleiria.estg.dei.savergy.server.SetAnswersTask;

/**
 * Created by Trabalho on 01/04/2017.
 */

public class QuestionsActivity extends AppCompatActivity {

    public static final String BATCHSERVERID = "BATCHSERVERID";
    public static final String BATCHNAME = "BATCHNAME";

    private ActionMenuView defaultMenu;
    private ActionMenuView defaultMenuTitle;
    private ActionMenuView defaultMenuBack;

    private QuestionTabAdapter questionTabAdapter;
    private ViewPager mViewPager;

    private DatabaseHelper dbHelper;
    private static LinkedList<Question> questions;

    private long id;
    private String name;
    private int numQuestions;
    private static TextView question;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        id = getIntent().getIntExtra(BATCHSERVERID, Integer.MAX_VALUE);
        name = getIntent().getStringExtra(BATCHNAME);
        if(id == 0) {
            questions = Savergy.INSTANCE.getProfileQuestions().getQuestions();
        } else {
            questions = Savergy.INSTANCE.getQuestionsByBatchServerId(id);
        }

        numQuestions = questions.size();

        setContentView(R.layout.activity_questions);

        Toolbar t = (Toolbar) findViewById(R.id.tToolbar);
        defaultMenu = (ActionMenuView) t.findViewById(R.id.defaultMenu);
        defaultMenuTitle = (ActionMenuView) t.findViewById(R.id.defaultMenuTitle);
        defaultMenuBack = (ActionMenuView) t.findViewById(R.id.defaultMenuBack);
        defaultMenu.setOnMenuItemClickListener(new ActionMenuView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                return onOptionsItemSelected(menuItem);
            }
        });
        defaultMenuBack.setOnMenuItemClickListener(new ActionMenuView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                return onOptionsItemSelected(menuItem);
            }
        });
        setSupportActionBar(t);
        getSupportActionBar().setTitle(null);

        questionTabAdapter = new QuestionTabAdapter(getSupportFragmentManager(), this);

        int limit = (questionTabAdapter.getCount() > 1 ? questionTabAdapter.getCount() - 1 : 1);

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(questionTabAdapter);
        mViewPager.setOffscreenPageLimit(limit);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {}
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            public void onPageSelected(int position) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_title, defaultMenuTitle.getMenu());
        inflater.inflate(R.menu.menu_return, defaultMenuBack.getMenu());
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        TextView title = (TextView) defaultMenuTitle.getChildAt(0).findViewById(R.id.menuTitle);
        title.setText(name);
        title.setTextSize(20);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        long id=item.getItemId();

        if (id == R.id.menuReturnActivity) {
            saveAwnsers();
            setResult(RESULT_OK);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        saveAwnsers();
        setResult(RESULT_OK);
        finish();
    }

    public class QuestionTabAdapter extends FragmentPagerAdapter {

        Context mContext;

        public QuestionTabAdapter(FragmentManager fm, Context context) {
            super(fm);
            mContext = context;
        }

        @Override
        public Fragment getItem(int position) {

            Fragment fragment = new QuestionTabFragment();

            Bundle args = new Bundle();
            args.putInt("fragment", position);
            fragment.setArguments(args);

            return fragment;
        }

        @Override
        public int getCount() {
            return numQuestions;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getString(R.string.question_tab_question)+(position+1);
        }

    }

    public static class QuestionTabFragment extends Fragment {

        View rootView;

        RadioGroup.OnCheckedChangeListener fourI_one_C_listener1 = null;
        RadioGroup.OnCheckedChangeListener fourI_one_C_listener2 = null;
        RadioGroup.OnCheckedChangeListener fourT_one_C_listener1 = null;
        RadioGroup.OnCheckedChangeListener fourT_one_C_listener2 = null;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            Bundle args = getArguments();
            final int fragment = args.getInt("fragment");
            String typeOfQuestion = questions.get(fragment).getAnswer_type();

            if(typeOfQuestion.equals("true_false")) {
                rootView = inflater.inflate(R.layout.fragment_question_true_false, container, false);
                question = (TextView) rootView.findViewById(R.id.textViewQuestionTrueFalse);
                question.setText(questions.get(fragment).getQuestion());

                LinkedList<Answer> answers = questions.get(fragment).getAnswers();

                RadioButton radio1 = (RadioButton) rootView.findViewById(R.id.radioButtonTrue);
                radio1.setText(answers.get(0).getAnswer());
                RadioButton radio2 = (RadioButton) rootView.findViewById(R.id.radioButtonFalse);
                radio2.setText(answers.get(1).getAnswer());

                RadioGroup radioGroupTrueFalse = (RadioGroup)rootView.findViewById(R.id.radioGroupTrueFalse);
                if(questions.get(fragment).getAnswered() == 1) {
                    for(int i = 0; i < radioGroupTrueFalse.getChildCount(); i++){
                        radioGroupTrueFalse.getChildAt(i).setEnabled(false);
                    }
                    if(questions.get(fragment).getAnswers().get(0).getSelected() == 1) {
                        RadioButton radio_true = (RadioButton) rootView.findViewById(R.id.radioButtonTrue);
                        radio_true.setChecked(true);
                    } else {
                        RadioButton radio_false = (RadioButton) rootView.findViewById(R.id.radioButtonFalse);
                        radio_false.setChecked(true);
                    }
                } else {
                    radioGroupTrueFalse.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
                    {
                        public void onCheckedChanged(RadioGroup group, int checkedId)
                        {
                            RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                            for (Answer a: questions.get(fragment).getAnswers()) {
                                if(a.getAnswer().equals(checkedRadioButton.getText())) {
                                    a.setSelected(1);
                                    showFeedback(a.getAdvise(),rootView.getContext());
                                } else {
                                    a.setSelected(0);
                                }
                            }
                            questions.get(fragment).setAnswered(1);

                        }
                    });
                }
            }

            if(typeOfQuestion.equals("one_choice_four_images")) {
                rootView = inflater.inflate(R.layout.fragment_question_one_choice_four_images, container, false);
                question = (TextView) rootView.findViewById(R.id.textViewQuestion1Choice4Images);
                question.setText(questions.get(fragment).getQuestion());

                LinkedList<Answer> answers = questions.get(fragment).getAnswers();

                ImageView image1 = (ImageView) rootView.findViewById(R.id.imageView4IImage1);
                ImageView image2 = (ImageView) rootView.findViewById(R.id.imageView4IImage2);
                ImageView image3 = (ImageView) rootView.findViewById(R.id.imageView4IImage3);
                ImageView image4 = (ImageView) rootView.findViewById(R.id.imageView4IImage4);

                Picasso.with(rootView.getContext()).load(getUrl(answers.get(0).getAnswer())).into(image1);
                Picasso.with(rootView.getContext()).load(getUrl(answers.get(1).getAnswer())).into(image2);
                Picasso.with(rootView.getContext()).load(getUrl(answers.get(2).getAnswer())).into(image3);
                Picasso.with(rootView.getContext()).load(getUrl(answers.get(3).getAnswer())).into(image4);

                final RadioGroup radioGroup4I1C_r = (RadioGroup)rootView.findViewById(R.id.radioGroup4I1C_r);
                final RadioGroup radioGroup4I1C_l = (RadioGroup)rootView.findViewById(R.id.radioGroup4I1C_l);
                radioGroup4I1C_r.clearCheck();
                radioGroup4I1C_l.clearCheck();

                if(questions.get(fragment).getAnswered() == 1) {
                    for(int i = 0; i < radioGroup4I1C_r.getChildCount(); i++){
                        radioGroup4I1C_r.getChildAt(i).setEnabled(false);
                    }
                    for(int i = 0; i < radioGroup4I1C_l.getChildCount(); i++){
                        radioGroup4I1C_l.getChildAt(i).setEnabled(false);
                    }
                    if(questions.get(fragment).getAnswers().get(0).getSelected() == 1) {
                        RadioButton radio1 = (RadioButton) rootView.findViewById(R.id.radioButton4IImage1);
                        radio1.setChecked(true);
                    }
                    if(questions.get(fragment).getAnswers().get(1).getSelected() == 1) {
                        RadioButton radio2 = (RadioButton) rootView.findViewById(R.id.radioButton4IImage2);
                        radio2.setChecked(true);
                    }
                    if(questions.get(fragment).getAnswers().get(2).getSelected() == 1) {
                        RadioButton radio3 = (RadioButton) rootView.findViewById(R.id.radioButton4IImage3);
                        radio3.setChecked(true);
                    }
                    if(questions.get(fragment).getAnswers().get(3).getSelected() == 1) {
                        RadioButton radio4 = (RadioButton) rootView.findViewById(R.id.radioButton4IImage4);
                        radio4.setChecked(true);
                    }
                }

                fourI_one_C_listener1 = new RadioGroup.OnCheckedChangeListener() {
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        if(checkedId != -1) {
                            RadioButton checkedRadioButton = (RadioButton) group.findViewById(checkedId);
                            for (Answer a : questions.get(fragment).getAnswers()) {
                                a.setSelected(0);
                            }

                            radioGroup4I1C_l.setOnCheckedChangeListener(null);
                            radioGroup4I1C_l.clearCheck();
                            radioGroup4I1C_l.setOnCheckedChangeListener(fourI_one_C_listener2);

                            questions.get(fragment).setAnswered(1);
                            questions.get(fragment).getAnswers().get(Integer.parseInt(checkedRadioButton.getHint().toString())).setSelected(1);
                            showFeedback(questions.get(fragment).getAnswers().get(Integer.parseInt(checkedRadioButton.getHint().toString())).getAdvise(),rootView.getContext());

                        }
                    }
                };

                fourI_one_C_listener2 = new RadioGroup.OnCheckedChangeListener() {
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        if(checkedId != -1) {
                            RadioButton checkedRadioButton = (RadioButton) group.findViewById(checkedId);
                            for (Answer a : questions.get(fragment).getAnswers()) {
                                a.setSelected(0);
                            }

                            radioGroup4I1C_r.setOnCheckedChangeListener(null);
                            radioGroup4I1C_r.clearCheck();
                            radioGroup4I1C_r.setOnCheckedChangeListener(fourI_one_C_listener1);

                            questions.get(fragment).setAnswered(1);
                            questions.get(fragment).getAnswers().get(Integer.parseInt(checkedRadioButton.getHint().toString())).setSelected(1);
                            showFeedback(questions.get(fragment).getAnswers().get(Integer.parseInt(checkedRadioButton.getHint().toString())).getAdvise(),rootView.getContext());

                        }
                    }
                };

                radioGroup4I1C_r.setOnCheckedChangeListener(fourI_one_C_listener1);
                radioGroup4I1C_l.setOnCheckedChangeListener(fourI_one_C_listener2);

            }

            if(typeOfQuestion.equals("one_choice_four_text")) {
                rootView = inflater.inflate(R.layout.fragment_question_one_choice_four_text, container, false);
                question = (TextView) rootView.findViewById(R.id.textViewQuestion1Choice4Text);
                question.setText(questions.get(fragment).getQuestion());

                LinkedList<Answer> answers = questions.get(fragment).getAnswers();

                RadioButton radio1 = (RadioButton) rootView.findViewById(R.id.radioButton4TText1);
                radio1.setText(answers.get(0).getAnswer());
                RadioButton radio2 = (RadioButton) rootView.findViewById(R.id.radioButton4TText2);
                radio2.setText(answers.get(1).getAnswer());
                RadioButton radio3 = (RadioButton) rootView.findViewById(R.id.radioButton4TText3);
                radio3.setText(answers.get(2).getAnswer());
                RadioButton radio4 = (RadioButton) rootView.findViewById(R.id.radioButton4TText4);
                radio4.setText(answers.get(3).getAnswer());

                final RadioGroup radioGroup4T1C_1 = (RadioGroup)rootView.findViewById(R.id.radioGroup4T1C_1);
                final RadioGroup radioGroup4T1C_2 = (RadioGroup)rootView.findViewById(R.id.radioGroup4T1C_2);
                radioGroup4T1C_1.clearCheck();
                radioGroup4T1C_2.clearCheck();

                if(questions.get(fragment).getAnswered() == 1) {
                    for(int i = 0; i < radioGroup4T1C_1.getChildCount(); i++){
                        radioGroup4T1C_1.getChildAt(i).setEnabled(false);
                    }
                    for(int i = 0; i < radioGroup4T1C_2.getChildCount(); i++){
                        radioGroup4T1C_2.getChildAt(i).setEnabled(false);
                    }
                    if(questions.get(fragment).getAnswers().get(0).getSelected() == 1) {
                        radio1.setChecked(true);
                    }
                    if(questions.get(fragment).getAnswers().get(1).getSelected() == 1) {
                        radio2.setChecked(true);
                    }
                    if(questions.get(fragment).getAnswers().get(2).getSelected() == 1) {
                        radio3.setChecked(true);
                    }
                    if(questions.get(fragment).getAnswers().get(3).getSelected() == 1) {
                        radio4.setChecked(true);
                    }
                }

                fourT_one_C_listener1 = new RadioGroup.OnCheckedChangeListener() {
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        if(checkedId != -1) {
                            RadioButton checkedRadioButton = (RadioButton) group.findViewById(checkedId);
                            for (Answer a: questions.get(fragment).getAnswers()) {
                                if(a.getAnswer().equals(checkedRadioButton.getText())) {
                                    a.setSelected(1);
                                    showFeedback(a.getAdvise(),rootView.getContext());
                                } else {
                                    a.setSelected(0);
                                }
                            }

                            radioGroup4T1C_2.setOnCheckedChangeListener(null);
                            radioGroup4T1C_2.clearCheck();
                            radioGroup4T1C_2.setOnCheckedChangeListener(fourT_one_C_listener2);

                            questions.get(fragment).setAnswered(1);

                        }
                    }
                };

                fourT_one_C_listener2 = new RadioGroup.OnCheckedChangeListener() {
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        if(checkedId != -1) {
                            RadioButton checkedRadioButton = (RadioButton) group.findViewById(checkedId);
                            for (Answer a: questions.get(fragment).getAnswers()) {
                                if(a.getAnswer().equals(checkedRadioButton.getText())) {
                                    a.setSelected(1);
                                    showFeedback(a.getAdvise(),rootView.getContext());
                                } else {
                                    a.setSelected(0);
                                }
                            }

                            radioGroup4T1C_1.setOnCheckedChangeListener(null);
                            radioGroup4T1C_1.clearCheck();
                            radioGroup4T1C_1.setOnCheckedChangeListener(fourT_one_C_listener1);

                            questions.get(fragment).setAnswered(1);

                        }
                    }
                };

                radioGroup4T1C_1.setOnCheckedChangeListener(fourT_one_C_listener1);
                radioGroup4T1C_2.setOnCheckedChangeListener(fourT_one_C_listener2);

            }

            if(typeOfQuestion.equals("multiple_choice_four_text")) {
                rootView = inflater.inflate(R.layout.fragment_question_multiple_choice_four_text, container, false);
                question = (TextView) rootView.findViewById(R.id.textViewQuestionXChoice4Text);
                question.setText(questions.get(fragment).getQuestion());

                LinkedList<Answer> answers = questions.get(fragment).getAnswers();

                TextView text1 = (TextView) rootView.findViewById(R.id.textViewXTText1);
                text1.setText(answers.get(0).getAnswer());
                TextView text2 = (TextView) rootView.findViewById(R.id.textViewXTText2);
                text2.setText(answers.get(1).getAnswer());
                TextView text3 = (TextView) rootView.findViewById(R.id.textViewXTText3);
                text3.setText(answers.get(2).getAnswer());
                TextView text4 = (TextView) rootView.findViewById(R.id.textViewXTText4);
                text4.setText(answers.get(3).getAnswer());

                CheckBox checkBoxXTText1 = (CheckBox) rootView.findViewById(R.id.checkBoxXTText1);
                CheckBox checkBoxXTText2 = (CheckBox) rootView.findViewById(R.id.checkBoxXTText2);
                CheckBox checkBoxXTText3 = (CheckBox) rootView.findViewById(R.id.checkBoxXTText3);
                CheckBox checkBoxXTText4 = (CheckBox) rootView.findViewById(R.id.checkBoxXTText4);

                if(questions.get(fragment).getAnswered() == 1) {
                    checkBoxXTText1.setEnabled(false);
                    checkBoxXTText2.setEnabled(false);
                    checkBoxXTText3.setEnabled(false);
                    checkBoxXTText4.setEnabled(false);
                    if(questions.get(fragment).getAnswers().get(0).getSelected() == 1) {
                        checkBoxXTText1.setChecked(true);
                    }
                    if(questions.get(fragment).getAnswers().get(1).getSelected() == 1) {
                        checkBoxXTText2.setChecked(true);
                    }
                    if(questions.get(fragment).getAnswers().get(2).getSelected() == 1) {
                        checkBoxXTText3.setChecked(true);
                    }
                    if(questions.get(fragment).getAnswers().get(3).getSelected() == 1) {
                        checkBoxXTText4.setChecked(true);
                    }
                } else {
                    checkBoxXTText1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                           @Override
                           public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                               if(buttonView.isChecked()){
                                   questions.get(fragment).setAnswered(1);
                                   questions.get(fragment).getAnswers().get(0).setSelected(1);
                                   showFeedback( questions.get(fragment).getAnswers().get(0).getAdvise(),rootView.getContext());
                               } else {
                                   questions.get(fragment).setAnswered(0);
                                   questions.get(fragment).getAnswers().get(0).setSelected(0);
                               }
                           }
                       }
                    );

                    checkBoxXTText2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                           @Override
                           public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                               if(buttonView.isChecked()){
                                   questions.get(fragment).setAnswered(1);
                                   questions.get(fragment).getAnswers().get(1).setSelected(1);
                                   showFeedback( questions.get(fragment).getAnswers().get(1).getAdvise(),rootView.getContext());
                               } else {
                                   questions.get(fragment).setAnswered(0);
                                   questions.get(fragment).getAnswers().get(1).setSelected(0);
                               }
                           }
                       }
                    );

                    checkBoxXTText3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                           @Override
                           public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                               if(buttonView.isChecked()){
                                   questions.get(fragment).setAnswered(1);
                                   questions.get(fragment).getAnswers().get(2).setSelected(1);
                                   showFeedback( questions.get(fragment).getAnswers().get(2).getAdvise(),rootView.getContext());
                               } else {
                                   questions.get(fragment).setAnswered(0);
                                   questions.get(fragment).getAnswers().get(2).setSelected(0);
                               }
                           }
                       }
                    );

                    checkBoxXTText4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                           @Override
                           public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                               if(buttonView.isChecked()){
                                   questions.get(fragment).setAnswered(1);
                                   questions.get(fragment).getAnswers().get(3).setSelected(1);
                                   showFeedback( questions.get(fragment).getAnswers().get(3).getAdvise(),rootView.getContext());
                               } else {
                                   questions.get(fragment).setAnswered(0);
                                   questions.get(fragment).getAnswers().get(3).setSelected(0);
                               }
                           }
                       }
                    );
                }
            }

            if(typeOfQuestion.equals("multiple_choice_four_images")) {
                rootView = inflater.inflate(R.layout.fragment_question_multiple_choice_four_images, container, false);
                question = (TextView) rootView.findViewById(R.id.textViewQuestionXChoice4Text);
                question.setText(questions.get(fragment).getQuestion());

                LinkedList<Answer> answers = questions.get(fragment).getAnswers();

                ImageView image1 = (ImageView) rootView.findViewById(R.id.imageViewX4IImage1);
                ImageView image2 = (ImageView) rootView.findViewById(R.id.imageViewX4IImage2);
                ImageView image3 = (ImageView) rootView.findViewById(R.id.imageViewX4IImage3);
                ImageView image4 = (ImageView) rootView.findViewById(R.id.imageViewX4IImage4);

                Picasso.with(rootView.getContext()).load(getUrl(answers.get(0).getAnswer())).into(image1);
                Picasso.with(rootView.getContext()).load(getUrl(answers.get(1).getAnswer())).into(image2);
                Picasso.with(rootView.getContext()).load(getUrl(answers.get(2).getAnswer())).into(image3);
                Picasso.with(rootView.getContext()).load(getUrl(answers.get(3).getAnswer())).into(image4);

                CheckBox checkBoxXTText1 = (CheckBox) rootView.findViewById(R.id.checkBoxXTText1);
                CheckBox checkBoxXTText2 = (CheckBox) rootView.findViewById(R.id.checkBoxXTText2);
                CheckBox checkBoxXTText3 = (CheckBox) rootView.findViewById(R.id.checkBoxXTText3);
                CheckBox checkBoxXTText4 = (CheckBox) rootView.findViewById(R.id.checkBoxXTText4);

                if(questions.get(fragment).getAnswered() == 1) {
                    checkBoxXTText1.setEnabled(false);
                    checkBoxXTText2.setEnabled(false);
                    checkBoxXTText3.setEnabled(false);
                    checkBoxXTText4.setEnabled(false);
                    if(questions.get(fragment).getAnswers().get(0).getSelected() == 1) {
                        checkBoxXTText1.setChecked(true);
                    }
                    if(questions.get(fragment).getAnswers().get(1).getSelected() == 1) {
                        checkBoxXTText2.setChecked(true);
                    }
                    if(questions.get(fragment).getAnswers().get(2).getSelected() == 1) {
                        checkBoxXTText3.setChecked(true);
                    }
                    if(questions.get(fragment).getAnswers().get(3).getSelected() == 1) {
                        checkBoxXTText4.setChecked(true);
                    }
                } else {
                    checkBoxXTText1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                           @Override
                           public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                               if(buttonView.isChecked()){
                                   questions.get(fragment).setAnswered(1);
                                   questions.get(fragment).getAnswers().get(0).setSelected(1);
                                   showFeedback( questions.get(fragment).getAnswers().get(0).getAdvise(),rootView.getContext());
                               } else {
                                   questions.get(fragment).setAnswered(0);
                                   questions.get(fragment).getAnswers().get(0).setSelected(0);
                               }
                           }
                       }
                    );

                    checkBoxXTText2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                           @Override
                           public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                               if(buttonView.isChecked()){
                                   questions.get(fragment).setAnswered(1);
                                   questions.get(fragment).getAnswers().get(1).setSelected(1);
                                   showFeedback( questions.get(fragment).getAnswers().get(1).getAdvise(),rootView.getContext());
                               } else {
                                   questions.get(fragment).setAnswered(0);
                                   questions.get(fragment).getAnswers().get(1).setSelected(0);
                               }
                           }
                       }
                    );

                    checkBoxXTText3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                           @Override
                           public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                               if(buttonView.isChecked()){
                                   questions.get(fragment).setAnswered(1);
                                   questions.get(fragment).getAnswers().get(2).setSelected(1);
                                   showFeedback( questions.get(fragment).getAnswers().get(2).getAdvise(),rootView.getContext());
                               } else {
                                   questions.get(fragment).setAnswered(0);
                                   questions.get(fragment).getAnswers().get(2).setSelected(0);
                               }
                           }
                       }
                    );

                    checkBoxXTText4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                           @Override
                           public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                               if(buttonView.isChecked()){
                                   questions.get(fragment).setAnswered(1);
                                   questions.get(fragment).getAnswers().get(3).setSelected(1);
                                   showFeedback( questions.get(fragment).getAnswers().get(3).getAdvise(),rootView.getContext());
                               } else {
                                   questions.get(fragment).setAnswered(0);
                                   questions.get(fragment).getAnswers().get(3).setSelected(0);
                               }
                           }
                       }
                    );
                }
            }

            return rootView;
        }

    }

    private static String getUrl(String server_url) {
        URL url = null;
        try {
            url = new URL(Savergy.INSTANCE.getAPI_URL());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        URL final_url = null;
        try {
            final_url = new URL(url,server_url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return final_url.toString();
    }

    public static void showFeedback(String feedback, Context context) {
        if(!feedback.equals("null") && !feedback.isEmpty()) {
            LayoutInflater li = LayoutInflater.from(context);
            View promptsView = li.inflate(R.layout.dialog_remove_account_confirm, null);

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

            alertDialogBuilder.setView(promptsView);

            final TextView dialogIntro = (TextView) promptsView.findViewById(R.id.textViewEqDialogQuestion);
            dialogIntro.setText(feedback);

            alertDialogBuilder
                    .setCancelable(false)
                    .setNegativeButton("Fechar",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }

    private void saveAwnsers() {
        Long userId = Long.parseLong(Savergy.INSTANCE.getCurrentUser().getDeviceId());

        dbHelper = new DatabaseHelper(this);

        if(id == 0) {
            dbHelper.updateQuestionByBatchID(userId, 1, questions);
            Savergy.INSTANCE.uploadProfileQuestions(this);
        } else {
            dbHelper.updateQuestionByBatchID(userId, id, questions);
            Savergy.INSTANCE.uploadBatchesList(this);
        }

        if(NetworkUtility.hasConnectivity(QuestionsActivity.this)) {
            SetAnswersTask setAnswersTask = new SetAnswersTask();
            setAnswersTask.execute(Savergy.INSTANCE.getAPI_TOKEN(),String.valueOf(id));
        }

    }
}

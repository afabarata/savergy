package pt.ipleiria.estg.dei.savergy.helpers;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;

import pt.ipleiria.estg.dei.savergy.database.DatabaseHelper;
import pt.ipleiria.estg.dei.savergy.model.Advice;
import pt.ipleiria.estg.dei.savergy.model.Answer;
import pt.ipleiria.estg.dei.savergy.model.Batch;
import pt.ipleiria.estg.dei.savergy.model.Equipment;
import pt.ipleiria.estg.dei.savergy.model.Question;
import pt.ipleiria.estg.dei.savergy.model.User;

/**
 * Created by Trabalho on 25/03/2017.
 */

public class JsonHelper {

    private DatabaseHelper dbHelper;

    public JsonHelper() {
        //no instance
    }

    public String loadJSONFromAsset(Context context, String file) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(file);

            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

        return json;
    }

    /*public User readUserFromJson(Context context, String file) {
        String json = loadJSONFromAsset(context,file);
        User user;

        try {
            JSONObject obj = new JSONObject(json);

            if(file.equals("user.json")) {

                user = new User(Savergy.INSTANCE.getCurrentUser().getDeviceId());
                user.setSocialId(obj.getString("socialid"));
                user.setAvatar(obj.getInt("avatar"));
                user.setUsername(obj.getString("username"));
                user.setEmail(obj.getString("email"));
                user.setName(obj.getString("name"));
                user.setPassword(obj.getString("password"));

                return user;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }*/

    public LinkedList<Batch> readBatchesFromJson(Context context, String file) {
        String json = loadJSONFromAsset(context,file);

        LinkedList<Batch> batches = new LinkedList<>();

        Batch batch;
        Question question;
        Answer answer;

        try {
            JSONObject obj = new JSONObject(json);

            if(file.equals("batches.json")) {
                //Savergy.INSTANCE.startTesteTimeCounter();

                JSONArray js_array = obj.getJSONArray("batches");

                for (int i = 0; i < js_array.length(); i++) {
                    JSONObject js_array_n2 = js_array.getJSONObject(i);

                    batch = new Batch(js_array_n2.getInt("id"),js_array_n2.getString("name"),js_array_n2.getInt("nperguntas"),js_array_n2.getInt("maxscore"));
                    batch.setScore(js_array_n2.getInt("score"));
                    batch.setNanswered(js_array_n2.getInt("nanswered"));

                    JSONArray js_array_2 = js_array_n2.getJSONArray("questions");

                    for (int z = 0; z < js_array_2.length(); z++) {
                        JSONObject js_array_n3 = js_array_2.getJSONObject(z);

                        question = new Question(js_array_n3.getInt("id"),js_array_n2.getInt("id"),js_array_n3.getString("question"),js_array_n3.getString("answer_type"));
                        question.setAnswered(js_array_n3.getInt("answered"));

                        JSONArray js_array_3 = js_array_n3.getJSONArray("answers");

                        for (int y = 0; y < js_array_3.length(); y++) {
                            JSONObject js_array_n4 = js_array_3.getJSONObject(y);

                            answer = new Answer(js_array_n4.getInt("id"),js_array_n3.getInt("id"),js_array_n4.getString("answer"),js_array_n4.getString("feedback"),js_array_n4.getInt("points"));
                            answer.setSelected(js_array_n4.getInt("selected"));
                            question.addAnswer(answer);

                        }

                        batch.addQuestion(question);
                    }

                    batches.add(batch);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return batches;
    }

    public LinkedList<Batch> readBatchesFromServer(String json) {
        LinkedList<Batch> batches = new LinkedList<>();

        Batch batch;
        Question question;
        Answer answer;

        try {
            JSONObject obj = new JSONObject(json);

            //Savergy.INSTANCE.startTesteTimeCounter();

            JSONArray js_array = obj.getJSONArray("batches");

            for (int i = 0; i < js_array.length(); i++) {
                JSONObject js_array_n2 = js_array.getJSONObject(i);

                batch = new Batch(js_array_n2.getInt("id"),js_array_n2.getString("name"),js_array_n2.getInt("nquestions"),js_array_n2.getInt("maxscore"));
                batch.setScore(js_array_n2.getInt("score"));
                batch.setNanswered(js_array_n2.getInt("nanswered"));

                JSONArray js_array_2 = js_array_n2.getJSONArray("questions");

                for (int z = 0; z < js_array_2.length(); z++) {
                    JSONObject js_array_n3 = js_array_2.getJSONObject(z);

                    question = new Question(js_array_n3.getInt("id"),js_array_n2.getInt("id"),js_array_n3.getString("question"),js_array_n3.getString("answer_typ"));
                    question.setAnswered(js_array_n3.getInt("answered"));

                    JSONArray js_array_3 = js_array_n3.getJSONArray("answers");

                    for (int y = 0; y < js_array_3.length(); y++) {
                        JSONObject js_array_n4 = js_array_3.getJSONObject(y);

                        answer = new Answer(js_array_n4.getInt("id"),js_array_n3.getInt("id"),js_array_n4.getString("answer"),js_array_n4.getString("feedback"),js_array_n4.getInt("points"));
                        answer.setSelected(js_array_n4.getInt("selected"));
                        question.addAnswer(answer);

                    }

                    batch.addQuestion(question);
                }

                batches.add(batch);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return batches;
    }

    public LinkedList<Batch> readProfileBatchFromJson(Context context, String file) {
        String json = loadJSONFromAsset(context,file);

        LinkedList<Batch> categories = new LinkedList<>();

        Batch batch;
        Question question;
        Answer answer;

        try {
            JSONObject obj = new JSONObject(json);

            if(file.equals("profile-questions.json")) {

                JSONArray js_array = obj.getJSONArray("batches");

                for (int i = 0; i < js_array.length(); i++) {
                    JSONObject js_array_n2 = js_array.getJSONObject(i);

                    batch = new Batch(js_array_n2.getInt("id"),js_array_n2.getString("name"),js_array_n2.getInt("nperguntas"),js_array_n2.getInt("maxscore"));
                    batch.setScore(js_array_n2.getInt("score"));
                    batch.setNanswered(js_array_n2.getInt("nanswered"));

                    JSONArray js_array_2 = js_array_n2.getJSONArray("questions");

                    for (int z = 0; z < js_array_2.length(); z++) {
                        JSONObject js_array_n3 = js_array_2.getJSONObject(z);

                        question = new Question(js_array_n3.getInt("id"),js_array_n2.getInt("id"),js_array_n3.getString("question"),js_array_n3.getString("answer_type"));
                        question.setAnswered(js_array_n3.getInt("answered"));

                        JSONArray js_array_3 = js_array_n3.getJSONArray("answers");

                        for (int y = 0; y < js_array_3.length(); y++) {
                            JSONObject js_array_n4 = js_array_3.getJSONObject(y);

                            answer = new Answer(js_array_n4.getInt("id"),js_array_n3.getInt("id"),js_array_n4.getString("answer"),js_array_n4.getString("feedback"),js_array_n4.getInt("points"));
                            answer.setSelected(js_array_n4.getInt("selected"));
                            question.addAnswer(answer);

                        }

                        batch.addQuestion(question);
                    }

                    categories.add(batch);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return categories;

    }

    public LinkedList<Equipment> readEquipmentFromJson(Context context, String file) {
        String json = loadJSONFromAsset(context,file);

        LinkedList<Equipment> equipments = new LinkedList<>();

        Equipment equipment;

        try {
            JSONObject obj = new JSONObject(json);

            if(file.equals("equipments.json")) {

                //equipamentos
                JSONArray js_array_2 = obj.getJSONArray("equipments");

                for (int z = 0; z < js_array_2.length(); z++) {
                    JSONObject js_array_n3 = js_array_2.getJSONObject(z);

                    equipment = new Equipment(js_array_n3.getInt("id"),js_array_n3.getString("name"),
                            js_array_n3.getString("icon"),js_array_n3.getString("description"), js_array_n3.getInt("isSelected"), js_array_n3.getInt("defaultUseTime"), js_array_n3.getInt("defaultPower"),
                            js_array_n3.getInt("configuredUseTime"), js_array_n3.getInt("configuredPower"));
                    equipments.add(equipment);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return equipments;

    }

    public LinkedList<Equipment> readEquipmentFromServer(String json) {
        LinkedList<Equipment> equipments = new LinkedList<>();

        Equipment equipment;

        try {
            JSONObject obj = new JSONObject(json);

            JSONArray js_array_2 = obj.getJSONArray("equipments");

            for (int z = 0; z < js_array_2.length(); z++) {
                JSONObject js_array_n3 = js_array_2.getJSONObject(z);

                equipment = new Equipment(js_array_n3.getInt("id"),js_array_n3.getString("name"),
                        js_array_n3.getString("icon"),js_array_n3.getString("description"), js_array_n3.getInt("isSelected"), js_array_n3.getInt("defaultUseTime"), js_array_n3.getInt("defaultPower"),
                        js_array_n3.getInt("configuredUseTime"), js_array_n3.getInt("configuredPower"));
                equipments.add(equipment);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return equipments;

    }

    public LinkedList<Advice> readAdvicesFromJson(Context context, String file) {
        String json = loadJSONFromAsset(context,file);

        LinkedList<Advice> advices = new LinkedList<>();

        Advice advice;

        try {
            JSONObject obj = new JSONObject(json);

            if(file.equals("advices.json")) {

                JSONArray js_array_1 = obj.getJSONArray("advices");

                for (int z = 0; z < js_array_1.length(); z++) {
                    JSONObject js_array_n1 = js_array_1.getJSONObject(z);

                    advice = new Advice(js_array_n1.getInt("id"), js_array_n1.getString("advice"));
                    advice.setIsOld(js_array_n1.getInt("isOld"));
                    advices.add(advice);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return advices;

    }

    public LinkedList<Advice> readAdvicesFromServer(String json) {
        LinkedList<Advice> advices = new LinkedList<>();

        Advice advice;

        try {
            JSONObject obj = new JSONObject(json);
            JSONArray js_array_1 = obj.getJSONArray("advices");

            for (int z = 0; z < js_array_1.length(); z++) {
                JSONObject js_array_n1 = js_array_1.getJSONObject(z);

                advice = new Advice(js_array_n1.getInt("advice_id"), js_array_n1.getString("message"));
                advice.setIsOld(js_array_n1.getInt("isOld"));
                advices.add(advice);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return advices;

    }

    public LinkedList<User> readRankingFromServer(String json) {
        LinkedList<User> users = new LinkedList<>();

        User user;

        try {
            JSONObject obj = new JSONObject(json);
            JSONArray js_array_1 = obj.getJSONArray("users");

            for (int z = 0; z < js_array_1.length(); z++) {
                JSONObject js_array_n1 = js_array_1.getJSONObject(z);

                user = new User(String.valueOf(js_array_n1.getInt("id")));
                user.setName(js_array_n1.getString("name"));
                user.setAvatar(js_array_n1.getInt("avatar"));
                user.setNumPoints(js_array_n1.getInt("score"));
                user.setRank(js_array_n1.getInt("rank"));

                users.add(user);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return users;


    }

    public void insertBatchesToBD(Context context, LinkedList<Batch> batches) {
        dbHelper = new DatabaseHelper(context);
        dbHelper.deleteBatches();
        dbHelper.insertBatches(batches);

        //dbHelper.toStringBatches();
        //dbHelper.toStringQuestions();
        //dbHelper.toStringAnswers();
    }

    public void insertEquipmentsToBD(Context context, LinkedList<Equipment> equipments) {
        dbHelper = new DatabaseHelper(context);
        dbHelper.deleteEquipments();
        dbHelper.insertEquipments(equipments);
    }

    public void insertAdvicesToBD(Context context, LinkedList<Advice> advices) {
        dbHelper = new DatabaseHelper(context);
        dbHelper.deleteAdvices();
        dbHelper.insertAdvices(advices);
    }



    /*public void createQuestionsJsonResponse(Long id) {
        LinkedList<Question> questions = Savergy.INSTANCE.getQuestionsByBatchServerId(id);
        JSONObject root = new JSONObject();
        JSONArray arr = new JSONArray();

        try {
            for (Question q: questions) {
                for (Answer a: q.getAnswers()) {
                    if(a.getSelected() == 1) {
                        JSONObject arr_root = new JSONObject();
                        arr_root.put("batch_id",id);
                        arr_root.put("answer_id",a.getServerid());
                        arr_root.put("question_id",q.getServerid());
                        arr.put((Object)arr_root);
                    }
                }
            }
            root.put("user",Savergy.INSTANCE.getCurrentUser().getDeviceId());
            root.put("awnsers",(Object)arr);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            Writer output = null;
            String path = "storage/sdcard0/Download";
            String data = path+"/test.json";

            File dir = new File(path);
            if (!dir.exists()) {
                dir.mkdirs();
            }

            File file = new File(data);
            if (!file.createNewFile()) {
                file.delete();
                file.createNewFile();
            }

            output = new BufferedWriter(new FileWriter(file));
            output.write(root.toString());
            output.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
}

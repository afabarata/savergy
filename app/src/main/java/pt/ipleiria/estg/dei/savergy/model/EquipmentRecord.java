package pt.ipleiria.estg.dei.savergy.model;

import java.util.Date;

/**
 * Created by Trabalho on 28/06/2017.
 */

public class EquipmentRecord {

    private int id;
    private int equipmentid;
    private String date;
    private int configuredUseTime;
    private int configuredPower;

    public EquipmentRecord(int equipmentid, String date, int configuredUseTime, int configuredPower) {
        this.equipmentid = equipmentid;
        this.date = date;
        this.configuredUseTime = configuredUseTime;
        this.configuredPower = configuredPower;
    }

    public int getEquipmentid() {
        return equipmentid;
    }

    public void setEquipmentid(int equipmentid) {
        this.equipmentid = equipmentid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getConfiguredUseTime() {
        return configuredUseTime;
    }

    public void setConfiguredUseTime(int configuredUseTime) {
        this.configuredUseTime = configuredUseTime;
    }

    public int getConfiguredPower() {
        return configuredPower;
    }

    public void setConfiguredPower(int configuredPower) {
        this.configuredPower = configuredPower;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("\n-- Reading --\n"
                + "ID: " + equipmentid + "\n"
                + "Date: " + date + "\n"
                + "configuredUseTime: " + configuredUseTime + "\n"
                + "configuredPower: " + configuredPower + "\n"
                + "\n");

        return sb.toString();
    }
}

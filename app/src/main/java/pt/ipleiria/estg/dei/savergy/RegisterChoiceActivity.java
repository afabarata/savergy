package pt.ipleiria.estg.dei.savergy;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import pt.ipleiria.estg.dei.savergy.helpers.LoginHelper;
import pt.ipleiria.estg.dei.savergy.helpers.NetworkUtility;

/**
 * Created by Trabalho on 14/03/2017.
 */

public class RegisterChoiceActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private static final int RC_SIGN_IN = 9001;
    private static final String TAG = "SignInActivity";

    private CallbackManager callbackManager;
    private GoogleApiClient mGoogleApiClient;

    private LoginHelper loginHelper= null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginHelper = new LoginHelper(this);
        setContentView(R.layout.activity_register_choice);

        LoginButton facebook = (LoginButton) findViewById(R.id.login_button);
        facebook.setReadPermissions(Arrays.asList("user_about_me", "email"));
        SignInButton google = (SignInButton) findViewById(R.id.sign_in_button);

        if(!NetworkUtility.hasConnectivity(RegisterChoiceActivity.this)) {
            facebook.setEnabled(false);
            google.setEnabled(false);
        }

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.d("LoginActivity", response.toString());

                        try {
                            String id = object.getString("id");
                            String name = object.getString("name");
                            Log.i("FB NAME:", name);
                            Log.i("FB ID:", id);
                            loginHelper.RegisterUserWithSocialID(name,id);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email");
                request.setParameters(parameters);
                request.executeAsync();

                Intent intent = new Intent(RegisterChoiceActivity.this, DashboardActivity.class);
                startActivity(intent);
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException exception) {
                Log.d("Error:",exception.toString());
                Toast.makeText(RegisterChoiceActivity.this, R.string.register_choice_social_error,Toast.LENGTH_SHORT).show();
            }
        });

        findViewById(R.id.sign_in_button).setOnClickListener(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        mGoogleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */).addApi(Auth.GOOGLE_SIGN_IN_API, gso).build();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(RegisterChoiceActivity.this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();

            Log.i("GOOGLE NAME:", acct.getDisplayName());
            Log.i("GOOGLE ID:", acct.getId());
            loginHelper.RegisterUserWithSocialID(acct.getDisplayName(), acct.getId());

            Intent intent = new Intent(this, DashboardActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(RegisterChoiceActivity.this, R.string.register_choice_social_error,Toast.LENGTH_SHORT).show();
        }
    }

    public void onClickRegisterByEmail(View view) {
        if(NetworkUtility.hasConnectivity(RegisterChoiceActivity.this)) {
            Intent intent = new Intent(this, RegisterByEmailActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(this, R.string.register_choice_turn_on_internet,Toast.LENGTH_SHORT).show();
        }
    }

    public void onClickLoginByEmail(View view) {
        if(NetworkUtility.hasConnectivity(RegisterChoiceActivity.this)) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(this, R.string.register_choice_turn_on_internet,Toast.LENGTH_SHORT).show();
        }
    }


    public void onClickNoRegister(View view) {
        Intent intent = new Intent(this, DashboardActivity.class);
        startActivity(intent);
    }

}

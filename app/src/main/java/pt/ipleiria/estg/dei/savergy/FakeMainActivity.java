package pt.ipleiria.estg.dei.savergy;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import java.util.concurrent.ExecutionException;
import pt.ipleiria.estg.dei.savergy.database.DatabaseHelper;
import pt.ipleiria.estg.dei.savergy.helpers.ConnectionsHelper;
import pt.ipleiria.estg.dei.savergy.helpers.LoginHelper;
import pt.ipleiria.estg.dei.savergy.helpers.NetworkUtility;
import pt.ipleiria.estg.dei.savergy.model.Savergy;

public class FakeMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String[] perms = {"android.permission.READ_PHONE_STATE"};
        int permsRequestCode = 200;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(perms, permsRequestCode);
        } else {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {

    }

    private boolean canMakeSmores(){
        return(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults){
        switch(permsRequestCode){
            case 200:
                boolean phoneStateAccepted = grantResults[0]== PackageManager.PERMISSION_GRANTED;
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;
            default:
                LayoutInflater li = LayoutInflater.from(FakeMainActivity.this);
                View promptsView = li.inflate(R.layout.dialog_remove_account_confirm, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(FakeMainActivity.this);

                alertDialogBuilder.setView(promptsView);

                final TextView dialogIntro = (TextView) promptsView.findViewById(R.id.textViewEqDialogQuestion);
                dialogIntro.setText(R.string.permission_error);

                alertDialogBuilder
                        .setCancelable(false)
                        .setNegativeButton("Compreendi",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        finish();
                                        moveTaskToBack(true);
                                    }
                                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
        }
    }
}


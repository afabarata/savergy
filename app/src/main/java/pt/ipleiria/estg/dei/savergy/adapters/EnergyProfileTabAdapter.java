package pt.ipleiria.estg.dei.savergy.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import pt.ipleiria.estg.dei.savergy.R;
import pt.ipleiria.estg.dei.savergy.fragments.EnergyProfileTabFragment;

/**
 * Created by Igor on 23/03/2017.
 */

public class EnergyProfileTabAdapter extends FragmentPagerAdapter {

    Context mContext;

    public EnergyProfileTabAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = new EnergyProfileTabFragment();

        Bundle args = new Bundle();
        args.putInt("fragment", position);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.profile_tab_data);
            case 1:
                return mContext.getString(R.string.profile_tab_questions);
            case 2:
                return mContext.getString(R.string.profile_tab_eq);
        }

        return null;
    }

}
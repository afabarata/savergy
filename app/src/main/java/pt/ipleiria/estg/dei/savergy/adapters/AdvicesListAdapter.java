package pt.ipleiria.estg.dei.savergy.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Iterator;

import pt.ipleiria.estg.dei.savergy.DashboardActivity;
import pt.ipleiria.estg.dei.savergy.R;
import pt.ipleiria.estg.dei.savergy.model.Advice;
import pt.ipleiria.estg.dei.savergy.model.Savergy;

/**
 * Created by Trabalho on 19/04/2017.
 */

public class AdvicesListAdapter  extends ArrayAdapter<Advice> {

    private static final class ViewHolder {
        public TextView textViewAdvice;
        public Button seeMore;

        public ViewHolder(View view) {
            textViewAdvice = (TextView) view.findViewById(R.id.textViewAdvice);
            seeMore = (Button) view.findViewById(R.id.buttonSeen);
        }

        public void update(Advice advice) {
            if(advice.getAdvice().length() > 29) {
                textViewAdvice.setText(String.valueOf(advice.getAdvice()).substring(0,30)+"...");
            } else {
                textViewAdvice.setText(advice.getAdvice());
                seeMore.setVisibility(View.INVISIBLE);
            }

            if(advice.getIsOld() == 0) {
                textViewAdvice.setTextColor(Color.parseColor("#ffffbb33"));
            }
        }
    }

    public AdvicesListAdapter(Context context, Iterable<Advice> objects) {
        this(context, objects.iterator());
    }

    public AdvicesListAdapter(Context context,  Iterator<Advice> objects) {
        super(context, R.layout.advice_list_item, R.id.textViewAdvice);
        while (objects.hasNext())
            add(objects.next());
    }

    @NonNull
    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        final Advice advice = getItem(position);

        ViewHolder holder = (ViewHolder)view.getTag();

        if (holder==null) { // First time I've seen this View: let's tag it!
            holder = new ViewHolder(view);

            Button seeMore = (Button) view.findViewById(R.id.buttonSeen);
            seeMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {

                    LayoutInflater li = LayoutInflater.from(view.getContext());
                    View promptsView = li.inflate(R.layout.dialog_all_advice, null);

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(view.getContext());
                    alertDialogBuilder.setView(promptsView);

                    final TextView dialogText= (TextView) promptsView.findViewById(R.id.textViewAllAdvice);
                    dialogText.setText(advice.getAdvice());

                    alertDialogBuilder
                            .setCancelable(false)
                            .setNegativeButton("Fechar",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            Savergy.INSTANCE.getAdvices().get(position).setIsOld(1);
                                            dialog.cancel();
                                        }
                                    });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                }
            });

            view.setTag(holder);
        }

        holder.update(advice);
        return view;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getServerid();
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

}

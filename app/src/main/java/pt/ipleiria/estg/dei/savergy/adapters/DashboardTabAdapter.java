package pt.ipleiria.estg.dei.savergy.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import pt.ipleiria.estg.dei.savergy.R;
import pt.ipleiria.estg.dei.savergy.fragments.DashboardTabFragment;

/**
 * Created by Trabalho on 19/03/2017.
 */

public class DashboardTabAdapter extends FragmentPagerAdapter {

    Context mContext;

    public DashboardTabAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = new DashboardTabFragment();

        Bundle args = new Bundle();
        args.putInt("fragment", position);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.dashboard_tab_home);
            case 1:
                return mContext.getString(R.string.dashboard_tab_quizzes);
            case 2:
                return mContext.getString(R.string.dashboard_tab_readings);
        }

        return null;
    }

}

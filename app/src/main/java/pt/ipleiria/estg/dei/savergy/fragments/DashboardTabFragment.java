package pt.ipleiria.estg.dei.savergy.fragments;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import at.grabner.circleprogress.CircleProgressView;
import at.grabner.circleprogress.TextMode;
import pt.ipleiria.estg.dei.savergy.DashboardActivity;
import pt.ipleiria.estg.dei.savergy.EquipmentsListActivity;
import pt.ipleiria.estg.dei.savergy.QuestionsActivity;
import pt.ipleiria.estg.dei.savergy.R;
import pt.ipleiria.estg.dei.savergy.database.DatabaseHelper;
import pt.ipleiria.estg.dei.savergy.model.Batch;
import pt.ipleiria.estg.dei.savergy.model.Equipment;
import pt.ipleiria.estg.dei.savergy.model.Savergy;

/**
 * Created by Trabalho on 19/03/2017.
 */

public class DashboardTabFragment extends Fragment {

    View rootView;
    private static final int REFRESH_DASHBOARD = 2;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        Bundle args = getArguments();
        DatabaseHelper dbHelper = new DatabaseHelper(getContext());

        switch (args.getInt("fragment")) {
            case 0:

                Configuration config = getResources().getConfiguration();
                if (config.smallestScreenWidthDp >= 330)
                {
                    rootView = inflater.inflate(R.layout.fragment_dashboard_tab, container, false);
                }
                else
                {
                    rootView = inflater.inflate(R.layout.fragment_dashboard_tab_xs, container, false);
                }

                //PERGUNTAS DE PERFIL POR RESPONDER //

                CardView cardViewDashboardProfileQuestions = (CardView) rootView.findViewById(R.id.cardViewDashboardProfileQuestions);
                CircleProgressView circleViewDashboardProfileQuestions = (CircleProgressView) rootView.findViewById(R.id.circleViewDashboardProfileQuestions);
                TextView textViewDashboardProfileQuestions = (TextView) rootView.findViewById(R.id.textViewDashboardProfileQuestions);

                int numProfileQuestions = Savergy.INSTANCE.getProfileQuestions().getNquestions();
                int numAnsweredProfileQuestions = Savergy.INSTANCE.getProfileQuestions().getNanswered();
                int difAnsweredProfileQuestions = numProfileQuestions - numAnsweredProfileQuestions;

                circleViewDashboardProfileQuestions.setMaxValue(numProfileQuestions);
                circleViewDashboardProfileQuestions.setBlockCount(numProfileQuestions);
                circleViewDashboardProfileQuestions.setText(String.valueOf(numAnsweredProfileQuestions));
                circleViewDashboardProfileQuestions.setUnitVisible(true);
                circleViewDashboardProfileQuestions.setValue(numAnsweredProfileQuestions);

                if(difAnsweredProfileQuestions == 0) {
                    textViewDashboardProfileQuestions.setText("Não falta nenhuma questão para completar o conjunto");
                }

                if(difAnsweredProfileQuestions == 1) {
                    textViewDashboardProfileQuestions.setText("Falta 1 questão para completar o conjunto");
                }

                if(difAnsweredProfileQuestions > 1) {
                    textViewDashboardProfileQuestions.setText("Faltam " + difAnsweredProfileQuestions + " questões para completar o conjunto");
                }

                final int finalDifAnsweredProfileQuestions = difAnsweredProfileQuestions;
                cardViewDashboardProfileQuestions.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(finalDifAnsweredProfileQuestions != 0) {
                            Intent intent = new Intent(getContext(), QuestionsActivity.class);
                            intent.putExtra(QuestionsActivity.BATCHSERVERID, 0);
                            ((DashboardActivity)getActivity()).startActivityForResult(intent, REFRESH_DASHBOARD);
                        }
                    }
                });

                //PERGUNTAS DE PERFIL POR RESPONDER //

                //BATCHES POR COMPLETAR //

                CardView cardViewDashboardBatchesNotFull = (CardView) rootView.findViewById(R.id.cardViewDashboardBatchesNotFull);
                CircleProgressView circleViewDashboardBatchesNotFull = (CircleProgressView) rootView.findViewById(R.id.circleViewDashboardBatchesNotFull);
                TextView textViewDashboardBatchesNotFull = (TextView) rootView.findViewById(R.id.textViewDashboardBatchesNotFull);

                int numBatches = Savergy.INSTANCE.getBatches().size();
                int numFullBatches = 0;

                for(Batch b: Savergy.INSTANCE.getBatches()) {
                    if(b.getNquestions() == b.getNanswered()) {
                        numFullBatches++;
                    }
                }

                int difFullBatches = numBatches - numFullBatches;
                int percentFullBatches = ((numFullBatches / numBatches) * 100);

                circleViewDashboardBatchesNotFull.setMaxValue(numBatches);
                circleViewDashboardBatchesNotFull.setBlockCount(numBatches);
                circleViewDashboardBatchesNotFull.setText(String.valueOf(percentFullBatches));
                circleViewDashboardBatchesNotFull.setUnitVisible(true);
                circleViewDashboardBatchesNotFull.setValue(numFullBatches);

                if(difFullBatches == 0) {
                    textViewDashboardBatchesNotFull.setText("Não falta nenhum quiz para terminar");
                }

                if(difFullBatches == 1) {
                    textViewDashboardBatchesNotFull.setText("Falta 1 quiz para terminar");
                }

                if(difFullBatches > 1) {
                    textViewDashboardBatchesNotFull.setText("Faltam " + difFullBatches + " quizzes para terminar");
                }

                final int finalDifFullBatches = difFullBatches;
                cardViewDashboardBatchesNotFull.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(finalDifFullBatches != 0) {
                            ((DashboardActivity)getActivity()).changeTab(1);
                        }
                    }
                });

                //BATCHES POR COMPLETAR //

                //PONTOS //


                CardView cardViewDashboardPoints = (CardView) rootView.findViewById(R.id.cardViewDashboardPoints);
                CircleProgressView circleViewDashboardPoints = (CircleProgressView) rootView.findViewById(R.id.circleViewDashboardPoints);
                TextView textViewDashboardPoints = (TextView) rootView.findViewById(R.id.textViewDashboardPoints);

                int currentPoints = Savergy.INSTANCE.getCurrentUser().getNumPoints();
                int totalPoints = 0;

                for(Batch b: Savergy.INSTANCE.getBatches()) {
                    totalPoints += b.getMaxscore();
                }

                circleViewDashboardPoints.setMaxValue(totalPoints);
                circleViewDashboardPoints.setBlockCount(totalPoints);
                circleViewDashboardPoints.setText(String.valueOf(currentPoints));
                circleViewDashboardPoints.setTextMode(TextMode.TEXT);
                circleViewDashboardPoints.setValue(currentPoints);

                if(currentPoints == totalPoints) {
                    textViewDashboardPoints.setText("Parabéns, atingiu o máximo atual de pontos");
                } else {
                    textViewDashboardPoints.setText("Responda a mais quizzes para aumente os seus pontos!");
                }

                final int finalCurrentPoints = currentPoints;
                final int finalTotalPoints = totalPoints;
                cardViewDashboardPoints.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(finalCurrentPoints != finalTotalPoints) {
                            ((DashboardActivity)getActivity()).changeTab(1);
                        }
                    }
                });


                //PONTOS //

                //BATCH PERTO DE TERMINAR //

                CardView cardViewDashboardBatchNearFull = (CardView) rootView.findViewById(R.id.cardViewDashboardBatchNearFull);
                CircleProgressView circleViewDashboardBatchNearFull = (CircleProgressView) rootView.findViewById(R.id.circleViewDashboardBatchNearFull);
                TextView textViewDashboardBatchNearFull = (TextView) rootView.findViewById(R.id.textViewDashboardBatchNearFull);

                int id = 0;
                String nome = "";

                for(Batch b: Savergy.INSTANCE.getBatches()) {
                    if((b.getNquestions() - b.getNanswered()) <= 3 && b.getNquestions() != b.getNanswered()) {
                        id = b.getServerid();
                        nome = b.getBatch();

                    }
                }

                circleViewDashboardBatchNearFull.setMaxValue(numBatches);
                circleViewDashboardBatchNearFull.setBlockCount(numBatches);
                circleViewDashboardBatchNearFull.setText(nome);
                circleViewDashboardBatchNearFull.setTextMode(TextMode.TEXT);
                circleViewDashboardBatchNearFull.setValue(numBatches);

                if(id == 0) {
                    circleViewDashboardBatchNearFull.setText("N/A");
                    textViewDashboardBatchNearFull.setText("Não tem quizzes perto de serem finalizados");
                } else {
                    textViewDashboardBatchNearFull.setText("Tem 1 quiz perto de ser finalizado");
                }

                final int finalId = id;
                cardViewDashboardBatchNearFull.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(finalId == 0) {
                            ((DashboardActivity)getActivity()).changeTab(1);
                        } else {
                            Intent intent = new Intent(getContext(), QuestionsActivity.class);
                            intent.putExtra(QuestionsActivity.BATCHSERVERID, finalId);
                            ((DashboardActivity)getActivity()).startActivityForResult(intent, REFRESH_DASHBOARD);
                        }
                    }
                });

                //BATCH PERTO DE TERMINAR //

                //EQUIPAMENTOS SELECIONADOS//

                CardView cardViewDashboardEquipmentsSelected = (CardView) rootView.findViewById(R.id.cardViewDashboardEquipmentsSelected);
                CircleProgressView circleViewDashboardEquipmentsSelected = (CircleProgressView) rootView.findViewById(R.id.circleViewDashboardEquipmentsSelected);
                TextView textViewDashboardEquipmentsSelected = (TextView) rootView.findViewById(R.id.textViewDashboardEquipmentsSelected);

                int numEquip = Savergy.INSTANCE.getEquipments().size();
                int numSelectedEquip = Savergy.INSTANCE.getSelectedEquipments().size();

                circleViewDashboardEquipmentsSelected.setMaxValue(numEquip);
                circleViewDashboardEquipmentsSelected.setBlockCount(numEquip);
                circleViewDashboardEquipmentsSelected.setText(String.valueOf(numSelectedEquip));
                circleViewDashboardEquipmentsSelected.setUnitVisible(true);
                circleViewDashboardEquipmentsSelected.setValue(numSelectedEquip);

                if(numSelectedEquip == 0) {
                    textViewDashboardEquipmentsSelected.setText("Não tem equipamentos seleccionados");
                } else {
                    textViewDashboardEquipmentsSelected.setText("Tem " + numSelectedEquip + " equipamentos seleccionados");
                }

                cardViewDashboardEquipmentsSelected.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getContext(), EquipmentsListActivity.class);
                        ((DashboardActivity)getActivity()).startActivityForResult(intent, REFRESH_DASHBOARD);
                    }
                });

                //EQUIPAMENTOS SELECIONADOS//

                //LEITURAS HOJE//

                CardView cardViewDashboardReadingsToday = (CardView) rootView.findViewById(R.id.cardViewDashboardReadingsToday);
                CircleProgressView circleViewDashboardReadingsToday = (CircleProgressView) rootView.findViewById(R.id.circleViewDashboardReadingsToday);
                TextView textViewDashboardReadingsToday = (TextView) rootView.findViewById(R.id.textViewDashboardReadingsToday);

                Calendar c = Calendar.getInstance();
                System.out.println("Current time => " + c.getTime());

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = df.format(c.getTime());
                System.out.println("Current time formatted => " + formattedDate);

                int numLeiturasHoje = dbHelper.getDailyRecordsAmount(formattedDate);

                circleViewDashboardReadingsToday.setMaxValue(numLeiturasHoje);
                circleViewDashboardReadingsToday.setBlockCount(numLeiturasHoje);
                circleViewDashboardReadingsToday.setText(numLeiturasHoje+"");
                circleViewDashboardReadingsToday.setTextMode(TextMode.TEXT);
                circleViewDashboardReadingsToday.setValue(numLeiturasHoje);

                if(numLeiturasHoje == 0) {
                    textViewDashboardReadingsToday.setText("Ainda não efectuou qualquer leitura hoje");
                } else {
                    textViewDashboardReadingsToday.setText("Efectuou " + numLeiturasHoje + " leituras hoje");
                }

                cardViewDashboardReadingsToday.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((DashboardActivity)getActivity()).changeTab(2);
                    }
                });

                //LEITURAS HOJE//

                break;
            case 1:
                rootView = inflater.inflate(R.layout.fragment_questions_tab, container, false);
                break;
            case 2:
                rootView = inflater.inflate(R.layout.fragment_equipments_tab, container, false);
                break;
            default:
                rootView = inflater.inflate(R.layout.fragment_dashboard_tab, container, false);
        }

        return rootView;
    }

}

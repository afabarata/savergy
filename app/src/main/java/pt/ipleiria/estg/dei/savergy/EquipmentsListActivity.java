package pt.ipleiria.estg.dei.savergy;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.LinkedList;

import pt.ipleiria.estg.dei.savergy.adapters.EquipmentsListAdapter;
import pt.ipleiria.estg.dei.savergy.database.DatabaseHelper;
import pt.ipleiria.estg.dei.savergy.helpers.NetworkUtility;
import pt.ipleiria.estg.dei.savergy.model.Equipment;
import pt.ipleiria.estg.dei.savergy.model.Savergy;
import pt.ipleiria.estg.dei.savergy.server.SetAnswersTask;
import pt.ipleiria.estg.dei.savergy.server.SetEquipamentsTask;

/**
 * Created by Trabalho on 22/05/2017.
 */

public class EquipmentsListActivity extends AppCompatActivity {

    private ActionMenuView defaultMenu;
    private ActionMenuView defaultMenuBack;

    private DatabaseHelper dbHelper;
    private ListView listViewEquipmentsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipament_list);

        Toolbar t = (Toolbar) findViewById(R.id.tToolbar);
        defaultMenu = (ActionMenuView) t.findViewById(R.id.defaultMenu);
        defaultMenuBack = (ActionMenuView) t.findViewById(R.id.defaultMenuBack);
        defaultMenu.setOnMenuItemClickListener(new ActionMenuView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                return onOptionsItemSelected(menuItem);
            }
        });
        defaultMenuBack.setOnMenuItemClickListener(new ActionMenuView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                return onOptionsItemSelected(menuItem);
            }
        });
        setSupportActionBar(t);
        getSupportActionBar().setTitle(null);

        dbHelper = new DatabaseHelper(this);

        initialize();
        updateEquipmentsList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_return, defaultMenuBack.getMenu());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        long id=item.getItemId();

        if (id == R.id.menuReturnActivity) {
            saveEquipments();
            setResult(RESULT_OK);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        saveEquipments();
        setResult(RESULT_OK);
        finish();
    }

    private void initialize() {
        listViewEquipmentsList = (ListView) findViewById(R.id.listViewEquipmentsList);
    }

    private void updateEquipmentsList() {
        EquipmentsListAdapter adapter = new EquipmentsListAdapter(this, Savergy.INSTANCE.getEquipments());
        listViewEquipmentsList.setAdapter(adapter);
    }

    public void saveEquipments(){
        dbHelper.insertEquipments(Savergy.INSTANCE.getEquipments());
        Savergy.INSTANCE.uploadSelectedEquipmentsList(this);

        if(NetworkUtility.hasConnectivity(EquipmentsListActivity.this)) {
            SetEquipamentsTask setEquipamentsTask = new SetEquipamentsTask();
            setEquipamentsTask.execute(Savergy.INSTANCE.getAPI_TOKEN(),"all");
        }
    }

}

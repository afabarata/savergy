package pt.ipleiria.estg.dei.savergy;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Locale;
import java.util.concurrent.ExecutionException;


import pt.ipleiria.estg.dei.savergy.adapters.BatchGridAdapter;
import pt.ipleiria.estg.dei.savergy.adapters.EnergyProfileTabAdapter;
import pt.ipleiria.estg.dei.savergy.adapters.EquipmentGridAdapter;
import pt.ipleiria.estg.dei.savergy.database.DatabaseHelper;
import pt.ipleiria.estg.dei.savergy.helpers.LoginHelper;
import pt.ipleiria.estg.dei.savergy.helpers.NetworkUtility;
import pt.ipleiria.estg.dei.savergy.model.Equipment;
import pt.ipleiria.estg.dei.savergy.model.Savergy;
import pt.ipleiria.estg.dei.savergy.server.SetEquipamentsTask;

public class EnergyProfileActivity extends AppCompatActivity {

    private static final int REFRESH_PROFILE_QUESTIONS = 1;
    private static final int REFRESH_EQUIPMENTS = 2;
    public static final String ADDEQUIPAMENT = "ADDEQUIPAMENT";

    private ActionMenuView defaultMenu;
    private ActionMenuView defaultMenuBack;

    private EnergyProfileTabAdapter energyProfileTabAdapter;
    private ViewPager mViewPager;
    private int addEquipment;

    private DatabaseHelper dbHelper;
    private LinkedList<Equipment> equipments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_energy_profile);

        Toolbar t = (Toolbar) findViewById(R.id.tToolbar);
        defaultMenu = (ActionMenuView) t.findViewById(R.id.defaultMenu);
        defaultMenuBack = (ActionMenuView) t.findViewById(R.id.defaultMenuBack);
        defaultMenu.setOnMenuItemClickListener(new ActionMenuView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                return onOptionsItemSelected(menuItem);
            }
        });
        defaultMenuBack.setOnMenuItemClickListener(new ActionMenuView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                return onOptionsItemSelected(menuItem);
            }
        });
        setSupportActionBar(t);
        getSupportActionBar().setTitle(null);

        dbHelper = new DatabaseHelper(this);
        equipments = new LinkedList<>();
        energyProfileTabAdapter = new EnergyProfileTabAdapter(getSupportFragmentManager(), this);

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(energyProfileTabAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {}
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            public void onPageSelected(int position) {
                switch (position) {
                    case 0:

                        break;
                    case 1:

                        break;
                    case 2:
                        populateEquipments();
                        break;
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REFRESH_PROFILE_QUESTIONS:
                    if(mViewPager.getCurrentItem() == 1) {
                        updateProfileQuestionsList();
                    }
                    break;
                case REFRESH_EQUIPMENTS:
                    if(mViewPager.getCurrentItem() == 2) {
                        updateEquipmentsList();
                    }
                    break;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_profile, defaultMenu.getMenu());
        inflater.inflate(R.menu.menu_return, defaultMenuBack.getMenu());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        long id=item.getItemId();

        if (id == R.id.menuReturnActivity) {
            saveEquipments();

            Intent intent = new Intent(EnergyProfileActivity.this, DashboardActivity.class);
            startActivity(intent);
            return true;
        }
        if(id == R.id.menuAddEquipment) {
            Intent intent = new Intent(EnergyProfileActivity.this, EquipmentsListActivity.class);
            startActivityForResult(intent, REFRESH_EQUIPMENTS);
            return true;
        }
        if (id == R.id.menuRemoveAccount) {

            LayoutInflater li = LayoutInflater.from(EnergyProfileActivity.this);
            View promptsView = li.inflate(R.layout.dialog_remove_account_confirm, null);

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(EnergyProfileActivity.this);

            alertDialogBuilder.setView(promptsView);

            final TextView dialogIntro = (TextView) promptsView.findViewById(R.id.textViewEqDialogQuestion);
            dialogIntro.setText(R.string.profile_remove_user);

            alertDialogBuilder
                    .setCancelable(false)
                    .setPositiveButton("Confirmar",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    LoginHelper loginHelper = new LoginHelper(EnergyProfileActivity.this);
                                    loginHelper.removeAccout();
                                    finish();
                                    moveTaskToBack(true);
                                }
                            })
                    .setNegativeButton("Cancelar",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        saveEquipments();

        Intent intent = new Intent(EnergyProfileActivity.this, DashboardActivity.class);
        startActivity(intent);
    }

    public void populateEquipments() {

        equipments = Savergy.INSTANCE.getSelectedEquipments();

        if(!equipments.isEmpty()) {

            EquipmentGridAdapter adapter = new EquipmentGridAdapter(this, equipments);
            GridView gView = (GridView) findViewById(R.id.gridViewEquipments);
            gView.setAdapter(adapter);

            Configuration config = getResources().getConfiguration();
            if (config.smallestScreenWidthDp >= 330)
            {
                if (config.smallestScreenWidthDp >= 420) {
                    gView.setNumColumns(3);
                }
                else {
                    gView.setNumColumns(2);
                }
            }
            else
            {
                gView.setNumColumns(1);
            }

            gView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    final Equipment equipmentAux = equipments.get(position);

                    LayoutInflater li = LayoutInflater.from(EnergyProfileActivity.this);
                    View promptsView = li.inflate(R.layout.dialog_equipment_config, null);

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(EnergyProfileActivity.this);

                    alertDialogBuilder.setView(promptsView);

                    final TextView dialogEqName = (TextView) promptsView.findViewById(R.id.textViewEqDialogEqName);
                    dialogEqName.setText(equipmentAux.getName());

                    final TextView dialogIntro = (TextView) promptsView.findViewById(R.id.textViewEqDialogIntro);
                    dialogIntro.setText("Caso pretenda configurar os dados do equipamento");

                    final EditText dialogDefaultUseTime = (EditText) promptsView.findViewById(R.id.editTextDefaultUseTime);
                    dialogDefaultUseTime.setText(equipmentAux.getDefaultUseTime()+"");
                    dialogDefaultUseTime.setEnabled(false);

                    final EditText dialogDefaultPower = (EditText) promptsView.findViewById(R.id.editTextDefaultPower);
                    dialogDefaultPower.setText(equipmentAux.getDefaultPower()+"");
                    dialogDefaultPower.setEnabled(false);

                    final EditText dialogConfiguredUseTime = (EditText) promptsView.findViewById(R.id.editTextConfiguredUseTime);
                    dialogConfiguredUseTime.setText(equipmentAux.getConfiguredUseTime()+"");
                    if(equipmentAux.getConfiguredUseTimeUnits().equals("Horas")) {
                        dialogConfiguredUseTime.setText((equipmentAux.getConfiguredUseTime()/60)+"");
                    }

                    final Spinner spinnerConfiguredUseTimeUnits = (Spinner) promptsView.findViewById(R.id.spinnerConfiguredUseTimeUnits);
                    ArrayAdapter<String> array = new ArrayAdapter<String>(EnergyProfileActivity.this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.readingsUnits));
                    int spinner_position = array.getPosition(equipmentAux.getConfiguredUseTimeUnits());
                    spinnerConfiguredUseTimeUnits.setSelection(spinner_position);

                    final EditText dialogConfiguredPower = (EditText) promptsView.findViewById(R.id.editTextConfiguredPower);
                    dialogConfiguredPower.setText(equipmentAux.getConfiguredPower()+"");


                    alertDialogBuilder
                            .setCancelable(false)
                            .setPositiveButton("Guardar",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,int id) {
                                            if(!dialogConfiguredUseTime.getText().toString().isEmpty()) {
                                                equipmentAux.setConfiguredUseTime(Integer.parseInt(dialogConfiguredUseTime.getText().toString()));
                                            } else {
                                                equipmentAux.setConfiguredUseTime(Integer.parseInt("0"));
                                            }
                                            equipmentAux.setConfiguredUseTimeUnits(spinnerConfiguredUseTimeUnits.getSelectedItem().toString());
                                            if(spinnerConfiguredUseTimeUnits.getSelectedItem().toString().equals("Horas")) {
                                                equipmentAux.setConfiguredUseTime(Integer.parseInt(dialogConfiguredUseTime.getText().toString())*60);
                                            }
                                            if(!dialogConfiguredPower.getText().toString().isEmpty()) {
                                                equipmentAux.setConfiguredPower(Integer.parseInt(dialogConfiguredPower.getText().toString()));
                                            }   else {
                                                equipmentAux.setConfiguredPower(Integer.parseInt("0"));
                                            }
                                            saveEquipments();

                                            if(NetworkUtility.hasConnectivity(EnergyProfileActivity.this)) {
                                                SetEquipamentsTask setEquipamentsTask = new SetEquipamentsTask();
                                                setEquipamentsTask.execute(Savergy.INSTANCE.getAPI_TOKEN(),"selected");
                                            }

                                            Toast.makeText(EnergyProfileActivity.this,"Os dados foram guardados com sucesso",Toast.LENGTH_SHORT).show();
                                        }
                                    })
                            .setNegativeButton("Cancelar",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
            });

        }
    }

    public void onProfileQuestionsClick(View view) {
        Intent intent = new Intent(getApplicationContext(), QuestionsActivity.class);
        intent.putExtra(QuestionsActivity.BATCHSERVERID, 0);
        intent.putExtra(QuestionsActivity.BATCHNAME, Savergy.INSTANCE.getProfileQuestions().getBatch());
        startActivityForResult(intent, REFRESH_PROFILE_QUESTIONS);
    }

    private void updateProfileQuestionsList() {
        TextView textViewProfileQuestionsScore = (TextView) findViewById(R.id.textViewProfileQuestionsScore);
        textViewProfileQuestionsScore.setText(Savergy.INSTANCE.getProfileQuestions().getScore() + " Pontos");

        RoundCornerProgressBar progress_questions = (RoundCornerProgressBar) findViewById(R.id.progress_profileQuestions);
        progress_questions.setProgressColor(Color.parseColor("#F7A42B"));
        progress_questions.setProgressBackgroundColor(Color.parseColor("#808080"));
        progress_questions.setMax(Savergy.INSTANCE.getProfileQuestions().getNquestions());
        progress_questions.setProgress(Savergy.INSTANCE.getProfileQuestions().getNanswered());
    }

    private void updateEquipmentsList() {
        if(!Savergy.INSTANCE.getSelectedEquipments().isEmpty()) {
            populateEquipments();
        } else {
            GridView gView = (GridView) findViewById(R.id.gridViewEquipments);
            gView.setAdapter(null);
        }
    }

    public void saveEquipments(){
        if(!equipments.isEmpty()){
            dbHelper.insertEquipments(equipments);
            Savergy.INSTANCE.uploadSelectedEquipmentsList(this);
        }
    }

    public void onClickSetProfile(View view) {

            Spinner spinner_ct = (Spinner) findViewById(R.id.spinnerCountry);
            Spinner spinner_d = (Spinner) findViewById(R.id.spinnerDistrict);
            Spinner spinner_c = (Spinner) findViewById(R.id.spinnerCounty);
            EditText editTextBirthDate = (EditText) findViewById(R.id.editTextBirthDate);
            EditText household = (EditText) findViewById(R.id.editTextHousehold);

        if((!spinner_ct.getSelectedItem().toString().equals("") && !spinner_ct.getSelectedItem().toString().isEmpty())
                && !spinner_d.getSelectedItem().toString().equals("") && !spinner_d.getSelectedItem().toString().isEmpty()
                && !spinner_c.getSelectedItem().toString().equals("") && !spinner_c.getSelectedItem().toString().isEmpty()
                && !editTextBirthDate.getText().toString().equals("") && !editTextBirthDate.getText().toString().isEmpty()
                && !household.getText().toString().equals("") && !household.getText().toString().isEmpty()) {

            Savergy.INSTANCE.getCurrentUser().setCountry(spinner_ct.getSelectedItem().toString());
            Savergy.INSTANCE.getCurrentUser().setDistrict(spinner_d.getSelectedItem().toString());
            Savergy.INSTANCE.getCurrentUser().setCounty(spinner_c.getSelectedItem().toString());
            Savergy.INSTANCE.getCurrentUser().setBirthdate(editTextBirthDate.getText().toString());
            Savergy.INSTANCE.getCurrentUser().setHousehold(Integer.parseInt(household.getText().toString()));

            Savergy.INSTANCE.getCurrentUser().toString();


            if(NetworkUtility.hasConnectivity(this)) {
                LoginHelper loginHelper = new LoginHelper(this);
                try {
                    loginHelper.setProfileData();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } else {
            Toast.makeText(this,"Tem de preencher todos os dados antes de gravar",Toast.LENGTH_SHORT).show();
        }


    }
}

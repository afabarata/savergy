package pt.ipleiria.estg.dei.savergy.server;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import pt.ipleiria.estg.dei.savergy.model.Answer;
import pt.ipleiria.estg.dei.savergy.model.Equipment;
import pt.ipleiria.estg.dei.savergy.model.Question;
import pt.ipleiria.estg.dei.savergy.model.Savergy;

/**
 * Created by Trabalho on 27/06/2017.
 */

public class SetEquipamentsTask extends AsyncTask<String, Void, String> {

    private final static String clientID = "f9alvjJieqOj1bkVZR7orxh8I8yJub7MYNdR4KPmVwM1LChL17Q8qlzkvTwn";
    private static Map<Character, Character> MAP_NORM;

    private Exception exception;

    protected void onPreExecute() {

    }

    protected String doInBackground(String... params) {

        StringBuilder sb = new StringBuilder();
        BufferedReader br;

        try {
            URL url = new URL(Savergy.INSTANCE.getAPI_URL() + "/api/appliances/setMyAppliances");
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setDoOutput(true);

            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Accept", "application/json");
            httpURLConnection.setRequestProperty("Content-Type", "application/json");
            httpURLConnection.setRequestProperty("Authorization", "Bearer "+params[0]);
            httpURLConnection.connect();

            LinkedList<Equipment> equipments;

            if(params[1].equals("all")) {
                equipments = Savergy.INSTANCE.getEquipments();
            } else {
                equipments = Savergy.INSTANCE.getSelectedEquipments();
            }

            JSONObject jsonObject = new JSONObject();
            JSONArray arr = new JSONArray();

            for (Equipment e: equipments) {

                JSONObject arr_root = new JSONObject();
                arr_root.put("id",e.getServerid());
                arr_root.put("name",removeAccents(e.getName()));
                arr_root.put("icon",e.getIcon());
                arr_root.put("description",removeAccents(e.getDescription()));
                arr_root.put("isSelected",e.getIsSelected());
                arr_root.put("defaultUseTime",e.getDefaultUseTime());
                arr_root.put("defaultPower",e.getDefaultPower());
                arr_root.put("configuredUseTime",e.getConfiguredUseTime());
                arr_root.put("configuredPower",e.getConfiguredPower());
                arr.put((Object)arr_root);

            }
            jsonObject.put("user",Savergy.INSTANCE.getCurrentUser().getDeviceId());
            jsonObject.put("equipments",(Object)arr);

            String object = jsonObject.toString().replace("\\","");

            Log.d("jsonObject: ", object);

            DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
            wr.writeBytes(object);
            wr.flush();
            wr.close();

            if (200 <= httpURLConnection.getResponseCode() && httpURLConnection.getResponseCode() <= 299) {
                br = new BufferedReader(new InputStreamReader((httpURLConnection.getInputStream())));
            } else {
                br = new BufferedReader(new InputStreamReader((httpURLConnection.getErrorStream())));
            }

            String output;
            while ((output = br.readLine()) != null) {
                sb.append(output);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }

    protected void onPostExecute(String response) {

        if(response == null) {
            response = "THERE WAS AN ERROR";
        } else {
            try {
                JSONObject obj = new JSONObject(response);
                if(obj.getInt("status") == 200) {
                    Log.i("FEZ SET AOS EQUIP: ", obj.getInt("status")+"");
                } else {
                    Log.i("N FEZ SET AOS EQUIP ", obj.getInt("status")+"");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Log.i("INFO", response);
    }

    public static String removeAccents(String value)
    {
        if (MAP_NORM == null || MAP_NORM.size() == 0)
        {
            MAP_NORM = new HashMap<Character, Character>();
            MAP_NORM.put('À', 'A');
            MAP_NORM.put('Á', 'A');
            MAP_NORM.put('Â', 'A');
            MAP_NORM.put('Ã', 'A');
            MAP_NORM.put('Ä', 'A');
            MAP_NORM.put('È', 'E');
            MAP_NORM.put('É', 'E');
            MAP_NORM.put('Ê', 'E');
            MAP_NORM.put('Ë', 'E');
            MAP_NORM.put('Í', 'I');
            MAP_NORM.put('Ì', 'I');
            MAP_NORM.put('Î', 'I');
            MAP_NORM.put('Ï', 'I');
            MAP_NORM.put('Ù', 'U');
            MAP_NORM.put('Ú', 'U');
            MAP_NORM.put('Û', 'U');
            MAP_NORM.put('Ü', 'U');
            MAP_NORM.put('Ò', 'O');
            MAP_NORM.put('Ó', 'O');
            MAP_NORM.put('Ô', 'O');
            MAP_NORM.put('Õ', 'O');
            MAP_NORM.put('Ö', 'O');
            MAP_NORM.put('Ñ', 'N');
            MAP_NORM.put('Ç', 'C');
            MAP_NORM.put('ª', 'A');
            MAP_NORM.put('º', 'O');
            MAP_NORM.put('§', 'S');
            MAP_NORM.put('³', '3');
            MAP_NORM.put('²', '2');
            MAP_NORM.put('¹', '1');
            MAP_NORM.put('à', 'a');
            MAP_NORM.put('á', 'a');
            MAP_NORM.put('â', 'a');
            MAP_NORM.put('ã', 'a');
            MAP_NORM.put('ä', 'a');
            MAP_NORM.put('è', 'e');
            MAP_NORM.put('é', 'e');
            MAP_NORM.put('ê', 'e');
            MAP_NORM.put('ë', 'e');
            MAP_NORM.put('í', 'i');
            MAP_NORM.put('ì', 'i');
            MAP_NORM.put('î', 'i');
            MAP_NORM.put('ï', 'i');
            MAP_NORM.put('ù', 'u');
            MAP_NORM.put('ú', 'u');
            MAP_NORM.put('û', 'u');
            MAP_NORM.put('ü', 'u');
            MAP_NORM.put('ò', 'o');
            MAP_NORM.put('ó', 'o');
            MAP_NORM.put('ô', 'o');
            MAP_NORM.put('õ', 'o');
            MAP_NORM.put('ö', 'o');
            MAP_NORM.put('ñ', 'n');
            MAP_NORM.put('ç', 'c');
        }

        if (value == null) {
            return "";
        }

        StringBuilder sb = new StringBuilder(value);

        for(int i = 0; i < value.length(); i++) {
            Character c = MAP_NORM.get(sb.charAt(i));
            if(c != null) {
                sb.setCharAt(i, c.charValue());
            }
        }

        return sb.toString();

    }
}

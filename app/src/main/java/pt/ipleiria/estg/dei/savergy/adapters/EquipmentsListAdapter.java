package pt.ipleiria.estg.dei.savergy.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.Iterator;

import pt.ipleiria.estg.dei.savergy.R;
import pt.ipleiria.estg.dei.savergy.helpers.NetworkUtility;
import pt.ipleiria.estg.dei.savergy.model.Equipment;
import pt.ipleiria.estg.dei.savergy.model.Savergy;

/**
 * Created by Trabalho on 22/05/2017.
 */

public class EquipmentsListAdapter extends ArrayAdapter<Equipment> {

    private static final class ViewHolder {
        public View view;

        public ImageView imageViewEquipment;
        public TextView textViewEquipment;
        public CheckBox checkBoxEquipment;

        public ViewHolder(View view) {
            this.view = view;

            imageViewEquipment = (ImageView) view.findViewById(R.id.imageViewEquipment);
            textViewEquipment = (TextView) view.findViewById(R.id.textViewEquipment);
            checkBoxEquipment = (CheckBox) view.findViewById(R.id.checkBoxEquipment);
        }

        public void update(Equipment equipment) {
            if(equipment.getIsSelected() == 1) {
                checkBoxEquipment.setChecked(true);
            } else {
                checkBoxEquipment.setChecked(false);
            }

            if(NetworkUtility.hasConnectivity(view.getContext())) {
                Picasso.with(view.getContext()).load(equipment.getIconURL()).into(imageViewEquipment);
            }else {
                Drawable d = view.getResources().getDrawable(R.drawable.no_photo);
                imageViewEquipment.setImageDrawable(d);
            }
            textViewEquipment.setText(equipment.getName());
        }
    }

    public EquipmentsListAdapter(Context context, Iterable<Equipment> objects) {
        this(context, objects.iterator());
    }

    public EquipmentsListAdapter(Context context,  Iterator<Equipment> objects) {
        super(context, R.layout.equipment_list_item, R.id.textViewEquipment);
        while (objects.hasNext())
            add(objects.next());
    }

    @NonNull
    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        final Equipment equipment = getItem(position);
        CheckBox checkBoxEquipment = (CheckBox) view.findViewById(R.id.checkBoxEquipment);

        EquipmentsListAdapter.ViewHolder holder = (EquipmentsListAdapter.ViewHolder)view.getTag();

        if (holder==null) { // First time I've seen this View: let's tag it!
            holder = new EquipmentsListAdapter.ViewHolder(view);

            view.setTag(holder);
        } else {
            checkBoxEquipment.setOnCheckedChangeListener(null);
        }

        checkBoxEquipment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(isChecked){
                    Savergy.INSTANCE.getEquipments().get(position).setIsSelected(1);
                } else {
                    Savergy.INSTANCE.getEquipments().get(position).setIsSelected(0);
                }
            }
        });

        holder.update(equipment);
        return view;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getServerid();
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

}
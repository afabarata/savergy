package pt.ipleiria.estg.dei.savergy.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Iterator;

import pt.ipleiria.estg.dei.savergy.R;
import pt.ipleiria.estg.dei.savergy.model.Batch;
import pt.ipleiria.estg.dei.savergy.model.Savergy;

/**
 * Created by Trabalho on 10/06/2017.
 */

public class AvatarGridAdapter extends ArrayAdapter<Integer> {

    private static final class ViewHolder {
        public View view;
        public TextView textViewAvatar;
        public ImageView imageViewAvatar;

        public ViewHolder(View view) {
            this.view = view;
            textViewAvatar = (TextView) view.findViewById(R.id.textViewAvatar);
            imageViewAvatar = (ImageView) view.findViewById(R.id.imageViewAvatar);
        }

        public void update(Integer id) {
            if(Savergy.INSTANCE.getCurrentUser().getAvatar() != 0) {
                int avatar = 0;
                switch (Savergy.INSTANCE.getCurrentUser().getAvatar()) {
                    case 1:
                        avatar = R.drawable.avatar_01;
                        break;
                    case 2:
                        avatar = R.drawable.avatar_02;
                        break;
                    case 3:
                        avatar = R.drawable.avatar_03;
                        break;
                    case 4:
                        avatar = R.drawable.avatar_04;
                        break;
                    case 5:
                        avatar = R.drawable.avatar_05;
                        break;
                    case 6:
                        avatar = R.drawable.avatar_06;
                        break;
                    case 7:
                        avatar = R.drawable.avatar_07;
                        break;
                    case 8:
                        avatar = R.drawable.avatar_08;
                        break;
                    case 9:
                        avatar = R.drawable.avatar_09;
                        break;
                    case 10:
                        avatar = R.drawable.avatar_010;
                        break;
                    case 11:
                        avatar = R.drawable.avatar_011;
                        break;
                    case 12:
                        avatar = R.drawable.avatar_012;
                        break;
                }
                if(avatar == id) {
                    view.findViewById(R.id.gridViewAvatarItem).setBackgroundColor(Color.parseColor("#F7A42B"));
                }
            }

            Drawable d = view.getResources().getDrawable(id);
            imageViewAvatar.setImageDrawable(d);
        }
    }

    public AvatarGridAdapter(Context context, Iterable<Integer> objects) {
        this(context, objects.iterator());
    }

    public AvatarGridAdapter(Context context, Iterator<Integer> objects) {
        super(context, R.layout.avatar_grid_item, R.id.textViewAvatar);
        while (objects.hasNext()) {
            add(objects.next());
        }

    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        Integer avatar = getItem(position);

        AvatarGridAdapter.ViewHolder holder = (AvatarGridAdapter.ViewHolder)view.getTag();

        if (holder==null) {
            holder = new AvatarGridAdapter.ViewHolder(view);
            view.setTag(holder);
        }

        holder.update(avatar);

        return view;
    }


    @Override
    public long getItemId(int position) {
        return position+1;
    }

}

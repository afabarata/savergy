package pt.ipleiria.estg.dei.savergy.model;

/**
 * Created by Trabalho on 19/04/2017.
 */

public class Advice {

    private int id;
    private int serverid;
    private String advice;
    private int isOld;

    public Advice(int serverid, String advice) {
        this.serverid = serverid;
        this.advice = advice;
        this.isOld = 0;
    }

    public int getServerid() {
        return serverid;
    }

    public void setServerid(int serverid) {
        this.serverid = serverid;
    }

    public String getAdvice() {
        return advice;
    }

    public void setAdvice(String advice) {
        this.advice = advice;
    }

    public int getIsOld() {
        return isOld;
    }

    public void setIsOld(int isOld) {
        this.isOld = isOld;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("\n-- Advice --\n"
                + "ID: " + serverid + "\n"
                + "Advice: " + advice + "\n"
                + "isOld: " + isOld + "\n"
                + "\n");

        return sb.toString();
    }

}

package pt.ipleiria.estg.dei.savergy.fragments;

import android.app.DatePickerDialog;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import pt.ipleiria.estg.dei.savergy.EnergyProfileActivity;
import pt.ipleiria.estg.dei.savergy.R;
import pt.ipleiria.estg.dei.savergy.adapters.AvatarGridAdapter;
import pt.ipleiria.estg.dei.savergy.helpers.AddressHelper;
import pt.ipleiria.estg.dei.savergy.model.Savergy;

/**
 * Created by Bruno on 23/03/2017.
 */

public class EnergyProfileTabFragment extends Fragment {

    View rootView;
    ArrayAdapter<String> adp2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Bundle args = getArguments();

        switch (args.getInt("fragment")) {
            case 0:
                rootView = inflater.inflate(R.layout.fragment_profile_general_tab, container, false);

                /* AVATAR */

                final GridView avatarGrid = (GridView) rootView.findViewById(R.id.gridViewAvatar);

                Integer[] avatares = new Integer[]{
                        R.drawable.avatar_01,
                        R.drawable.avatar_02,
                        R.drawable.avatar_03,
                        R.drawable.avatar_04,
                        R.drawable.avatar_05,
                        R.drawable.avatar_06,
                        R.drawable.avatar_07,
                        R.drawable.avatar_08,
                        R.drawable.avatar_09,
                        R.drawable.avatar_010,
                        R.drawable.avatar_011,
                        R.drawable.avatar_012,
                };

                List<Integer> avataresList = new ArrayList<Integer>(Arrays.asList(avatares));
                AvatarGridAdapter adapter = new AvatarGridAdapter(getContext(), avataresList);
                avatarGrid.setAdapter(adapter);

                Configuration config = getResources().getConfiguration();
                if (config.smallestScreenWidthDp >= 330)
                {
                    if (config.smallestScreenWidthDp >= 420) {
                        avatarGrid.setNumColumns(8);
                        setDynamicHeight(avatarGrid, 8);
                    }
                    else {
                        avatarGrid.setNumColumns(6);
                        setDynamicHeight(avatarGrid, 6);
                    }
                }
                else
                {
                    avatarGrid.setNumColumns(3);
                    setDynamicHeight(avatarGrid, 3);
                }

                avatarGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Savergy.INSTANCE.getCurrentUser().setAvatar(Integer.parseInt(id+""));

                        for(int i = 0; i < parent.getChildCount(); i++) {
                            View iterate = parent.getChildAt(i);
                            iterate.findViewById(R.id.gridViewAvatarItem).setBackgroundColor(Color.parseColor("#ffffff"));
                        }

                        View c = parent.getChildAt(position);
                        c.findViewById(R.id.gridViewAvatarItem).setBackgroundColor(Color.parseColor("#F7A42B"));
                    }
                });

                /* AVATAR */

                /* SPINNERS */

                final AddressHelper addressHelper = new AddressHelper();
                final Spinner spinner_ct = (Spinner) rootView.findViewById(R.id.spinnerCountry);
                final Spinner spinner_d = (Spinner) rootView.findViewById(R.id.spinnerDistrict);
                final Spinner spinner_c = (Spinner) rootView.findViewById(R.id.spinnerCounty);

                String[] districts = new String[addressHelper.getDistritos().size()];
                for(int i= 0; i < addressHelper.getDistritos().size(); i++) {
                    districts[i] = addressHelper.getDistritos().get(i).getName();
                }

                ArrayAdapter<String> adp1 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, districts);
                adp1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner_d.setAdapter(adp1);

                spinner_d.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        adp2 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, addressHelper.getDistritos().get(i).getConcelhos());
                        adp2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner_c.setAdapter(adp2);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                        adp2 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, new String[0]);
                        adp2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner_c.setAdapter(adp2);
                    }
                });

                if(Savergy.INSTANCE.getCurrentUser().getCountry().isEmpty() && Savergy.INSTANCE.getCurrentUser().getCountry() != null && !Savergy.INSTANCE.getCurrentUser().getCountry().equals("null")) {
                    ArrayAdapter<String> array = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.countries));
                    int position = array.getPosition(Savergy.INSTANCE.getCurrentUser().getCountry());
                    spinner_ct.setSelection(position);
                }

                if(Savergy.INSTANCE.getCurrentUser().getDistrict().isEmpty() && Savergy.INSTANCE.getCurrentUser().getDistrict() != null && !Savergy.INSTANCE.getCurrentUser().getDistrict().equals("null")) {
                    final int position = adp1.getPosition(Savergy.INSTANCE.getCurrentUser().getDistrict());
                    spinner_d.setSelection(position);
                    if(Savergy.INSTANCE.getCurrentUser().getCounty().isEmpty() && Savergy.INSTANCE.getCurrentUser().getCounty() != null && !Savergy.INSTANCE.getCurrentUser().getCounty().equals("null")) {
                        new android.os.Handler().postDelayed(
                            new Runnable() {
                                public void run() {
                                    adp2 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, addressHelper.getDistritos().get(position).getConcelhos());
                                    int position2 = adp2.getPosition(Savergy.INSTANCE.getCurrentUser().getCounty());
                                    spinner_c.setAdapter(adp2);
                                    spinner_c.setSelection(position2);
                                }
                            },
                        500);
                    }
                }


                /* SPINNERS */

                /* CALENDAR */

                final Calendar myCalendar = Calendar.getInstance();

                final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        EditText editTextBirthDate = (EditText) rootView.findViewById(R.id.editTextBirthDate);

                        String myFormat = "yyyy/MM/dd"; //In which you need put here
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                        editTextBirthDate.setText(sdf.format(myCalendar.getTime()));

                    }

                };

                EditText editTextBirthDate = (EditText) rootView.findViewById(R.id.editTextBirthDate);

                editTextBirthDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        new DatePickerDialog(getContext(), date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                    }
                });

                if(Savergy.INSTANCE.getCurrentUser().getBirthdate().isEmpty() && Savergy.INSTANCE.getCurrentUser().getBirthdate() != null && !Savergy.INSTANCE.getCurrentUser().getBirthdate().equals("null")) {
                    editTextBirthDate.setText(Savergy.INSTANCE.getCurrentUser().getBirthdate());
                }

                /* CALENDAR */

                /* HOUSEHOLD */

                EditText editTextHousehold = (EditText) rootView.findViewById(R.id.editTextHousehold);

                if(Savergy.INSTANCE.getCurrentUser().getHousehold() != 0) {
                    editTextHousehold.setText(String.valueOf(Savergy.INSTANCE.getCurrentUser().getHousehold()));
                }

                /* HOUSEHOLD */

                /* GENDER */

                RadioGroup radioGrpGender = (RadioGroup) rootView.findViewById(R.id.radioGrpGender);

                radioGrpGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                      @Override
                      public void onCheckedChanged(RadioGroup group, int checkedId)
                      {
                          RadioButton radioButton = (RadioButton) rootView.findViewById(checkedId);
                          Savergy.INSTANCE.getCurrentUser().setGender(radioButton.getText().toString());
                      }
                  }
                );

                if(Savergy.INSTANCE.getCurrentUser().getGender().isEmpty() && Savergy.INSTANCE.getCurrentUser().getGender() != null && !Savergy.INSTANCE.getCurrentUser().getGender().equals("null")) {
                    if(Savergy.INSTANCE.getCurrentUser().getGender().equals("Masculino")) {
                        RadioButton radioButton = (RadioButton) rootView.findViewById(R.id.radioM);
                        radioButton.setChecked(true);
                    } else {
                        RadioButton radioButton = (RadioButton) rootView.findViewById(R.id.radioF);
                        radioButton.setChecked(true);
                    }
                }

                /* GENDER */

                break;
            case 1:
                rootView = inflater.inflate(R.layout.fragment_profile_questions_tab, container, false);

                TextView textViewProfileQuestionsScore = (TextView) rootView.findViewById(R.id.textViewProfileQuestionsScore);
                textViewProfileQuestionsScore.setText(Savergy.INSTANCE.getProfileQuestions().getScore() + " Pontos");

                RoundCornerProgressBar progress_questions = (RoundCornerProgressBar) rootView.findViewById(R.id.progress_profileQuestions);
                progress_questions.setProgressColor(Color.parseColor("#F7A42B"));
                progress_questions.setProgressBackgroundColor(Color.parseColor("#808080"));
                progress_questions.setMax(Savergy.INSTANCE.getProfileQuestions().getNquestions());
                progress_questions.setProgress(Savergy.INSTANCE.getProfileQuestions().getNanswered());

                break;
            case 2:
                rootView = inflater.inflate(R.layout.fragment_profile_equipments_tab, container, false);
                break;
            default:
                rootView = inflater.inflate(R.layout.fragment_profile_questions_tab, container, false);
        }

        return rootView;
    }

    private void setDynamicHeight(GridView gridView, int numItemsForRow) {
        ListAdapter gridViewAdapter = gridView.getAdapter();
        if (gridViewAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int items = gridViewAdapter.getCount();
        int rows = 0;

        View listItem = gridViewAdapter.getView(0, null, gridView);
        listItem.measure(0, 0);
        totalHeight = listItem.getMeasuredHeight();

        float x = 1;
        if( items > 12 ){
            x = items/12;
            rows = (int) (x + 1);
            totalHeight *= rows;
        }

        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = totalHeight+40;
        gridView.setLayoutParams(params);
    }
}
package pt.ipleiria.estg.dei.savergy.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import pt.ipleiria.estg.dei.savergy.R;
import pt.ipleiria.estg.dei.savergy.fragments.EnergyProfileTabFragment;
import pt.ipleiria.estg.dei.savergy.fragments.RankingTabFragment;

/**
 * Created by Trabalho on 11/06/2017.
 */

public class RankingTabAdapter extends FragmentPagerAdapter {

    Context mContext;

    public RankingTabAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = new RankingTabFragment();

        Bundle args = new Bundle();
        args.putInt("fragment", position);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.ranking_tab_local);
            case 1:
                return mContext.getString(R.string.ranking_tab_country);
        }

        return null;
    }

}

package pt.ipleiria.estg.dei.savergy;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

import pt.ipleiria.estg.dei.savergy.adapters.BatchGridAdapter;
import pt.ipleiria.estg.dei.savergy.adapters.DashboardTabAdapter;
import pt.ipleiria.estg.dei.savergy.adapters.EquipmentGridAdapter;
import pt.ipleiria.estg.dei.savergy.database.DatabaseHelper;
import pt.ipleiria.estg.dei.savergy.helpers.ConnectionsHelper;
import pt.ipleiria.estg.dei.savergy.helpers.NetworkUtility;
import pt.ipleiria.estg.dei.savergy.model.Batch;
import pt.ipleiria.estg.dei.savergy.model.Equipment;
import pt.ipleiria.estg.dei.savergy.model.Savergy;
import pt.ipleiria.estg.dei.savergy.server.GetRankingLocalTask;
import pt.ipleiria.estg.dei.savergy.server.SetEquipamentsRecordsTask;
import pt.ipleiria.estg.dei.savergy.server.SetEquipamentsTask;

/**
 * Created by Trabalho on 17/03/2017.
 */

public class DashboardActivity extends AppCompatActivity {

    private static final int REFRESH_BATCHES = 1;
    private static final int REFRESH_DASHBOARD = 2;
    private static final int REFRESH_EQUIPMENTS = 3;

    private ActionMenuView defaultMenu;
    private ActionMenuView defaultMenuBack;
    private DashboardTabAdapter dashboardTabAdapter;
    private ViewPager mViewPager;

    private DatabaseHelper dbHelper;
    private ConnectionsHelper connectionsHelper;
    private LinkedList<Batch> batches;
    private LinkedList<Equipment> equipments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        Toolbar t = (Toolbar) findViewById(R.id.tToolbar);
        defaultMenu = (ActionMenuView) t.findViewById(R.id.defaultMenu);
        defaultMenuBack = (ActionMenuView) t.findViewById(R.id.defaultMenuBack);
        defaultMenu.setOnMenuItemClickListener(new ActionMenuView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                return onOptionsItemSelected(menuItem);
            }
        });
        defaultMenuBack.setOnMenuItemClickListener(new ActionMenuView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                return onOptionsItemSelected(menuItem);
            }
        });
        setSupportActionBar(t);
        getSupportActionBar().setTitle(null);

        dbHelper = new DatabaseHelper(this);
        dbHelper.updateUser(Savergy.INSTANCE.getCurrentUser());
        dbHelper.toStringUsers();

        dashboardTabAdapter = new DashboardTabAdapter(getSupportFragmentManager(), this);

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(dashboardTabAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {}
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        refreshDasboardTab();
                        break;
                    case 1:
                        populateCategories();
                        break;
                    case 2:
                        populateEquipments();
                        break;
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REFRESH_BATCHES:
                    updateCategoriesList();
                    break;
                case REFRESH_DASHBOARD:
                    refreshDasboardTab();
                    break;
                case REFRESH_EQUIPMENTS:
                    populateEquipments();
                    break;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_dashboard, defaultMenu.getMenu());
        inflater.inflate(R.menu.menu_return, defaultMenuBack.getMenu());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        long id=item.getItemId();

        if (id == R.id.menuReturnActivity) {
            if(!Savergy.INSTANCE.getCurrentUser().getSocialId().isEmpty() || !Savergy.INSTANCE.getCurrentUser().getEmail().isEmpty()) {
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
            } else {
                Intent intent = new Intent(this, RegisterChoiceActivity.class);
                startActivity(intent);
            }
            return true;
        }
        if (id == R.id.menuProfileActivity) {
            Intent intent = new Intent(DashboardActivity.this, EnergyProfileActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.menuRankingActivity) {
            connectionsHelper = new ConnectionsHelper(this);

            if(NetworkUtility.hasConnectivity(DashboardActivity.this)) {
                try {
                    connectionsHelper.getRankingLocalFromSerer();
                    connectionsHelper.getRankingCountryFromSerer();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            Intent intent = new Intent(DashboardActivity.this, RankingActivity.class);
            startActivity(intent);
            return true;
        }

        if (id == R.id.menuNotificationsActivity) {
            Intent intent = new Intent(DashboardActivity.this, AdvicesListActivity.class);
            startActivity(intent);
            return true;
        }

        if (id == R.id.menuSuggestionsActivity) {
            Intent intent = new Intent(DashboardActivity.this, SuggestionsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void refreshDasboardTab() {
        mViewPager.setAdapter(dashboardTabAdapter);
    }

    public void populateCategories() {
        batches = Savergy.INSTANCE.getBatches();

        BatchGridAdapter adapter = new BatchGridAdapter(this, batches);
        GridView gView = (GridView) findViewById(R.id.gridViewBatch);
        gView.setAdapter(adapter);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 330)
        {
            if (config.smallestScreenWidthDp >= 420) {
                gView.setNumColumns(3);
            }
            else {
                gView.setNumColumns(2);
            }
        }
        else
        {
            gView.setNumColumns(1);
        }

        gView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Batch batchAux = batches.get(position);

                Intent intent = new Intent(DashboardActivity.this, QuestionsActivity.class);
                intent.putExtra(QuestionsActivity.BATCHSERVERID, batchAux.getServerid());
                intent.putExtra(QuestionsActivity.BATCHNAME, batchAux.getBatch());
                startActivityForResult(intent, REFRESH_BATCHES);
            }
        });
    }

    public void populateEquipments() {

        equipments = Savergy.INSTANCE.getSelectedEquipments();

        EquipmentGridAdapter adapter = new EquipmentGridAdapter(this, equipments);
        GridView gView = (GridView) findViewById(R.id.gridViewEquipments);
        gView.setAdapter(adapter);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 330)
        {
            if (config.smallestScreenWidthDp >= 420) {
                gView.setNumColumns(3);
            }
            else {
                gView.setNumColumns(2);
            }
        }
        else
        {
            gView.setNumColumns(1);
        }

        gView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                Log.d("Num Childs:", parent.getChildCount()+"");
                Log.d("EQ POS:", position+"");
                Equipment equipmentAux = equipments.get(position);

                if(view.isEnabled()) {
                    int useTime = 0;
                    int power = 0;

                    if(equipmentAux.getConfiguredUseTime() != 0) {
                        useTime = equipmentAux.getConfiguredUseTime();
                    } else {
                        useTime = equipmentAux.getDefaultUseTime();
                    }

                    if(equipmentAux.getConfiguredPower() != 0) {
                        power = equipmentAux.getConfiguredPower();
                    } else {
                        power = equipmentAux.getDefaultPower();
                    }

                    dbHelper.insertEquipmentRecord(Savergy.INSTANCE.getCurrentUser().getDeviceId(),equipmentAux.getServerid(), useTime, power);
                    //dbHelper.toStringEquipmentRecords();

                    Savergy.INSTANCE.uploadEquipmentsRecordsList(DashboardActivity.this);
                    if(NetworkUtility.hasConnectivity(DashboardActivity.this)) {
                        SetEquipamentsRecordsTask setEquipamentsRecordsTask = new SetEquipamentsRecordsTask();
                        setEquipamentsRecordsTask.execute(Savergy.INSTANCE.getAPI_TOKEN());
                    }

                    Toast.makeText(DashboardActivity.this, R.string.dashboard_readings_send, Toast.LENGTH_SHORT).show();
                    view.findViewById(R.id.gridViewEquipmentItem).setBackgroundColor(Color.parseColor("#F7A42B"));
                    view.setEnabled(false);

                    Timer buttonTimer = new Timer();
                    buttonTimer.schedule(new TimerTask() {

                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    view.setEnabled(true);
                                    view.findViewById(R.id.gridViewEquipmentItem).setBackgroundColor(Color.parseColor("#C6D76B"));
                                }
                            });
                        }
                    }, 5000);

                }

            }
        });

        gView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                final Equipment equipmentAux = equipments.get(position);

                LayoutInflater li = LayoutInflater.from(DashboardActivity.this);
                View promptsView = li.inflate(R.layout.dialog_equipment_config, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DashboardActivity.this);

                alertDialogBuilder.setView(promptsView);

                final TextView dialogEqName = (TextView) promptsView.findViewById(R.id.textViewEqDialogEqName);
                dialogEqName.setText(equipmentAux.getName());

                final TextView dialogIntro = (TextView) promptsView.findViewById(R.id.textViewEqDialogIntro);
                dialogIntro.setText(R.string.dashboard_readings_config_intro);

                final TextView textViewDefaultUseTime = (TextView) promptsView.findViewById(R.id.textViewDefaultUseTime);
                textViewDefaultUseTime.setVisibility(View.GONE);

                final EditText dialogDefaultUseTime = (EditText) promptsView.findViewById(R.id.editTextDefaultUseTime);
                dialogDefaultUseTime.setText(equipmentAux.getDefaultUseTime()+"");
                dialogDefaultUseTime.setVisibility(View.GONE);

                final TextView textViewDefaultPower = (TextView) promptsView.findViewById(R.id.textViewDefaultPower);
                textViewDefaultPower.setVisibility(View.GONE);

                final EditText dialogDefaultPower = (EditText) promptsView.findViewById(R.id.editTextDefaultPower);
                dialogDefaultPower.setText(equipmentAux.getDefaultPower()+"");
                dialogDefaultPower.setVisibility(View.GONE);

                final EditText dialogConfiguredUseTime = (EditText) promptsView.findViewById(R.id.editTextConfiguredUseTime);
                dialogConfiguredUseTime.setText(equipmentAux.getConfiguredUseTime()+"");
                if(equipmentAux.getConfiguredUseTimeUnits().equals("Horas")) {
                    dialogConfiguredUseTime.setText((equipmentAux.getConfiguredUseTime()/60)+"");
                }

                final Spinner spinnerConfiguredUseTimeUnits = (Spinner) promptsView.findViewById(R.id.spinnerConfiguredUseTimeUnits);
                ArrayAdapter<String> array = new ArrayAdapter<String>(DashboardActivity.this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.readingsUnits));
                int spinner_position = array.getPosition(equipmentAux.getConfiguredUseTimeUnits());
                spinnerConfiguredUseTimeUnits.setSelection(spinner_position);

                final EditText dialogConfiguredPower = (EditText) promptsView.findViewById(R.id.editTextConfiguredPower);
                dialogConfiguredPower.setText(equipmentAux.getConfiguredPower()+"");

                alertDialogBuilder
                    .setCancelable(false)
                    .setPositiveButton("Enviar",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {

                                    int useTime = 0;
                                    int power = 0;

                                    if(!dialogConfiguredUseTime.getText().toString().isEmpty()) {
                                        if(!dialogConfiguredUseTime.getText().toString().equals("0")) {
                                            useTime = Integer.parseInt(dialogConfiguredUseTime.getText().toString());
                                            if(spinnerConfiguredUseTimeUnits.getSelectedItem().toString().equals("Horas")) {
                                                useTime = useTime * 60;
                                            }
                                        } else {
                                            useTime = Integer.parseInt(dialogDefaultUseTime.getText().toString());
                                        }

                                    }else {
                                        useTime = Integer.parseInt(dialogDefaultUseTime.getText().toString());
                                    }

                                    if(!dialogConfiguredPower.getText().toString().isEmpty()) {
                                        if(!dialogConfiguredPower.getText().toString().equals("0")) {
                                            power = Integer.parseInt(dialogConfiguredPower.getText().toString());
                                        } else {
                                            power = Integer.parseInt(dialogDefaultPower.getText().toString());
                                        }
                                    } else {
                                        power = Integer.parseInt(dialogDefaultPower.getText().toString());
                                    }

                                    dbHelper.insertEquipmentRecord(Savergy.INSTANCE.getCurrentUser().getDeviceId(),equipmentAux.getServerid(), useTime, power);
                                    dbHelper.toStringEquipmentRecords();

                                    Savergy.INSTANCE.uploadEquipmentsRecordsList(DashboardActivity.this);
                                    if(NetworkUtility.hasConnectivity(DashboardActivity.this)) {
                                        SetEquipamentsRecordsTask setEquipamentsRecordsTask = new SetEquipamentsRecordsTask();
                                        setEquipamentsRecordsTask.execute(Savergy.INSTANCE.getAPI_TOKEN());
                                    }
                                }
                            })
                    .setNegativeButton("Cancelar",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

                return true;
            }
        });

    }

    private void updateCategoriesList() {
        BatchGridAdapter adapter = new BatchGridAdapter(this, Savergy.INSTANCE.getBatches());
        GridView gView = (GridView) findViewById(R.id.gridViewBatch);
        gView.setAdapter(adapter);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 330)
        {
            if (config.smallestScreenWidthDp >= 420) {
                gView.setNumColumns(3);
            }
            else {
                gView.setNumColumns(2);
            }
        }
        else
        {
            gView.setNumColumns(1);
        }
    }

    public void onClickfabAddEquip(View view) {
        Intent intent = new Intent(DashboardActivity.this, EquipmentsListActivity.class);
        startActivityForResult(intent, REFRESH_EQUIPMENTS);
    }

    public void changeTab(int tab) {
        mViewPager.setCurrentItem(tab);
    }

    @Override
    public void onBackPressed() {
        if((!Savergy.INSTANCE.getCurrentUser().getSocialId().equals("")) && (Savergy.INSTANCE.getCurrentUser().getSocialId() != null)) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, RegisterChoiceActivity.class);
            startActivity(intent);
        }
    }

}

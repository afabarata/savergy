package pt.ipleiria.estg.dei.savergy.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Iterator;

import pt.ipleiria.estg.dei.savergy.R;
import pt.ipleiria.estg.dei.savergy.model.Savergy;
import pt.ipleiria.estg.dei.savergy.model.User;

/**
 * Created by Trabalho on 11/06/2017.
 */

public class RankingListAdapter  extends ArrayAdapter<User> {

    private static final class ViewHolder {
        View view;
        public TextView textViewTop10Position;
        public TextView textViewTop10Username;
        public ImageView imageViewTop10Avatar;

        public ViewHolder(View view) {
            this.view = view;
            textViewTop10Position = (TextView) view.findViewById(R.id.textViewTop10Position);
            textViewTop10Username = (TextView) view.findViewById(R.id.textViewTop10Username);
            imageViewTop10Avatar = (ImageView) view.findViewById(R.id.imageViewTop10Avatar);
        }

        public void update(User user) {
            textViewTop10Position.setText(String.valueOf(user.getRank()));

            if(!user.getName().isEmpty()) {
                textViewTop10Username.setText(user.getName() + " - " + user.getNumPoints() + " Pontos");
            } else {
                textViewTop10Username.setText("Convidado" + " - " + user.getNumPoints() + " Pontos");
            }

            int avatar = R.drawable.no_avatar_masculino;

            switch (user.getAvatar()) {
                case 1:
                    avatar = R.drawable.avatar_01;
                    break;
                case 2:
                    avatar = R.drawable.avatar_02;
                    break;
                case 3:
                    avatar = R.drawable.avatar_03;
                    break;
                case 4:
                    avatar = R.drawable.avatar_04;
                    break;
                case 5:
                    avatar = R.drawable.avatar_05;
                    break;
                case 6:
                    avatar = R.drawable.avatar_06;
                    break;
                case 7:
                    avatar = R.drawable.avatar_07;
                    break;
                case 8:
                    avatar = R.drawable.avatar_08;
                    break;
                case 9:
                    avatar = R.drawable.avatar_09;
                    break;
                case 10:
                    avatar = R.drawable.avatar_010;
                    break;
                case 11:
                    avatar = R.drawable.avatar_011;
                    break;
                case 12:
                    avatar = R.drawable.avatar_012;
                    break;
            }

            Drawable d = view.getResources().getDrawable(avatar);
            imageViewTop10Avatar.setImageDrawable(d);

        }
    }

    public RankingListAdapter(Context context, Iterable<User> objects) {
        this(context, objects.iterator());
    }

    public RankingListAdapter(Context context,  Iterator<User> objects) {
        super(context, R.layout.ranking_list_item, R.id.textViewTop10Username);
        while (objects.hasNext())
            add(objects.next());
    }

    @NonNull
    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        final User user = getItem(position);

        RankingListAdapter.ViewHolder holder = (RankingListAdapter.ViewHolder)view.getTag();

        if (holder==null) {
            holder = new RankingListAdapter.ViewHolder(view);

            view.setTag(holder);
        }

        holder.update(user);
        return view;
    }

    @Override
    public long getItemId(int position) {
        return Integer.parseInt(getItem(position).getDeviceId());
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

}

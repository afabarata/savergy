package pt.ipleiria.estg.dei.savergy.model;

import android.content.Context;

import java.util.LinkedList;

import pt.ipleiria.estg.dei.savergy.database.DatabaseHelper;

/**
 * Created by Trabalho on 16/04/2017.
 */

public enum Savergy {
    INSTANCE;

    private final static String API_URL = "http://194.210.216.190";
    //private final static String API_URL = "http://194.210.216.190";
    private String API_TOKEN;

    private DatabaseHelper dbHelper;

    private User currentUser;
    private LinkedList<Batch> batches;
    private Batch profileQuestions;
    private LinkedList<Equipment> equipments;
    private LinkedList<Equipment> selectedEquipments;
    private LinkedList<EquipmentRecord> equipmentRecords;
    private LinkedList<Advice> advices;
    private LinkedList<User> rankingLocalList;
    private LinkedList<User> rankingCountryList;

    private Savergy() {
        this.API_TOKEN = "";

        this.currentUser = null;
        this.batches = null;
        this.equipments = null;
        this.equipmentRecords= null;
        this.advices = null;
        this.rankingLocalList = null;
        this.rankingCountryList = null;
    }

    public String getAPI_URL() {
        return API_URL;
    }

    public String getAPI_TOKEN() {
        return API_TOKEN;
    }

    public void setAPI_TOKEN(String API_TOKEN) {
        this.API_TOKEN = API_TOKEN;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public LinkedList<Batch> getBatches() {
        return batches;
    }

    public LinkedList<Question> getQuestionsByBatchServerId(Long serverid) {
        for (Batch c: batches) {
            if(c.getServerid() == serverid) {
                return c.getQuestions();
            }
        }
        return null;
    }

    public void setBatches(LinkedList<Batch> batches) {
        this.batches = batches;
    }

    public Batch getProfileQuestions() {
        return profileQuestions;
    }

    public LinkedList<Question> getProfileQuestionsQuestions() {
        return profileQuestions.getQuestions();
    }

    public void setProfileQuestions(Batch profileQuestions) {
        this.profileQuestions = profileQuestions;
    }

    public LinkedList<Equipment> getEquipments() {
        return equipments;
    }

    public void setEquipments(LinkedList<Equipment> equipments) {
        this.equipments = equipments;
    }

    public LinkedList<Equipment> getSelectedEquipments() {
        return selectedEquipments;
    }

    public void setSelectedEquipments(LinkedList<Equipment> selectedEquipments) {
        this.selectedEquipments = selectedEquipments;
    }

    public LinkedList<EquipmentRecord> getEquipmentRecords() {
        return equipmentRecords;
    }

    public void setEquipmentRecords(LinkedList<EquipmentRecord> equipmentRecords) {
        this.equipmentRecords = equipmentRecords;
    }

    public LinkedList<Advice> getAdvices() {
        return advices;
    }

    public void setAdvices(LinkedList<Advice> advices) {
        this.advices = advices;
    }

    public LinkedList<User> getRankingLocalList() {
        return rankingLocalList;
    }

    public void setRankingLocalList(LinkedList<User> rankingLocalList) {
        this.rankingLocalList = rankingLocalList;
    }

    public LinkedList<User> getRankingCountryList() {
        return rankingCountryList;
    }

    public void setRankingCountryList(LinkedList<User> rankingCountryList) {
        this.rankingCountryList = rankingCountryList;
    }

    public void saveCurrentUser(Context context) {
        dbHelper = new DatabaseHelper(context);
        dbHelper.insertUser(currentUser);
    }

    public void uploadBatchesList(Context context) {
        dbHelper = new DatabaseHelper(context);
        this.batches = dbHelper.getBatchesList();
    }

    public void uploadProfileQuestions(Context context) {
        dbHelper = new DatabaseHelper(context);
        this.profileQuestions = dbHelper.getProfileQuestions();
    }

    public void uploadEquipmentsList(Context context) {
        dbHelper = new DatabaseHelper(context);
        this.equipments = dbHelper.getEquipamentsList();
    }

    public void uploadSelectedEquipmentsList(Context context) {
        dbHelper = new DatabaseHelper(context);
        this.selectedEquipments = dbHelper.getSelectedEquipamentsList();
    }

    public void uploadEquipmentsRecordsList(Context context) {
        dbHelper = new DatabaseHelper(context);
        this.equipmentRecords = dbHelper.getEquipmentRecords();
    }

    public void uploadAdvicesList(Context context) {
        dbHelper = new DatabaseHelper(context);
        this.advices = dbHelper.getAdvicesList();
    }

}

package pt.ipleiria.estg.dei.savergy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import pt.ipleiria.estg.dei.savergy.adapters.AdvicesListAdapter;
import pt.ipleiria.estg.dei.savergy.adapters.RankingTabAdapter;
import pt.ipleiria.estg.dei.savergy.database.DatabaseHelper;
import pt.ipleiria.estg.dei.savergy.model.Savergy;

/**
 * Created by Trabalho on 27/05/2017.
 */

public class RankingActivity extends AppCompatActivity {

    private ActionMenuView defaultMenu;
    private ActionMenuView defaultMenuBack;

    private RankingTabAdapter rankingTabAdapter;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);

        Toolbar t = (Toolbar) findViewById(R.id.tToolbar);
        defaultMenu = (ActionMenuView) t.findViewById(R.id.defaultMenu);
        defaultMenuBack = (ActionMenuView) t.findViewById(R.id.defaultMenuBack);
        defaultMenu.setOnMenuItemClickListener(new ActionMenuView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                return onOptionsItemSelected(menuItem);
            }
        });
        defaultMenuBack.setOnMenuItemClickListener(new ActionMenuView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                return onOptionsItemSelected(menuItem);
            }
        });
        setSupportActionBar(t);
        getSupportActionBar().setTitle(null);

        rankingTabAdapter = new RankingTabAdapter(getSupportFragmentManager(), this);

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(rankingTabAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {}
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            public void onPageSelected(int position) {
                switch (position) {
                    case 0:

                        break;
                    case 1:

                        break;
                    case 2:

                        break;
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        //inflater.inflate(R.menu.menu_profile, defaultMenu.getMenu());
        inflater.inflate(R.menu.menu_return, defaultMenuBack.getMenu());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        long id=item.getItemId();

        if (id == R.id.menuReturnActivity) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

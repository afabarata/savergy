package pt.ipleiria.estg.dei.savergy;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import pt.ipleiria.estg.dei.savergy.adapters.AdvicesListAdapter;
import pt.ipleiria.estg.dei.savergy.database.DatabaseHelper;
import pt.ipleiria.estg.dei.savergy.model.Savergy;

/**
 * Created by Trabalho on 19/04/2017.
 */

public class AdvicesListActivity extends AppCompatActivity {

    private ActionMenuView defaultMenu;
    private ActionMenuView defaultMenuBack;

    private DatabaseHelper dbHelper;
    private ListView listViewAdvicesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advices_list);

        Toolbar t = (Toolbar) findViewById(R.id.tToolbar);
        defaultMenu = (ActionMenuView) t.findViewById(R.id.defaultMenu);
        defaultMenuBack = (ActionMenuView) t.findViewById(R.id.defaultMenuBack);
        defaultMenu.setOnMenuItemClickListener(new ActionMenuView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                return onOptionsItemSelected(menuItem);
            }
        });
        defaultMenuBack.setOnMenuItemClickListener(new ActionMenuView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                return onOptionsItemSelected(menuItem);
            }
        });
        setSupportActionBar(t);
        getSupportActionBar().setTitle(null);

        dbHelper = new DatabaseHelper(this);
        initialize();
        updateAdvicesList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        //inflater.inflate(R.menu.menu_profile, defaultMenu.getMenu());
        inflater.inflate(R.menu.menu_return, defaultMenuBack.getMenu());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        long id=item.getItemId();

        if (id == R.id.menuReturnActivity) {
            dbHelper.insertAdvices(Savergy.INSTANCE.getAdvices());

            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        dbHelper.insertAdvices(Savergy.INSTANCE.getAdvices());

        Intent intent = new Intent(AdvicesListActivity.this, DashboardActivity.class);
        startActivity(intent);
    }

    private void initialize() {
        listViewAdvicesList = (ListView) findViewById(R.id.listViewAdvicesList);

        listViewAdvicesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            //Toast.makeText(AdvicesListActivity.this,id+"",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateAdvicesList() {
        AdvicesListAdapter adapter = new AdvicesListAdapter(this, Savergy.INSTANCE.getAdvices());
        listViewAdvicesList.setAdapter(adapter);
    }

}

